
旨在通过dubbo架构，来将日常所用的技术点整合一起，并写完整实例，方便开发直接copy and use

待技术点初步整合完毕，微服务springCloud立即整合，届时会于此附上传送门

更新于2019.05.16 17:31
最近挣扎于新公司业务，无暇顾及于此。深思之后发现，明知道cloud是大趋势所在，但现在却已经很生疏了。故决定此项目就更新于此
可能新的cloud整合需要一段时间，到时会实时于此处附上地址【https://gitee.com/QingShanRuShi/My-life-cloud.git】，容我在新公司先熟悉下。

    工程结构

    ├── provider-api         公共jar包
    │       ├── dto             rpc交互数据传输对象
    │       └── service         zk注册的service
    ├── provider             底层rpc服务提供者 服务数据库的crud 技术点整合所在
    │       ├── annotation      自定义注解
    │       ├── aop             切面
    │       ├── common          自定义常量、异常，枚举，工具类等
    │       ├── config          自定义配置
    │       ├── controller      接口层
    │       ├── crontab         定时任务
    │       ├── domain          数据库交互层
    │       ├── interceptor     过滤器拦截器
    │       ├── jdk             jdk用法学习
    │       ├── listener        mq消费者
    │       ├── mapper          mybatis mapper层
    │       ├── mongodao        mongo dao层
    │       └── service.impl    逻辑处理及暴露服务层
    ├── consumer              底层rpc服务调用者 作为接口层使用
    │       ├──                 仅仅是实现调用服务demo，自己做拓展
    │       └──      
    ├── generator             mybatis逆向工程
    │       ├──generator        配置所在，配置好xml后直接运行这里的main即可             
    │       └──                 
    ├── common                公共包
    │       ├──                 目前只有定义返回集合 可以考虑抽出所有公共工具于此
    │       └── 
    ├── My-life.iml
    └── pom.xml
    