package com.zxr.providerapi.dto;

import lombok.*;

import java.io.Serializable;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:22 2019/4/28
 */
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable{

    private Integer id;

    private String username;

    private String password;
}
