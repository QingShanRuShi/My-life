package com.zxr.providerapi.service;


import com.zxr.common.vm.Result;
import com.zxr.providerapi.dto.UserDTO;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:10 2019/3/14
 */
public interface UserService {

    Result<UserDTO> getUser(int id);

    Result<UserDTO> updUser(UserDTO user);
}
