package zxr.starter.demo;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description: 从配置中获取指定配置
 * @Author: Zheng Xinrui
 * @Date: 16:21 2022/6/26
 */
@ConfigurationProperties("my.autoconfig")
public class HelloProperties {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
