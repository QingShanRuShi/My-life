package zxr.starter.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 自定义一个初始化接口
 * @Author: Zheng Xinrui
 * @Date: 16:19 2022/6/26
 */
@Slf4j
@RestController
public class MyAutoConfigurationController {

    private final HelloProperties helloProperties;

    public MyAutoConfigurationController(HelloProperties helloProperties) {
        this.helloProperties = helloProperties;
    }

    @RequestMapping("/")
    public String index(){
        log.info("访问首页");
        return helloProperties.getName() + " 欢迎您！";
    }
}
