package zxr.starter.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: resources/META-INF/spring.factories中指定boot自动装配该config，从而实例化指定的其他Bean等操作
 * @Author: Zheng Xinrui
 * @Date: 16:23 2022/6/26
 */
@Configuration
@ConditionalOnProperty(value = "my.autoconfig.name") //配置文件中有my.autoconfig.name才允许装配当前HelloAutoConfitguration Bean
@EnableConfigurationProperties(HelloProperties.class) //实例化HelloProperties
@ConditionalOnClass(Slf4j.class) //项目中有lombok的Slf4j.class（也就是需要有lombok包依赖）才允许装配当前HelloAutoConfitguration Bean
public class HelloAutoConfiguration {

    @Autowired
    HelloProperties helloProperties;

    // 创建接口的Bean
    @Bean
    public MyAutoConfigurationController myAutoConfigurationController(){
        return new MyAutoConfigurationController(helloProperties);
    }
}
