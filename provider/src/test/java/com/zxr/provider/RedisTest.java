package com.zxr.provider;

import com.zxr.provider.common.util.RedisUtil;
import com.zxr.provider.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 15:43 2019/4/2
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Component
@Slf4j
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testRedisUtil(){

        System.out.println("RedisUtilzxr"+"==="+ RedisUtil.getString("zxr"));
        System.out.println("RedisUtilzxr"+"==="+redisTemplate.opsForValue().get("zxr"));
        RedisUtil.set("zxr123",11111);
        RedisUtil.set("zxr000","dfafaf");
        System.out.println("RedisUtilzxr123"+"==="+RedisUtil.getString("zxr123"));
        System.out.println("redisTemplatezxr123"+"==="+redisTemplate.opsForValue().get("zxr123"));
        System.out.println("RedisUtilzxr000"+"==="+RedisUtil.getString("zxr000"));
        System.out.println("redisTemplatezxr000"+"==="+redisTemplate.opsForValue().get("zxr000"));
        RedisUtil.set("zxr222",123, 4000L);

        User user = User.builder()
                .id(1).username("zxr")
                .password("111111").build();
        RedisUtil.set("user", user);
        System.out.println("RedisUtilUser"+"====="+RedisUtil.getObject("user", User.class));
    }
}
