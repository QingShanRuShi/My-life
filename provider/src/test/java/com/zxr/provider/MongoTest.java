package com.zxr.provider;

import com.zxr.provider.domain.mongoEntity.MongoUser;
import com.zxr.provider.domain.mongoEntity.MongoUserAddress;
import com.zxr.provider.mongodao.UserAddressDAO;
import com.zxr.provider.mongodao.UserDAO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 14:52 2019/4/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Component
@Slf4j
public class MongoTest {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserAddressDAO userAddressDAO;

    @Test
    public void testUpdate(){
        MongoUser byPassword = userDAO.findByPassword("123456");
        byPassword.setUsername("weiTeng么么哒");
        long l = userDAO.updateUser(byPassword);
        log.info("更新结果-{}", l);
    }

    @Test
    public void testInsertList(){
        List<MongoUser> list = new ArrayList<>();
        list.add(MongoUser.builder().username("zxr").password("3445345").build());
        list.add(MongoUser.builder().username("ch").password("324325235").build());
        list.add(MongoUser.builder().username("wt").password("saf234124").build());
        list.add(MongoUser.builder().username("dy").password("!@@##$$%^^&*(").build());
        list.add(MongoUser.builder().username("xa").password("fdareqreqr").build());
        List<MongoUser> resultList = userDAO.inserList(list);
        log.info("批量插入结果输出-{}", resultList);
    }

    @Test
    public void testDelete(){
        long deleteNum = userDAO.delete("5cb978047f916d1fbc520f2b");
        long deleteNum2 = userDAO.delete("1111111111");
        log.info("存在的id的删除结果-{}", deleteNum);
        log.info("不存在的id的删除结果-{}", deleteNum2);
    }

    @Test
    public void insertUserAddress(){
        List<MongoUserAddress> list = new ArrayList<>();
        list.add(MongoUserAddress.builder().mongoUserId("5cb95d367f916d400c29b29f").address("北京").build());
        list.add(MongoUserAddress.builder().mongoUserId("5cb9613c7f916d2670da548b").address("上海").build());
        list.add(MongoUserAddress.builder().mongoUserId("5cb978047f916d1fbc520f27").address("长沙").build());
        list.add(MongoUserAddress.builder().mongoUserId("5cb978047f916d1fbc520f28").address("合肥").build());
        list.add(MongoUserAddress.builder().mongoUserId("5cb978047f916d1fbc520f29").address("广德").build());
        list.add(MongoUserAddress.builder().mongoUserId("5cb978047f916d1fbc520f2a").address("扎木斯").build());
        List<MongoUserAddress> mongoUserAddresses = userAddressDAO.insertList(list);
        log.info("插入的结果：-{}", mongoUserAddresses);
    }

    @Test
    public void findOrAndInCompareRegex(){
        System.out.println("test"+userAddressDAO.findOr());
        System.out.println("test"+userAddressDAO.findAnd());
        System.out.println("test"+userAddressDAO.findIn("北京","上海"));
        System.out.println("test"+userAddressDAO.findCompare());
        System.out.println("test"+userAddressDAO.findRegex());
    }

    @Test
    public void findGroup(){
        System.out.println("test"+userAddressDAO.findGroup());
        System.out.println("test"+userAddressDAO.findSort());
        System.out.println("test"+userAddressDAO.findLimit(2, 3));
        System.out.println("test"+userAddressDAO.findAggregation());
        System.out.println("test"+userAddressDAO.findAggregation2());
    }
}
