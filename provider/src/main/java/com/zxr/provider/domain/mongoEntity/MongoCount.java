package com.zxr.provider.domain.mongoEntity;

import lombok.*;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 13:15 2019/4/28
 */
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MongoCount {

    private String id;

    private int count;
}
