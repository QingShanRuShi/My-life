package com.zxr.provider.domain;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Log implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 操作人token
     */
    private String token;

    /**
     * 操作的方法全类名
     */
    private String method;

    /**
     * 创建时间
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;
}