package com.zxr.provider.domain.mongoEntity;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @Description: mongo测试使用实体类
 * @Author: Zheng Xinrui
 * @Date: 11:17 2019/4/19
 */
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MongoUser implements Serializable{
    @Id
    private String id; //mongo会根据时间戳，机器码，等信息自动生成String类型id并返回

    private String username;

    private String password;
}
