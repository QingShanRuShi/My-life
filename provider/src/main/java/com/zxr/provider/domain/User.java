package com.zxr.provider.domain;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable{
    private Integer id;

    private String username;

    private String password;
}