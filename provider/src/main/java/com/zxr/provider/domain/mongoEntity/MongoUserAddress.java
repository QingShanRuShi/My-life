package com.zxr.provider.domain.mongoEntity;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @Description: mongo用户地址
 * @Author: Zheng Xinrui
 * @Date: 16:42 2019/4/19
 */
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MongoUserAddress implements Serializable{

    @Id
    private String id;

    //MongoUser表id
    private String mongoUserId;

    private String address;

    private Integer age;
}
