package com.zxr.provider.mongodao;

import com.zxr.provider.domain.mongoEntity.MongoCount;
import com.zxr.provider.domain.mongoEntity.MongoUserAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Description: mongo操作dao层
 * @Author: Zheng Xinrui
 * @Date: 16:45 2019/4/19
 */
@Slf4j
@Component
public class UserAddressDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    private String collenctionName = "user_address";

    /**
     * 批量插入
     * @param list
     * @return
     */
    public List<MongoUserAddress> insertList(List<MongoUserAddress> list){
        return new ArrayList<>( mongoTemplate.insert(list, collenctionName));
    }

    /**
     * or查询，可以和and合用
     *
     * @return
     */
    public List<MongoUserAddress> findOr(){
        //在前提条件下的or查询
        Criteria[] criteria = {Criteria.where("mongoUserId").is("jnhja13241jn135njn51134")
                , Criteria.where("mongoUserId").is("5cb9613c7f916d2670da548b")};

        Query query = new Query(Criteria.where("address").is("北京").orOperator(criteria));
        Query query1 = new Query(new Criteria().orOperator(criteria));
        log.info("单独or条件查询结果-{}", mongoTemplate.find(query1, MongoUserAddress.class, collenctionName));
        return mongoTemplate.find(query, MongoUserAddress.class, collenctionName);
    }

    /**
     * and查询
     *
     * @return
     */
    public List<MongoUserAddress> findAnd(){
        Query query = new Query(Criteria.where("address").is("北京").and("mongoUserId").is("5cb95d367f916d400c29b29f"));
        return mongoTemplate.find(query, MongoUserAddress.class, collenctionName);
    }

    /**
     * in操作
     *
     * @param address
     * @return
     */
    public List<MongoUserAddress> findIn(String...address){
        List<String> list = new ArrayList<>(Arrays.asList(address));
        Query query = new Query(Criteria.where("address").in(list));
        return mongoTemplate.find(query, MongoUserAddress.class, collenctionName);
    }

    /**
     * 数值比较查询gte(>=),gt(>),lte(<=),lt(<)
     *
     * @return
     */
    public List<MongoUserAddress> findCompare(){
        Query queryLt = new Query(Criteria.where("age").lt(20));
        List<MongoUserAddress> mongoUserAddressesLt = mongoTemplate.find(queryLt, MongoUserAddress.class, collenctionName);
        log.info("小于查询-{}", mongoUserAddressesLt);
        Query queryLte = new Query(Criteria.where("age").lte(20));
        List<MongoUserAddress> mongoUserAddressesLte = mongoTemplate.find(queryLte, MongoUserAddress.class, collenctionName);
        log.info("小于等于查询-{}", mongoUserAddressesLte);
        Query queryGt = new Query(Criteria.where("age").gt(20));
        List<MongoUserAddress> mongoUserAddressesGt = mongoTemplate.find(queryGt, MongoUserAddress.class, collenctionName);
        log.info("大于查询-{}", mongoUserAddressesGt);
        Query queryGte = new Query(Criteria.where("age").gte(20));
        List<MongoUserAddress> mongoUserAddressesGte = mongoTemplate.find(queryGte, MongoUserAddress.class, collenctionName);
        log.info("小于等于查询-{}", mongoUserAddressesGte);
        return mongoUserAddressesGte;
    }

    //正则查询

    /**
     * 正则查询，常用来进行模糊查询
     *
     * @return
     */
    public List<UserAddressDAO> findRegex(){
        //address中带“京”的模糊查询
        Query query = new Query(Criteria.where("address").regex("^.*京.*$"));
        Query query2 = new Query(new Criteria().orOperator(Criteria.where("address").regex("^.*京.*$")
                .and("age").lt(65)));
        log.info("模糊查询加其他条件查询-{}", mongoTemplate.find(query2, MongoUserAddress.class, collenctionName));
        return mongoTemplate.find(query, UserAddressDAO.class, collenctionName);
    }

    /**
     * 分组查询，统计各个地方的人数
     *
     * @return
     */
    public List<Map> findGroup(){
        Aggregation aggregation = Aggregation.newAggregation(Aggregation.group("address").count().as("城市"));
        AggregationResults<Map> aggregate = mongoTemplate.aggregate(aggregation, collenctionName, Map.class);
        return aggregate.getMappedResults();
    }

    /**
     * 排序,根据年龄倒序(如果指定排序的字段在某条记录中没有，则都会查出来, 但是顺序有待考察)
     *
     * @return
     */
    public List<MongoUserAddress> findSort(){
        Query query = new Query(Criteria.where("age").gt(10)).with(new Sort(Sort.Direction.DESC, "age"));
        return mongoTemplate.find(query, MongoUserAddress.class, collenctionName);
    }

    /**
     * 分页，先根据 age大于10岁的倒序
     *
     * @param pageSize 每页数量
     * @param pageNum 页码
     * @return
     */
    public List<MongoUserAddress> findLimit(int pageSize, int pageNum){
        Query query = new Query(Criteria.where("age").gt(10))
                .with(new Sort(Sort.Direction.DESC, "age")).skip((pageNum-1)*pageSize).limit(pageSize);
        return mongoTemplate.find(query, MongoUserAddress.class, collenctionName);
    }

    /**
     * 聚合查询
     *
     * @return
     */
    public List<MongoUserAddress> findAggregation(){
        Criteria criteria = Criteria.where("address").is("北京");
        Aggregation aggregation = Aggregation.newAggregation(
//                Aggregation.group("address"),
                Aggregation.match(criteria),
                Aggregation.skip(1L),
                Aggregation.limit(10L),
                Aggregation.sort(Sort.Direction.DESC, "age")
        );
        AggregationResults<MongoUserAddress> aggregate = mongoTemplate.aggregate(aggregation, collenctionName, MongoUserAddress.class);
        return aggregate.getMappedResults();
    }

    /**
     * 聚合查询，分组的使用
     *
     * @return
     */
    public List<MongoCount> findAggregation2(){
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.group("address").count().as("count"),
                Aggregation.sort(Sort.Direction.ASC, "count")
        );
        AggregationResults<MongoCount> aggregate = mongoTemplate.aggregate(aggregation, collenctionName, MongoCount.class);
        return aggregate.getMappedResults();
    }
}
