package com.zxr.provider.mongodao;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.zxr.provider.domain.mongoEntity.MongoUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: mongo操作dao层
 * @Author: Zheng Xinrui
 * @Date: 17:23 2019/4/18
 */
@Slf4j
@Component
public class UserDAO implements Serializable{

    @Autowired
    MongoTemplate mongoTemplate;

    private String collenctionName = "user";

    /*如果插入的集合的“_id”值，在集合中已经存在,用Insert执行插入操作回报异常，已经存在"_id"的键。
    用Save如果系统中没有相同的"_id"就执行插入操作，有的话就执行覆盖掉原来的值
    insert: 可以一次性插入一整个列表，而不用进行遍历操作，效率相对较高
　　 save: 需要遍历列表，进行一个个的插入*/
    public MongoUser inser(MongoUser user){
        //mongoTemplate.save(user, "user");
        return mongoTemplate.insert(user, collenctionName);
    }

    public List<MongoUser> inserList(List<MongoUser> userList){
        return new ArrayList<>(mongoTemplate.insert(userList, collenctionName));
    }

    /**
     * 根据单一条件查询一个结果，当有多个条件满足时，根据先后顺序取第一个
     * @param password
     * @return
     */
    public MongoUser findByPassword(String password){
        Query query = new Query(Criteria.where("password").is(password));
        return mongoTemplate.findOne(query, MongoUser.class, collenctionName);
    }

    /**
     * 根据单一条件查询所有满足条件的结果
     * @param password
     * @return
     */
    public List<MongoUser> findAllByPassword(String password){
        Query query = new Query(Criteria.where("password").is(password));
        return mongoTemplate.find(query, MongoUser.class, collenctionName);
    }


    /**
     * 单个更新
     * @param user
     * @return
     */
    public long updateUser(MongoUser user){
        Update update = new Update().set("username", user.getUsername()).set("password", user.getPassword());
        Query query = new Query(Criteria.where("id").is(user.getId()));
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, MongoUser.class, collenctionName);
        //返回更新成功的数量
        return updateResult.getMatchedCount();
    }

    /**
     * 批量更新，更新指定id集合的相同属性为相同值
     * @param user
     * @return
     */
    public long updateUserMulti(MongoUser user){
        List<String> list = new ArrayList<>();
        list.add(user.getId());
        list.add("5cb9613c7f916d2670da548b");//模拟批量更新
        Query query = new Query(Criteria.where("id").in(list));
        Update update = new Update().set("username", user.getUsername()).set("password", user.getPassword());
        //批量更新
        UpdateResult updateResult = mongoTemplate.updateMulti(query, update, MongoUser.class, collenctionName);
        //返回更新成功的数量
        return updateResult.getMatchedCount();
    }

    public long delete(String id){
        Query query = new Query(Criteria.where("id").is(id));
        DeleteResult removeResult = mongoTemplate.remove(query, MongoUser.class,collenctionName);
        return removeResult.getDeletedCount();
    }
}
