package com.zxr.provider.listener;

import com.zxr.provider.config.RocketmqConConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description: rocketmq default消费者
 *                 ApplicationListener<ContextRefreshedEvent>当所有bean被预实例化完成后触发监控事件（观察者模式）
 *                 新增rocket消费者的时候，只需要继承conConfig，实现项目启动后注入容器即可开启观察者监听
 * @Author: Zheng Xinrui
 * @Date: 16:44 2019/4/8
 */
@Slf4j
@Component
public class RocketDefaultListener extends RocketmqConConfig implements ApplicationListener<ContextRefreshedEvent>{

    @Value("${constants.rocketTopic}")
    private String topic;

    @Value("${constants.rocketTag}")
    private String tag;

    /**
     * 消费者具体的业务逻辑实现
     *
     * @param msgs
     * @return
     */
    @Override
    public ConsumeConcurrentlyStatus dealBody(List<MessageExt> msgs) {

        try {
            for(MessageExt messageExt : msgs){
                log.info("消费完成后消息内容为：{}",new String(messageExt.getBody(), "utf-8"));
            }
        }catch (Exception e){
            log.error("消费消息时出错", e);
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }

    /**
     * 消费者监听器
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        try {
            super.listener(topic, tag);
        }catch (Exception e){
            log.error("消费者监听异常", e);
        }
    }
}
