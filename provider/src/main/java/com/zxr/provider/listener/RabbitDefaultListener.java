package com.zxr.provider.listener;

import com.zxr.provider.common.constant.QueueNames;
import com.zxr.provider.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Description: rabbitmq default消费者
 * @Author: Zheng Xinrui
 * @Date: 11:25 2019/4/4
 */
@Component
@Slf4j
@RabbitListener(queues = QueueNames.DEFAULT)
public class RabbitDefaultListener {

    @RabbitHandler
    public void defaultListener(String msg){
        log.info("mq字符串消息-{}", msg);
    }

    @RabbitHandler
    public void defaultListener(User user){
        log.info("mq对象消息-{}", user);
    }
}
