package com.zxr.provider.listener;

import com.zxr.provider.common.constant.QueueNames;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @Description: kafka消费者
 * @Author: Zheng Xinrui
 * @Date: 21:51 2019/12/24
 */
@Slf4j
@Component
public class KafkaDefaultListener {

    @KafkaListener(topics = {QueueNames.KAFKADEFAULT})
    public void kafkaDefaultListener(ConsumerRecord<String, String> consumerRecord){
        log.info("topic:{}, 接收消息：{}, offset:{}, Pattern:{}", consumerRecord.topic(), consumerRecord.value(), consumerRecord.offset(), consumerRecord.partition());
    }

    @KafkaListener(topics = {QueueNames.KAFKADEFAULT2})
    public void kafkaDefaultListener2(ConsumerRecord<String, String> consumerRecord){
        log.info("topic:{}, 接收消息：{}, offset:{}, Pattern:{}", consumerRecord.topic(), consumerRecord.value(), consumerRecord.offset(), consumerRecord.partition());
    }
}
