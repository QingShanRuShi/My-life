package com.zxr.provider.annotation;

import java.lang.annotation.*;

/**
 * @Description: 基于redis实现的分布式锁
 * @Author: Zheng Xinrui
 * @Date: 15:42 2019/4/1
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {

    /**
     *  最大锁定时间
     */
    String timeOut() default "";

    /**
     * 生成锁名(为了后面拓展为函数式编程所留)
     */
    String value() default "";
}
