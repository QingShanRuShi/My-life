package com.zxr.provider.annotation;

import java.lang.annotation.*;

/**
 * @Description: 自定义注解，校验token
 * @Author: Zheng Xinrui
 * @Date: 14:40 2019/4/9
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CheckToken {

    /**
     * 是否忽视token校验
     *
     * @return
     */
    boolean ignore() default false;
}
