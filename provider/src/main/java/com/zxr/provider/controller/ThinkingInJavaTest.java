package com.zxr.provider.controller;


import java.util.Random;

/**
 * @Description: thinking in java 例子测试
 * @Author: Zheng Xinrui
 * @Date: 22:37 2020/5/21
 */
public class ThinkingInJavaTest {

    public static void main(String[] args) {
//        magicNumber47();
//        label();
        classStructure();
    }

    /**
     * chapter 3 operator, 3.5
     */
    private static void magicNumber47(){
        Random random = new Random(47);
        for (int i = 0; i < 50; i++) {
            System.out.println(random.nextInt());
        }
    }

    /**
     * 标签的用法，标签必须紧跟循环，for,while
     */
    /*1.一般的 continue 会退回最内层循环的开头（顶部），并继续执行。
      2.带标签的 continue 会到达标签的位置，并重新进入紧接在那个标签后面的循环。
      3.一般的 break 会中断并跳出当前循环。
      4.带标签的 break 会中断并跳出标签所指的循环。*/
    private static void label(){
        int i = 0;
        label1:
        for(; true; ){
            label2:
            for(; i < 10; i++){
                System.out.println("i = " + i);
                if(i == 2){
                    System.out.println("i==2 continue");
                    continue;
                }
                if(i == 3){
                    System.out.println("i == 3 break");
                    i++;
                    break;
                }
                if(i == 7){
                    System.out.println("i == 7 continue label1");
                    i++;
                    continue label1;
                }
                if(i == 8){
                    System.out.println("i == 8 break label2");
                    break label1;
                }
                for (int j = 0; j < 5; j++) {
                    if(j == 3){
                        System.out.println("j == 3 continue label2");
                        continue label2;
                    }
                }
            }
        }
    }

    //类的构造函数
    private static void classStructure(){
        Bird bird1 = new Bird();
        Bird bird = new Bird(10);
        Bird zxr1 = new Bird("zxr");
        zxr1.getAll();
        Bird zxr = new Bird(10, "zxr");

    }
}

//类的初始化
class Bird{

    private int i = 0;
    private String s = "";

    //由类的构造器，引出重载（overload）,
    //重载：方法名和返回类型相同，参数不同（这里的参数不同包含：1.参数顺序，2.参数类型，3.参数个数）,之所以没有将方法返回类型列为重载的条件，是因为实际开发中很多场景只需要使用方法的内容，而不关心返回，这个时候就给jvm造成了困扰
    //默认构造函数：如果没有写明类构造函数，则默认一个空参构造函数，如果有写明构造函数则不会有默认的空参构造出现
    Bird(){
        this(1);
//        this("x"); Can't call two  ，构造方法调用构造方法只能调用一次且必须在方法最开始
        System.out.println("默认的构造方法");
    }

    Bird(String s){
        this(1, s);
        System.out.println("一个参数的构造方法，参数s: " + s);
    }

    Bird(int i){
        System.out.println("一个参数的构造方法，参数i: "+ i);
    }

    Bird(int i, String s){
        this.i = i;
        this.s = s;
        System.out.println("两个参数的构造方法，参数i: " + i + "，参数s: " + s);
    }

    void getAll(){
//        this(1); Not inside non-constructor 除了构造器外，不允许在其他方法直接调用构造器(注意，这里是说“调用”而不是类的初始化)
        System.out.println("参数i: " + i + "，参数s: " + s);
    }
}
