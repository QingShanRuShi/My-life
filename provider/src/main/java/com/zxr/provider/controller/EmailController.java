package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 15:46 2021/11/19
 */
@RestController
public class EmailController {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailProperties mailProperties;

    @Autowired
    private TemplateEngine templateEngine;

    @GetMapping("/testEmail")
    public Result<Object> testEmail() throws MessagingException {
        Context context = new Context();
        context.setVariable("project", "测试");
        context.setVariable("author", "zxr");
        context.setVariable("code", "999999");
        String emailContent = templateEngine.process("EmailTemplate", context);
        MimeMessageHelper helper = new MimeMessageHelper(mailSender.createMimeMessage(),true,"utf-8");
        helper.setFrom(mailProperties.getUsername()); //发件人账号
        helper.setTo("zhengxinrui@wondersgroup.com"); //目标邮箱
        helper.setSubject("zxr测试测试"); //主题
        helper.setText(emailContent, true); //邮件内容，可以使用template模板引擎
        //抄送
        ArrayList<String> cc = new ArrayList<>();
        cc.add("changhong@wondersgroup.com");
        cc.add("2657470350@qq.com");
        if (!CollectionUtils.isEmpty(cc)) {
            for (Object o : cc) {
                helper.addCc(o.toString());
            }
        }
        //密送
        helper.addBcc("15855188124@163.com");
        //添加附件
        helper.addAttachment("XXX个人简历.pdf",new ByteArrayResource(getBytes("C:\\Users\\zhengxinrui\\Desktop\\XXX.pdf")));
        //添加图片, contentId对应html中的img标签中src属性设置的：cid，如<img src="cid:testimage1">
        helper.addInline("testimage1", new File("C:\\Users\\zhengxinrui\\Desktop\\images\\bz3.jpg"));
        //发送
        mailSender.send(helper.getMimeMessage());
        return Result.result();
    }

    /**
     * 将文件转为byte[]
     * @param filePath 文件路径
     * @return
     */
    public static byte[] getBytes(String filePath){
        File file = new File(filePath);
        ByteArrayOutputStream out = null;
        try {
            FileInputStream in = new FileInputStream(file);
            out = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            while (in.read(b) != -1) {
                out.write(b, 0, b.length);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }
}
