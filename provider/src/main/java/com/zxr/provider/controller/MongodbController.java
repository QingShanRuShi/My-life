package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import com.zxr.provider.domain.mongoEntity.MongoUser;
import com.zxr.provider.mongodao.UserDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: mongodb测试
 * @Author: Zheng Xinrui
 * @Date: 11:11 2019/4/19
 */
@Slf4j
@RestController
@RequestMapping("mongo")
public class MongodbController {

    @Autowired
    private UserDAO userDAO;

    @GetMapping("testMongo")
    public Result testMongo(){
        return Result.result(userDAO.inser(MongoUser.builder().username("rxz").password("123456").build()));
    }

    @GetMapping("testMongo2")
    public Result testMongo2(){
        MongoUser byPassword = userDAO.findByPassword("123456");
        List<MongoUser> allByPassword = userDAO.findAllByPassword("123456");
        log.info("byPassword-{}", byPassword);
        log.info("allByPassword-{}", allByPassword);
        return Result.result();
    }
}
