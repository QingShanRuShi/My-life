package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: swagger2测试controller
 * @Author: Zheng Xinrui
 * @Date: 17:50 2019/4/11
 */
@RestController
@RequestMapping("swagger")
@Slf4j
@Api(value = "Swagger2Controller|swagger2测试controller")
public class Swagger2Controller {

    @ApiOperation(value = "测试swagger")
    @ApiImplicitParam(name = "id", value = "测试id", paramType = "path", required = true, dataType = "Integer")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result testSwagger2(@PathVariable int id){
        return Result.result(id);
    }
}
