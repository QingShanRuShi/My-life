package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: rocketmq测试
 * @Author: Zheng Xinrui
 * @Date: 16:58 2019/4/8
 */
@RestController
@Slf4j
@RequestMapping("/rocket")
public class RocketmqController {

    @Value("${constants.rocketTopic}")
    private String topic;

    @Value("${constants.rocketTag}")
    private String tag;

    @Autowired
    private DefaultMQProducer defaultMQProducer;

    /**
     * 普通的rocket消息发送测试
     *
     * @param msg 要发送的消息
     * @return
     * @throws Exception
     */
    @GetMapping("/rocketTest")
    public Result rocketTest(String msg) throws RemotingException, MQClientException, InterruptedException {

        for(int i = 0; i < 1000; i++){
            Message message = new Message(topic, tag, "123456", (msg+i).getBytes());
            //这里是提供了一个回调消息(消息处理是异步，类似ajax)
            defaultMQProducer.send(message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    log.info("传输成功:{}", sendResult.toString());
                }
                @Override
                public void onException(Throwable e) {
                    log.error("传输失败", e);
                }
            });
        }
        return Result.result();
    }
}
