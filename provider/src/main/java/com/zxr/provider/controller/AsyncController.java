package com.zxr.provider.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zxr.common.vm.Result;
import com.zxr.provider.service.impl.AsyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @Description: 异步方法测试
 * @Author: Zheng Xinrui
 * @Date: 13:49 2019/4/30
 */
@Slf4j
@RestController
@RequestMapping("/async")
public class AsyncController {

    @Autowired
    private AsyncService asyncService;

    /**
     * 无返回值的异步调用
     *
     * @param coreThreadSize
     * @return
     * @throws InterruptedException
     */
    @GetMapping("/asyncVoid")
    public Result asyncVoid(int coreThreadSize) throws InterruptedException {
        log.info("asyncVoid is starting");
        long startTime = System.currentTimeMillis();
        for(int i=0; i<coreThreadSize; i++){
            asyncService.asyncRetrunVoid();
        }
        log.info("asyncVoid is ending, expend time is:{}", System.currentTimeMillis() - startTime);
        return Result.result();
    }

    /**
     * 有返回值的异步调用, 多次调同一个异步方法，返回值需要统一封装，这样所耗时间只有一次异步方法的时间
     *
     * @param coreThreadSize
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @GetMapping("/asyncString")
    public Result asyncString(int coreThreadSize) throws InterruptedException, ExecutionException {
        long startTime = System.currentTimeMillis();
        List<Future<String>> listFuture = new ArrayList<>();
        //一定要如下面一样统一取出返回值，不然达不到异步效果
        for(int i=0; i<coreThreadSize; i++){
            Future<String> stringFuture = asyncService.asyncRetrunString(i);
            listFuture.add(stringFuture);
        }
        List<String> listResult = new ArrayList<>();
        for(Future<String> future : listFuture){
            listResult.add(future.get());
        }
        log.info("asyncString is ending, expend time is:{}", System.currentTimeMillis() - startTime);
        return Result.result(listResult);
    }

}


