package com.zxr.provider.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 10:36 2020/11/4
 */
public class TestController {

    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        int reverse = reverse(-100102180);
        System.out.println(reverse);
    }

    //整数反转
    public static int reverse(int x) {
        //如果是一位数直接返回
        if(x/10 == 0){
           return x;
        }
        ArrayList<Long> arrayList = new ArrayList<>();
        boolean b = false;
        String s = x + "";
        long l;
        //包含负符号
        if(s.contains("-")){
            b = true;
            s= s.replace("-", "");
        }
        l = Long.parseLong(s);
        for (int i = 0; i < s.length(); i++) {
            long i1 = l % 10;
            if(i1 != 0 || arrayList.size() > 0){
                arrayList.add(i1);
            }
            l = l / 10;
        }
        String ss = "";
        for (Long l1 : arrayList) {
            ss += l1;
        }
        long l2 = Long.parseLong(ss);
        //如果最终数值不在int范围
        if(l2 > Integer.MAX_VALUE || -l2 < Integer.MIN_VALUE){
            return 0;
        }
        if(b){
           ss = "-"+ss;
        }
        return Integer.parseInt(ss);
    }


    /**
     int[] ints = {1, 2, 3, 4, 5, 6, 7, 8, 9};
     int target = 10;
     int[] ints1 = sumTowNum(ints, target);
     System.out.println("结果："+Arrays.toString(ints1));
     */
    //两数之和
    private static int[] sumTowNum(int[] num, int target){
        System.out.println("数组内容：" + Arrays.toString(num));
        System.out.println("目标值：" + target);

        /*//暴力循环,时间复杂度O(N)，不可取
        for (int i = 0; i < num.length; i++) {
            for (int j = 0; j < num.length; j++) {
                if(target - num[i] == num[j]){
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{};*/

        //hash，时间复杂度O(1)
        HashMap<Integer, Integer> hashTable = new HashMap<>();
        for (int i = 0; i < num.length; i++) {
            if(hashTable.containsKey(target - num[i])){
                return new int[]{i, hashTable.get(target - num[i])};
            }else {
                hashTable.put(num[i], i);
            }
        }
        return new int[]{};
    }
}
