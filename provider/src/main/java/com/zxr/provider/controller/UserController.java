package com.zxr.provider.controller;

import com.github.pagehelper.PageHelper;
import com.zxr.common.enums.ResultCodes;
import com.zxr.common.vm.PageTO;
import com.zxr.common.vm.Result;
import com.zxr.provider.annotation.RedisLock;
import com.zxr.provider.common.constant.QueueNames;
import com.zxr.provider.domain.User;
import com.zxr.provider.domain.UserSecond;
import com.zxr.provider.service.impl.UserServiceImpl;
import com.zxr.providerapi.dto.UserDTO;
import com.zxr.providerapi.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:20 2019/3/14
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private AmqpTemplate amqpTemplate;

    //测试数据库连接,此处应该重新定义个vo来表示与视图层交互
    @GetMapping("/testUser")
    public Result<UserDTO> testUser(int id){
        return userService.getUser(id);
    }

    @GetMapping("/getUser")
    public Result<PageTO<UserDTO>> getUser(){
        PageHelper.startPage(2, 3);
        List<UserDTO> userAll = userServiceImpl.getUserAll();
        return Result.result(new PageTO<UserDTO>(userAll));
    }

    @GetMapping("/testMostDataSource")
    public Result<UserSecond> testMostDataSource(int id){
        return userServiceImpl.getSecondUser(id);
    }

    //测试分布式锁
    @RedisLock(timeOut = "10")
    @GetMapping("/testLock")
    public void testLock(HttpServletRequest request) throws InterruptedException {
        String token = "token";
        long sleepTime = 30000;
       log.info("token={}", request.getParameter(token));
       Thread.sleep(sleepTime);
       log.info("end==============end");
    }

    //测试返回值
    @GetMapping("testResultCodes")
    public Result<User> testResultCodes(){
        ThreadLocal threadLocal = new ThreadLocal();
        User user = User.builder().id(1).username("zxr").password("123456").build();
        log.info("threadLocal-{}", threadLocal.get());
        return Result.result(user, ResultCodes.SUCCESS);
    }

    //测试rabbitmq发送消息,测试ThreadLocal
    @GetMapping("testRabbitMq")
    public Result testRabbitMq(){

        String msg = "rabbitMq的第一个消息";
        User user = User.builder().id(1).username("zxr").password("111").build();
        //普通字符串消息
        amqpTemplate.convertAndSend(QueueNames.DEFAULT, msg);
        //发送对象消息
        amqpTemplate.convertAndSend(QueueNames.DEFAULT, user);

        /*
        //重写ThreadLocaljdk8新方法即完成初始化
        ThreadLocal threadLocal = new ThreadLocal(){
            @Override
            public User initialValue(){

                return User.builder().id(2).username("rxz").password("zzz").build();
            }
        };
        //这里是直接set进数值
       threadLocal.set(user);*/

        //上面的可以替换成jdk8中, 直接初始化
        ThreadLocal<User> threadLocal = ThreadLocal.withInitial(() -> user);

        for (int i = 0; i < 1000; i++) {
            System.out.print(i);
        }
        System.out.println();
        log.info("threadLocal{}", threadLocal.get());
        return Result.result();
    }

}