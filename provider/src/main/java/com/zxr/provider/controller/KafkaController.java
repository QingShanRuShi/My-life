package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import com.zxr.provider.common.constant.QueueNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: kafka测试接口
 * @Author: Zheng Xinrui
 * @Date: 15:52 2019/11/28
 */
@Slf4j
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @GetMapping("/testSend")
    public Result testSend(String message){
        for(int i=0; i<100; i++){
           kafkaTemplate.send(QueueNames.KAFKADEFAULT, message+i);
        }
        return Result.result();
    }
    @GetMapping("/testSend2")
    public Result testSend2(String message){
        for(int i=0; i<5; i++){
            ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(QueueNames.KAFKADEFAULT, i + message);
            future.addCallback(
                    success -> log.info("testSend2 send success:{}", success.getProducerRecord()),
                    fail -> log.info("testSend2 send fail:{}",fail.getMessage())
            );
        }
        return Result.result();
    }
}
