package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import com.zxr.provider.common.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 测试jwt
 * @Author: Zheng Xinrui
 * @Date: 13:34 2019/4/16
 */
@RestController
@RequestMapping("/jwt")
public class JWTController {

    @Autowired
    private JWTUtil jwtUtil;

    private String username = "zxr";

    @GetMapping("getJWT")
    public Result testJWT(){
        String zxr = jwtUtil.getToken(username);
        return Result.result(zxr);
    }
    @GetMapping("checkJWT")
    public Result checkJWT(String token){
        boolean verify = jwtUtil.verify(token,username, username);
        return Result.result(verify);
    }
    @GetMapping("getNameFromJWT")
    public Result getNameFromJWT(String token){
        String str = "name";
        String user = jwtUtil.getUser(token, str);
        return Result.result(user);
    }
}
