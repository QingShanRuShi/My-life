package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import com.zxr.provider.annotation.CheckToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:01 2019/4/9
 */
@Slf4j
@RestController
@RequestMapping("/check")
public class CheckUserController {

    @GetMapping("/interceptorTest")
    @CheckToken
    public Result interceptorTest(HttpServletRequest request){
        log.info("进入接口");
        log.info("token-{}", request.getParameter("token"));
        log.info("id-{}", request.getParameter("id"));
        return Result.result();
    }
}
