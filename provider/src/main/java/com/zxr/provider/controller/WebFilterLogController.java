package com.zxr.provider.controller;

import com.zxr.common.vm.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 测试webFilter记录日志
 * @Author: Zheng Xinrui
 * @Date: 16:55 2019/4/29
 */
@Slf4j
@RestController
@RequestMapping("/log")
public class WebFilterLogController {


    @GetMapping("/filterTest")
    public Result filterTest(){
        return Result.result();
    }


    @GetMapping("/filterTest2")
    public Result filterTest2(){
        return Result.result();
    }


}
