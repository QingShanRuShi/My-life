package com.zxr.provider.controller;

import com.zxr.common.enums.ErrorCodes;
import com.zxr.common.vm.Result;
import com.zxr.provider.common.exception.BaseException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 测试全局异常捕获
 * @Author: Zheng Xinrui
 * @Date: 13:35 2019/4/18
 */
@RestController
@RequestMapping("exception")
public class GlobalExceptionController {

    @GetMapping("testBaseException")
    public void testBaseException(){
        throw  new BaseException(ErrorCodes.ERROR_10002);
    }

    @GetMapping("testException")
    public void testException(){
        int i = 1/0;
    }

    @GetMapping("testAssert")
    public Result testAssert(int i){
        //断言，需要开启相应的jvm参数，否则不校验继续往下走,表达式为true则继续走下去，否则抛java.lang.AssertionError
        //冒号后面为自定义异常信息
        assert i > 0 :"传入的参数不能小于等于0";
        return Result.result();
    }
}
