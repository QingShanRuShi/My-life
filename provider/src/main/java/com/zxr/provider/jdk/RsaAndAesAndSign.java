package com.zxr.provider.jdk;

import com.alibaba.fastjson.JSONObject;
import com.zxr.provider.common.constant.SecretKeyConstants;
import com.zxr.provider.common.util.SignUtils;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @Description: rsa,aes,sign加解密
 * @Author: Zheng Xinrui
 * @Date: 10:13 2019/4/18
 */
public class RsaAndAesAndSign {


    public static void main(String[] args) {
        rsaAndSign();

        Map<String, Object> treeMap = new TreeMap<>();
        treeMap.put("sy",1111);
        treeMap.put("s",1111);
        treeMap.put("a",1111);
        treeMap.put("zs",1111);
        treeMap.put("sa",1111);

        System.out.println(treeMap.toString());



    }

    private static void rsaAndSign(){
        //使用treeMap来帮我们排序,按key的ascii码依次递增排序
        Map<String, Object> params = new TreeMap<>();
        params.put("clientId", "123456");
        params.put("userId", "88889");
        params.put("customerStatement", "需要解析的用户语句");
        params.put("sessionId", "99999");
        params.put("token", "shanghaiwonders");
        System.out.println("未签名时数据"+params.toString());
        //拼接
        StringBuilder stringBuilder = new StringBuilder();
        for (String key : params.keySet()) {
            stringBuilder.append(key).append("=").append(params.get(key)).append("&");
        }
        String sign = stringBuilder.toString().substring(0, stringBuilder.length() - 1);
        System.out.println("加签前参数"+sign);
        //客户端使用client私钥sign签名,并当做参数
        String sign2 = SignUtils.sign(sign, SecretKeyConstants.clientPrivateKEY);
        boolean verify1 = SignUtils.verify(sign, sign2, SecretKeyConstants.clientPublicKEY);
        System.out.println("直接验签"+verify1);

        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(params));
        System.out.println(jsonObject);

        System.out.println("加签"+sign2);
        params.put("sign", sign2);
        //客户端使用system公钥rsa加密参数
        String ss=JSONObject.toJSONString(params);
        String enContent = SignUtils.encrypt(ss, SecretKeyConstants.systemPublicKey);
        System.out.println("客户端rsa加密后"+enContent);

        //服务端使用system私钥rsa解密参数
        String decrypt = SignUtils.decrypt(enContent, SecretKeyConstants.systemPrivateKey);
        System.out.println("解密后"+decrypt);
        Map<String, Object> map = JSONObject.parseObject(decrypt).toJavaObject(Map.class);
        String sign1 = (String) map.get("sign");
        map.remove("sign");

        Map<String, Object> treeMap = map.keySet().stream().collect(Collectors.toMap(key -> key, map::get, (a, b) -> b, TreeMap::new));

        String stringBuilder1 = treeMap.keySet().stream().map(key -> key + "=" + treeMap.get(key) + "&").collect(Collectors.joining());

        //以上两个stream表达式相当于
        /* //使用treeMap自动排序
        Map<String, Object> treeMap = new TreeMap<>();
        for(String key : map.keySet()){
            treeMap.put(key, map.get(key));
        }
        //拼接字符串
        StringBuilder stringBuilder = new StringBuilder();
        for (String key : treeMap.keySet()) {
            stringBuilder.append(key).append("=").append(treeMap.get(key)).append("&");
        }*/
        String sign3 = stringBuilder1.substring(0, stringBuilder1.length() - 1);

        System.out.println("验签前参数"+sign3);
        System.out.println("验签前签名"+sign1);
        //服务端使用client公钥sign验签
        boolean verify = SignUtils.verify(sign3, sign1, SecretKeyConstants.clientPublicKEY);
        System.out.println(verify);
    }
}
