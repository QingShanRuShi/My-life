package com.zxr.provider.jdk.dynamicProxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Description: cglib动态代理
 * @Author: Zheng Xinrui
 * @Date: 23:23 2022/7/19
 */
public class CglibDynamicProxy implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("cglib动态代理开始了");
        Object invoke = methodProxy.invokeSuper(o, objects);
        System.out.println("cglib动态代理结束了");
        return invoke;
    }

}
