package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description: 借鉴redisTemplate源码用法
 * @Author: Zheng Xinrui
 * @Date: 18:41 2020/8/20
 */
public class TestFunction {

    public static void main(String[] args) {
        String desc = "金毛, ";
        int age = 10;
        String desc2 = "哈士奇, ";
        int age2 = 15;
        function((dog -> {dog.jinmao(desc, age); dog.hashiqi(desc2, age2);}), true);
    }

    private static void function(FunctionInterface functionInterface, boolean b){
        Dog dog;
        if(b){
            dog = new Dog1();
        }else {
            dog = new Dog2();
        }
        functionInterface.function(dog);
    }
}
