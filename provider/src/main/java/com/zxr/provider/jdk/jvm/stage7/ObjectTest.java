package com.zxr.provider.jdk.jvm.stage7;

/**
 * @Description: 对象实例化过程
 *          javap反编译class后，得到字节码文件，其中：
 *          0: new           #2                  // class java/lang/Object
 *          3: dup
 *          4: invokespecial #1                  // Method java/lang/Object."<init>":()V
 *          就是obj对象的实例化过程
 * @Author: Zheng Xinrui
 * @Date: 21:46 2022/2/24
 */
public class ObjectTest {


    public static void main(String[] args) {
        Object obj = new Object();
    }

    /*
    * 对象实例化步骤：
    * 1、判断对象对应的class是否加载、链接、初始化，没有的话使用类加载器双亲委派机制下加载并生成对象
    * 2、为对象分配内存，处理并发问题
    * 3、初始化分配到的空间，即对象的属性隐式赋予默认值（如int默认值0）
    * 4、执行init方法进行初始化，该步骤是进行初始化属性显式复制（int a=1这种）、静态代码块执行、调用类构造方法
    * */
}
