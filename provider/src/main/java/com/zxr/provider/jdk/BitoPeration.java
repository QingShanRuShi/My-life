package com.zxr.provider.jdk;

import java.util.Scanner;

/**
 * @description: 位运算，二进制运算
 *
 * @author: ZhengXinrui
 * @date: 2018年12月21日 上午11:39:28
 */
public class BitoPeration {

	public static void main(String[] args) {
		rightMove();
	}


	/**
	 *
	 * @description: &运算，同一位数都为1则为1，有0则0
	 * 			     |运算，与&相反，有1则1
	 *
	 */
	public static void qiePeration() {
		// 求奇偶
		System.out.println("请输入整数：");
		Scanner scanner = new Scanner(System.in);
		int num = scanner.nextInt();
		if ((num & 1) == 0) { // num=2   0010 & 0001 = 0000=0
			System.out.println(num + "偶数");
		} else {  // num=2   0011 & 0001 = 0001=1 只会等于0或者1，等同于取模判断奇偶
			System.out.println(num + "奇数");
		}
		scanner.close();
	}

	/**
	 *
	 * @description: ^异或运算， 同一位数相反（0和1）则为1，反之为0
	 *
	 */
	public static void yihuoPeration() {
		//交换两个数
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入第一个数1：");
		int num1 = scanner.nextInt();
		System.out.println("请输入第二个数2：");
		int num2 = scanner.nextInt();
		System.out.println("num1:"+num1+" num2:"+num2);
		System.out.println("开始转换");
		num1 ^= num2;
		num2 ^= num1;
		num1 ^= num2;
		System.out.println("num1:"+num1+" num2:"+num2);
		scanner.close();
	}


	public static void rightMovePeration() {
		//取相反数
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入");
		int num1 = scanner.nextInt();
		num1 = (~num1+1);
		System.out.println(num1);
		scanner.close();
	}

	//左移, 将二进制向左移动指定位数，右边不足补零，相当于乘以二操作(value*(2的幂次方)，幂等于移位的数)
	public static void leftMove() {
		int aValue =10;
		show(aValue);
		Integer leftMoveValue1 =aValue <<1; //左移1位，10*(2的1次方)
		show(leftMoveValue1);
		Integer leftMoveValue2 =aValue <<2; //左移2位，10*(2的2次方)
		show(leftMoveValue2);
		Integer leftMoveValue3 =aValue <<3;//左移3位，10*2(的3次方)
		show(leftMoveValue3);
	}
	//右移，将二进制向右移动指定位数，右边的数会被挤掉，左边不足补零，相当于除以2向零取整操作(value/(2的幂次方)，幂等于移位的数)
	public static void rightMove() {
		int aValue =10;
		show(aValue);
		Integer leftMoveValue1 =aValue >>1; //右移1位，10/(2的1次方)
		show(leftMoveValue1);
		Integer leftMoveValue2 =aValue >>2; //右移2位，10/(2的2次方)
		show(leftMoveValue2);
		Integer leftMoveValue3 =aValue >>3;//右移3位，10/(2的3次方)
		show(leftMoveValue3);
		Integer leftMoveValue4 =aValue >>4;//右移3位，10/(2的4次方)
		show(leftMoveValue4);
		Integer leftMoveValue5 =aValue >>5;//右移3位，10/(2的5次方)
		show(leftMoveValue5);
	}
	/**
	 * 打印数字及对应的二进制字符串
	 */
	public static void show(Integer number)
	{
		//打印原值
		System.out.println(number);
		//打印值对应的二进制
		System.out.println(Integer.toBinaryString(number));
	}

}
