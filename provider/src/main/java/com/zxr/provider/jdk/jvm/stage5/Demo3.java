package com.zxr.provider.jdk.jvm.stage5;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: gc日志分析
 *      测试MinorGC、MajorGC、FullGC
 *      -Xms9m -Xmx9m -XX:+PrintGCDetails
 *      执行main后很快oom异常，并打印了gc日志与堆空间信息：
 *      [GC (Allocation Failure) [PSYoungGen: 2048K->504K(2560K)] 2048K->1044K(9728K), 0.0006453 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
 *      [PSYoungGen: 2048K->504K(2560K)] 新生代总大小2560k，当前被占用大小2048k，回收后被占用大小504k
 *      2048K->1044K(9728K) 堆空间在gc之前被占用的大小2048k，堆空间gc之后的被占用大小1044k，堆空间总大小9728k
 *      0.0006453 secs 该次gc耗时
 *      [Full GC (Ergonomics) [PSYoungGen: 504K->0K(2560K)] [ParOldGen: 6307K->4448K(7168K)] 6811K->4448K(9728K), [Metaspace: 3482K->3482K(1056768K)], 0.0060811 secs] [Times: user=0.16 sys=0.00, real=0.01 secs]
 *      [ParOldGen: 6307K->4448K(7168K)] 老年代总大小7168k，老年代被占用大小6307k，回收后老年代被占用大小4448k
 *      [Metaspace: 3482K->3482K(1056768K)] 元空间总大小1056768k，元空间被占用大小3482k，回收后元空间被占用大小3482k
 *
 * @Author: Zheng Xinrui
 * @Date: 21:46 2022/2/22
 */
public class Demo3 {

    public static void main(String[] args) {
        int i = 0;
        try {
            List<String> list = new ArrayList<>();
            String a = "xxxxxxxxxxxxxzxr";
            while (true) {
                list.add(a);
                a = a + a;
                i++;
            }
        } catch (Throwable t) {
            t.printStackTrace();
            System.out.println("遍历次数为：" + i);
        }
    }
}
