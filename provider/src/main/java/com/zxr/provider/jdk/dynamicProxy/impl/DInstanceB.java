package com.zxr.provider.jdk.dynamicProxy.impl;


import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: BeanB
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public class DInstanceB {

    public DInstanceB() {
        System.out.println("InstanceB实例化");
    }

    public String sayB() {
        System.out.println("I'm B");
        return "DInstanceB.sayB()";
    }

}
