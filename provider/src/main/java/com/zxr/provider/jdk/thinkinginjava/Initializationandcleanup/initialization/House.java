package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description: 证明变量的位置，不影响和其他方法执行的顺序，因为变量总是会先与任何方法之前得到初始化，就算是散布于方法的各个位置
 * @Author: Zheng Xinrui
 * @Date: 20:04 2021/2/23
 */
public class House {

    Window w1 = new Window(1);

    House(){
        System.out.println("house()");
        w3 = new Window(33);
    }

    Window w2 = new Window(2);

    void f(){
        System.out.println("f()");
    }

    Window w3 = new Window(3);
}
