package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 18:36 2020/8/20
 */
public interface Dog {

        void jinmao(String desc, int age);

        void hashiqi(String desc, int age);

        void bianmu(String desc, int age);

        void demu(String desc, int age);
}
