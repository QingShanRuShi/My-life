package com.zxr.provider.jdk.lambda.advanced;

import org.springframework.lang.Nullable;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:54 2020/8/25
 */
@FunctionalInterface
public interface SuccessFunction<T> {

    void onSuccess(@Nullable T result);
}
