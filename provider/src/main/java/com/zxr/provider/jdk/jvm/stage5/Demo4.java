package com.zxr.provider.jdk.jvm.stage5;

/**
 * @Description: 矢量替换
 *
 *  以下代码alloc()方法会被替换成这样，进而将Point对象拆成局部矢量，交由栈管理
 *  private static void alloc() {
 *     int x = 1;
 *     int y = 2;
 *     System.out.println("point.x = " + x + "; point.y=" + y);
 * }
 * @Author: Zheng Xinrui
 * @Date: 18:07 2022/2/23
 */
public class Demo4 {

    public static void main(String args[]) {
        alloc();
    }

    private static void alloc() {
        Point point = new Point(1,2);
        System.out.println("point.x" + point.x + ";point.y" + point.y);
    }

    static class Point {
        private int x;
        private int y;

        Point(int x, int y){
            this.x = x;
            this.y = y;
        }
    }
}
