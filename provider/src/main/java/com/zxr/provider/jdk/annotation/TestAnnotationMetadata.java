package com.zxr.provider.jdk.annotation;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.zxr.providerapi.dto.UserDTO;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 15:59 2021/9/29
 */
public class TestAnnotationMetadata {

    //获取指定文件下所有.class文件的 Class
    public static void getAllClass() throws IOException, ClassNotFoundException {
        PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new
                PathMatchingResourcePatternResolver();
        CachingMetadataReaderFactory cachingMetadataReaderFactory = new CachingMetadataReaderFactory();
        Resource[] resources = pathMatchingResourcePatternResolver.getResources("classpath*:com/zxr/provider/service/impl/*.class");
        ClassLoader loader = ClassLoader.getSystemClassLoader();
        for (Resource resource : resources) {
            System.out.println("========================begin============================");
            //获取class对象
            MetadataReader reader = cachingMetadataReaderFactory.getMetadataReader(resource);
            String className = reader.getClassMetadata().getClassName();
            Class<?> loadClass = ClassUtils.forName(className, loader);
            System.out.println("全类名: " + loadClass);
            //获取类上所有注解
            System.out.println("类上的注解: " + Arrays.toString(loadClass.getAnnotations()));
            //获取指定方法上注解
            Method updUser = null;
            try {
                updUser = loadClass.getMethod("updUser", UserDTO.class);
            } catch (NoSuchMethodException e) {
                System.out.println("未找到updUser方法");
            }
            if (updUser != null) {
                Annotation[] annotations = updUser.getAnnotations();
                System.out.println("updUser: " + Arrays.toString(annotations));
            }
            //获取类所有方法上注解
            System.out.println("==================方法上的注解==================");
            Method[] methods = loadClass.getMethods();
            if (methods.length > 0) {
                for (Method method : methods) {
                    Annotation[] annotations = method.getAnnotations();
                    if(annotations.length > 0){
                        System.out.println(method.getName() + ": " + Arrays.toString(annotations));
                    }
                }
            }
            //获取类中属性上的注解
            System.out.println("===============属性上的注解===================");
            Field[] fields = loadClass.getDeclaredFields();
            if (fields.length > 0){
                for (Field field : fields) {
                    Annotation[] annotations = field.getDeclaredAnnotations();
                    if(annotations.length > 0){
                        System.out.println("属性修饰符：" + Modifier.toString(field.getModifiers()));
                        System.out.println("属性类型：" + field.getType().getCanonicalName());
                        System.out.println(field.getName() + ": " + Arrays.toString(annotations));
                    }
                }
            }
            System.out.println("=======================end=============================");
        }
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        getAllClass();
//        CachingMetadataReaderFactory readerFactory = new CachingMetadataReaderFactory();
//        // 下面两种初始化方式都可，效果一样
//        //MetadataReader metadataReader = readerFactory.getMetadataReader(MetaDemo.class.getName());
//        MetadataReader metadataReader = readerFactory.getMetadataReader(new ClassPathResource("com\\zxr\\provider\\ProviderApplication.class"));
//
//        ClassMetadata classMetadata = metadataReader.getClassMetadata();
//        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
//        Resource resource = metadataReader.getResource();
//
//        Set<String> metaAnnotationTypes = annotationMetadata.getMetaAnnotationTypes(DubboComponentScan.class.getName());
//        System.out.println(metaAnnotationTypes);
//
//        System.out.println(classMetadata); // org.springframework.core.type.classreading.AnnotationMetadataReadingVisitor@79079097
//        System.out.println(annotationMetadata); // org.springframework.core.type.classreading.AnnotationMetadataReadingVisitor@79079097
//        System.out.println(resource); // class path resource [com/fsx/maintest/MetaDemo.class]

    }
}
