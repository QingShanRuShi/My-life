package com.zxr.provider.jdk.spring.circulardependencies;


/**
 * @Description: 顶层接口
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public interface IApi {

	void say();
}
