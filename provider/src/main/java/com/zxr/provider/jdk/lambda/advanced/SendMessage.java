package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:59 2020/8/25
 */
public class SendMessage<T> implements SendMessageInterface<T>{

    private String result;

    private Throwable throwable;

    public void setResult(String result) {
        this.result = result;
    }

    public void setThrowable(Throwable throwable){
        this.throwable = throwable;
    }

    @Override
    public void addCallback(SuccessFunction<? super T> successFunction, FailureFunction failureFunction, Boolean b) {
        if(b){
            setResult("功能使用成功");
            if(successFunction != null) successFunction.onSuccess((T) this.result);
        }else {
            setThrowable(new RuntimeException("功能使用异常，原因是xxxx"));
            if(failureFunction != null) failureFunction.onFailure(throwable);
        }
    }
}
