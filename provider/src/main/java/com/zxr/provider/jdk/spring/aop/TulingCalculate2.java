package com.zxr.provider.jdk.spring.aop;


import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;


/**
 * @Description: 计算器2
 * @Author: Zheng Xinrui
 * @Date: 23:46 2022/6/24
 */
@Component
public class TulingCalculate2 implements Calculate {

    public int add(int numA, int numB) {

        System.out.println("计算器2，执行目标方法:add");
        System.out.println("计算器2，" + numA/numB);
        return numA+numB;
    }

    public int sub(int numA, int numB) {
        System.out.println("计算器2，执行目标方法:reduce");
        return numA-numB;
    }

    public int div(int numA, int numB) {
        System.out.println("计算器2，执行目标方法:div");
        return numA/numB;
    }

    public int multi(int numA, int numB) {
        System.out.println("计算器2，执行目标方法:multi");

        return numA*numB;
    }

    public int mod(int numA,int numB){
        System.out.println("执行目标方法:mod");

		int retVal = ((Calculate) AopContext.currentProxy()).add(numA,numB);
        //int retVal = this.add(numA,numB);

        return retVal%numA;

        //return numA%numB;
    }

}
