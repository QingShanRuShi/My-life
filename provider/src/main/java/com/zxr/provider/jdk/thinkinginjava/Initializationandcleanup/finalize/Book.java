package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.finalize;

/**
 * @Description: 垃圾回收之前，总是调用finalize方法
 * @Author: Zheng Xinrui
 * @Date: 19:23 2021/2/23
 */
public class Book {

    boolean checkedOut = false;

    Book(boolean checkedOut){
        this.checkedOut = checkedOut;
    }

    void checkIn(){
        checkedOut = false;
    }

    @Override
    protected void finalize() throws Throwable {
        if(checkedOut){
            System.out.println("错误：check out");
        }
//        super.finalize();
    }
}
