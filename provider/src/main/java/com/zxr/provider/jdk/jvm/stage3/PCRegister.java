package com.zxr.provider.jdk.jvm.stage3;

/**
 * @Description: pc寄存器（程序计数器）
 * @Author: Zheng Xinrui
 * @Date: 17:17 2021/5/7
 */
public class PCRegister {

    public static void main(String[] args) {
        int i = 10;
        int j = 20;
        int k = i + j;
    }

    /*
    * 反编译字节码得到如下内容，其中竖排左边的数字，0，2,3,4,5,6,7,8,9就是pc寄存器存储的当前线程的指令地址（指令偏移）
    * 然后执行引擎读取 PC 寄存器中的值，并执行该指令
    * */
//    public static void main(java.lang.String[]);
//    descriptor: ([Ljava/lang/String;)V
//    flags: ACC_PUBLIC, ACC_STATIC
//    Code:
//    stack=2, locals=4, args_size=1
//         0: bipush        10
//         2: istore_1
//         3: bipush        20
//         5: istore_2
//         6: iload_1
//         7: iload_2
//         8: iadd
//         9: istore_3
//        10: return

}
