package com.zxr.provider.jdk.dynamicProxy;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * @Description: jdk动态代理
 * @Author: Zheng Xinrui
 * @Date: 22:30 2022/7/19
 */
public class JdkDynamicProxy<T> implements InvocationHandler, Serializable {

	private static final long serialVersionUID = -7334771077505569345L;
	private final Class<T> target;

	public Class<T> getTarget() {
		return target;
	}

	public JdkDynamicProxy(Class<T> target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (target.isInterface()) {
			System.out.println("jdk动态代理interface开始");
			System.out.println("干想干的事儿");
			System.out.println("jdk动态代理interface结束");
			System.out.println("interface方法无法执行");
			return null;
		}else {
			System.out.println("jdk动态代理开始");
			Object invoke = method.invoke(target.newInstance(), args);
			System.out.println("jdk动态代理结束");
			return invoke;
		}
	}
}
