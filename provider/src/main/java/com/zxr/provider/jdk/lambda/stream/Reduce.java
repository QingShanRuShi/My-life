package com.zxr.provider.jdk.lambda.stream;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @Description: stream reduce操作
 * @Author: Zheng Xinrui
 * @Date: 10:41 2020/12/16
 */
public class Reduce {

    public static void main(String[] args) {
        reduceTest1();
        System.out.println("===================================================");
        System.out.println();
        System.out.println();
        reduceTest2();
        System.out.println("===================================================");
        System.out.println();
        System.out.println();
        reduceTest3();
    }

    //Optional<T> reduce(BinaryOperator<T> accumulator);
    //reduce(BinaryOperator<T> accumulator)方法需要一个函数式接口参数，该函数式接口需要两个参数，返回一个结果(reduce中返回的结果会作为下次累加器计算的第一个参数)，也就是累加器
    public static void reduceTest1() {
        //这里reduce里的acc和item两个，未指定reduce初始值，那么acc就是自动取的流中的第一个数即1，item取的第二个即2
        //每次函数式返回之后，acc的值就是函数值return的值，item会自动往流后面取数值
        Optional accResult = Stream.of(1, 2, 3, 4).reduce((acc, item) -> {
            System.out.println("acc : " + acc);
            acc += item;
            System.out.println("item: " + item);
            System.out.println("acc+ : " + acc);
            System.out.println("--------");
            return acc;
        });
        System.out.println(accResult);
    }

    //T reduce(T identity, BinaryOperator<T> accumulator);
    //提供一个跟Stream中数据同类型的初始值identity，通过累加器accumulator迭代计算Stream中的数据，得到一个跟Stream中数据相同类型的最终结果
    public static void reduceTest2() {

        //第一个identify 100就是给后面的函数式中第一个参数即acc赋予初值，此时acc=100，item=1
        // 后面的逻辑就是正常遍历item不断取流后续的值，acc一直是函数式的返回值
        int accResult = Stream.of(1, 2, 3, 4)
                .reduce(100, (acc, item) -> {
                    System.out.println("acc : " + acc);
                    acc += item;
                    System.out.println("item: " + item);
                    System.out.println("acc+ : " + acc);
                    System.out.println("--------");
                    return acc;
                });
        System.out.println(accResult);
    }

    //<U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner);
    //提供一个不同于Stream中数据类型的初始值，通过累加器规则迭代计算Stream中的数据，最终得到一个同初始值同类型的结果
    public static void reduceTest3() {
        ArrayList<Integer> newList = new ArrayList<>();

        //newList给acc赋予初始值，item顺序取流的值，acc一直接收函数式返回的值，最后返回newList定义的类型对象
        //第三个参数(acc, item) -> null定义的规则并没有执行。这是因为reduce的第三个参数是在使用parallelStream的reduce操作时，合并各个流结果的，本例中使用的是stream，所以第三个参数是不起作用的
        ArrayList<Integer> accResult_ = Stream.of(1,2, 3, 4)
                .reduce(newList,
                        (acc, item) -> {
                            acc.add(item);
                            System.out.println("item: " + item);
                            System.out.println("acc+ : " + acc);
                            System.out.println("--------");
                            return acc;
                        }, (acc, item) -> null);
        System.out.println("accResult_: " + accResult_);
    }
}
