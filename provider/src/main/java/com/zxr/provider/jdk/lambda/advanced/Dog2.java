package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 18:38 2020/8/20
 */
public class Dog2 implements Dog {
    @Override
    public void jinmao(String desc, int age) {
        System.out.println(this.getClass() + ", 我是：" + desc + "现在："+age + "岁");
    }

    @Override
    public void hashiqi(String desc, int age) {
        System.out.println(this.getClass() + ", 我是：" + desc + "现在："+age + "岁");
    }

    @Override
    public void bianmu(String desc, int age) {
        System.out.println(this.getClass() + ", 我是：" + desc + "现在："+age + "岁");
    }

    @Override
    public void demu(String desc, int age) {
        System.out.println(this.getClass() + ", 我是：" + desc + "现在："+age + "岁");
    }
}
