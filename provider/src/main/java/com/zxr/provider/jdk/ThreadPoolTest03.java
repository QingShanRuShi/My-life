package com.zxr.provider.jdk;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description: ExecutorService线程池使用之newSingleThreadExecutor
 * 
 * @author: ZhengXinrui
 * @date: 2018年12月3日 下午4:34:49
 */
public class ThreadPoolTest03 {
	
	/**
	 *  newSingleThreadExecutor线程池：
	 * 
	 *  - 线程中只有一个线程在执行 
	 *	- 适用于有明确执行顺序但是不影响主线程的任务，压入池中的任务会按照队列顺序执行。 
	 *	- 使用无界的LinkedBlockingQueue，要综合考虑生成与消费能力，生成过剩，可能导致堆内存溢出。
	 *
	 */
	public static void main(String[] args) {
        ExecutorService pool = Executors.newSingleThreadExecutor();
        for(int i = 0 ; i < 50 ; i++){
            pool.submit(new ThreadRunnable((i + 1)));
        }
        pool.shutdown();
    }

}

