package com.zxr.provider.jdk.test;

/**
 * @Description: 枚举学习，这里面method1和metod2都是单例
 * @Author: Zheng Xinrui
 * @Date: 17:03 2022/2/21
 */
public enum TestMethodEnum {

    mehtod1{
        public String testStr(String str) {
            return "实现1: "+str;
        }
        public long testLon(Long lon){
            return lon + 10;
        }
    },

    mehtod2{
        public String testStr(String str) {
            return "实现2: "+str;
        }
        public int testInt(Integer inte){
            return inte + 2;
        }
        public long testLon(Long lon){
            return lon + 20;
        }
    };

    public String testStr(String str){
        throw new RuntimeException();
    }

    // 普通方法可以选择性多实现
    public int testInt(Integer inte){
        throw new RuntimeException();
    }

    // 设置成抽象类之后每个都要实现它
    public abstract long testLon(Long lon);

    public static final int a = 10;

    public void aa(String str, Integer inte, Long lon){
        System.out.println(testStr(str));
        System.out.println(testInt(inte));
        System.out.println(testLon(lon));
    }
}
