package com.zxr.provider.jdk.jvm.stage1;

/**
 * @Description: 字节码初体验, jvm生命周期
 * @Author: Zheng Xinrui
 * @Date: 16:06 2021/5/6
 */
public class StackStruTest {


    public static void main(String[] args) throws InterruptedException {
        int i = 2;
        int j = 3;
        int k = i + j;

        //执行后在10s内控制台jps可以查看一个进程名叫做StackStruTest的进程及它的进程id
        Thread.sleep(10000);

        System.out.println("结束");
    }
    /*
    * 1.jvm字节码初体验，编译之后执行javap -v StackStruTest.class，即可看到反解析的当前类对应的汇编指令、本地表量表等信息
    * 2.jvm生命周期：启动一个main就是启动一个jvm进程
    *   启动：启动是通过引导类加载器（bootstrap class loader）创建一个初始类（initial class）来完成的，这个类是由虚拟机的具体实现指定的。
    *   执行：
    *       1. 一个运行中的Java虚拟机有着一个清晰的任务：执行Java程序
            2. 程序开始执行时他才运行，程序结束时他就停止
            3. 执行一个所谓的Java程序的时候，真真正正在执行的是一个叫做Java虚拟机的进程
    *    退出：
    *       1. 程序正常执行结束
            2. 程序在执行过程中遇到了异常或错误而异常终止
            3. 由于操作系统用现错误而导致Java虚拟机进程终止
            4. 某线程调用Runtime类或System类的exit()方法，或Runtime类的halt()方法，并且Java安全管理器也允许这次exit()或halt()操作。
            5. 除此之外，JNI（Java Native Interface）规范描述了用JNI Invocation API来加载或卸载 Java虚拟机时，Java虚拟机的退出情况。
    * */
}
