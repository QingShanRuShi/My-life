package com.zxr.provider.jdk.jvm.stage6;

import java.io.Serializable;

/**
 * @Description: 测试方法区的内部构成
 *              编译后，进去class文件，执行命令javap -v -p MethodInnerStrucTest.class > test.txt，将字节码反编译结果输出到test.txt中
 *              在test.txt可以看到放到方法区的内容：
 *                  1. 方法名称
 *                  2. 方法的返回类型（包括 void 返回类型），void 在 Java 中对应的为 void.class
 *                  3. 方法参数的数量和类型（按顺序）
 *                  4. 方法的修饰符（public，private，protected，static，final，synchronized，native，abstract的一个子集）
 *                  5. 方法的字节码（bytecodes）、操作数栈、局部变量表及大小（abstract和native方法除外）
 *                  6. 异常表（abstract和native方法除外），异常表记录每个异常处理的开始位置、结束位置、代码处理在程序计数器中的偏移地址、被捕获的异常类的常量池索引
 *                 （ 7.在运行时方法区中，类信息中记录了哪个加载器加载了该类，同时类加载器也记录了它加载了哪些类）
 * @Author: Zheng Xinrui
 * @Date: 22:08 2022/2/23
 */
public class MethodInnerStrucTest extends Object implements Comparable<String>, Serializable {



    private static final long serialVersionUID = 4560913312151138116L;
    //属性
    public int num = 10;
    private static String str = "测试方法的内部结构";

    //构造器
    //方法
    public void test1(){
        int count = 20;
        System.out.println("count = " + count);
    }
    public static int test2(int cal){
        int result = 0;
        try {
            int value = 30;
            result = value / cal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }
}
