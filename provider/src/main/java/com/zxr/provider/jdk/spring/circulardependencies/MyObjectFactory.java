package com.zxr.provider.jdk.spring.circulardependencies;

import org.springframework.beans.BeansException;

/**
 * @Description: 三级缓存使用的函数式接口
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
@FunctionalInterface
public interface MyObjectFactory<T> {


    T getObject() throws BeansException;

}
