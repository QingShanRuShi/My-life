package com.zxr.provider.jdk;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @description: ExecutorService线程池使用之newFixedThreadPool
 * 
 * @author: ZhengXinrui
 * @date: 2018年12月3日 下午3:52:01
 */
public class ThreadPoolTest01 {
	
	
	/**
	 *  newFixedThreadPool线程池：
	 * 	
	 * 	- 池中线程数量固定，不会发生变化 
	 *  - 使用无界的LinkedBlockingQueue，要综合考虑生成与消费能力，生成过剩，可能导致堆内存溢出。 
	 *	- 适用一些很稳定很固定的正规并发线程，多用于服务器
	 * 
	 */
	public static void main(String[] args) {
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(4);
		for (int i = 0; i < 50; i++) {
			newFixedThreadPool.execute(new ThreadRunnable(i+1));
		}
		newFixedThreadPool.shutdown();
	}
	
}


class ThreadRunnable implements  Runnable{
	
    private final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
	
	private int num;
	
	public  ThreadRunnable(int num) {
		this.num = num;
	}

	@Override
	public void run() {
        System.out.println("thread:" + Thread.currentThread().getName() + ",time:" + format.format(new Date()) + ",num:" + num);
        try {//使线程睡眠，模拟线程阻塞情况
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}
	
}
