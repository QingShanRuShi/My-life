package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:54 2020/8/25
 */
@FunctionalInterface
public interface FailureFunction {

    void onFailure(Throwable te);
}
