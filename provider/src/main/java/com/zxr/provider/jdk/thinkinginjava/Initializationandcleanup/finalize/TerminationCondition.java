package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.finalize;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 19:25 2021/2/23
 */
public class TerminationCondition {

    public static void main(String[] args) {
        Book book = new Book(true);
        book.checkIn();

        new Book(true);
        //这只是提醒应该执行垃圾回收，但是并不一定执行，当产生垃圾回收时候，因为在finalize的判断中，还有对象未checkIn，所有停止垃圾回收
        System.gc();
    }
}
