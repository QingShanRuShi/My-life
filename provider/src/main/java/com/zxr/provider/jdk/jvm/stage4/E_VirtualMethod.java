package com.zxr.provider.jdk.jvm.stage4;

/**
 * @Description: 虚方法与非虚方法
 * 一个方法编译器被确定具体的调用版本内容，并且在运行时不可被改变，就是非虚方法，如静态方法，构造方法，final修饰的方法，私有方法，父类方法(super.方式调用)都是非虚方法
 * 不在上面内容内的方法，就是虚方法
 * 这里的虚方法和非虚方法，并不是说方法的性质，而是虚拟机调用该方法的方式，是通过虚方法方式调用还是非虚方法调用，如父类一个普通的public方法a，被正常a();使用时，就是虚方法， 子类通过super.a()；调用时，就是非虚方法。
 * @Author: Zheng Xinrui
 * @Date: 16:55 2021/5/25
 */
public class E_VirtualMethod {
}


class Father {
    public Father() {
        System.out.println("father的构造器");
    }

    public static void showStatic(String str) {
        System.out.println("father " + str);
    }

    public final void showFinal() {
        System.out.println("father show final");
    }

    public void showCommon() {
        System.out.println("father 普通方法");
    }
}

class Son extends Father {
    public Son() {
        //invokespecial
        super();
    }

    public Son(int age) {
        //invokespecial
        this();
    }

    //不是重写的父类的静态方法，因为静态方法不能被重写！
    public static void showStatic(String str) {
        System.out.println("son " + str);
    }

    private void showPrivate(String str) {
        System.out.println("son private" + str);
    }

    public void show() {
        //invokestatic
        showStatic("atguigu.com");
        //invokestatic
        super.showStatic("good!");
        //invokespecial
        showPrivate("hello!");
        //invokespecial
        super.showCommon();

        //invokevirtual
        showFinal();//因为此方法声明有final，不能被子类重写，所以也认为此方法是非虚方法。
        //虚方法如下：

        /*
        invokevirtual  你没有显示的加super.，编译器认为你可能调用子类的showCommon(即使son子类没有重写，也会认为)，所以编译期间确定不下来，就是虚方法。
        */
        showCommon();
        info();

        MethodInterface in = null;
        //invokeinterface
        in.methodA();
    }

    public void info() {

    }

    public void display(Father f) {
        f.showCommon();
    }

    public static void main(String[] args) {
        Son so = new Son();
        so.show();
    }
}

interface MethodInterface {
    void methodA();
}
