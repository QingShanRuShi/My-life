package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description: 校验对象成员变量的默认值
 * @Author: Zheng Xinrui
 * @Date: 19:47 2021/2/23
 */
public class InitialValues {

    boolean t;
    char c;
    byte b;
    short s;
    int i;
    long l;
    float f;
    double d;
    InitialValues initialValues;

    void printInitialValues(){
        System.out.println("boolean: "+t);
        System.out.println("char: "+c);
        System.out.println("byte: "+b);
        System.out.println("short: "+s);
        System.out.println("int: "+i);
        System.out.println("long: "+l);
        System.out.println("float: "+f);
        System.out.println("double: "+d);
        System.out.println("initialValues: "+initialValues);
    }
}
