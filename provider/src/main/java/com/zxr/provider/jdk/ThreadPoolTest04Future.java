package com.zxr.provider.jdk;

import java.util.concurrent.*;

/**
 * @description: 使用Future和Callable结合线程池
 * 				  创建线程的2种方式，一种是直接继承Thread，另外一种就是实现Runnable接口。 
 *				  这2种方式都有一个缺陷就是：在执行完任务之后无法获取执行结果。 
 *               如果需要获取执行结果，就必须通过共享变量或者使用线程通信的方式来达到效果，这样使用起来就比较麻烦。
 *               Future和Callable的出现就完美解决这个问题，并且除了获取线程返回值，还有获取线程状态功能
 * 
 * @author: ZhengXinrui
 * @date: 2018年12月13日 下午5:30:39
 */
public class ThreadPoolTest04Future {
	
	static class Thread implements Callable<Integer> {

		@Override
		public Integer call() throws Exception {
			// 
			int i = 0;
			for (int j = 0; j < 1000; j++) {
				System.out.println(i);
				i += j;
			}
			return i;
		}
		
	}

	/**
	 * @description: Future的get方法，会将其线程挂起，等待线程执行返回结果为止
	 *
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//
		long startTime = System.currentTimeMillis();
		System.out.println("startTime:"+startTime);
		ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
		FutureTask<Integer> futureTask = new FutureTask<>(new Thread());
		newSingleThreadExecutor.submit(futureTask);
		//使用Callable内部类实现方式
		/*Future<Integer> futureCallable = newSingleThreadExecutor.submit(new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				int i = 0;
				for (int j = 0; j < 1000; j++) {
					System.out.println(i);
					i += j;
				}
				return i;
			}
			
		});*/
		long endTime = System.currentTimeMillis();
		System.out.println("endTime:"+endTime);
		System.out.println("Future自己的get():"+futureTask.get());
		System.out.println("sumTime:"+(endTime-startTime));
	}

}

