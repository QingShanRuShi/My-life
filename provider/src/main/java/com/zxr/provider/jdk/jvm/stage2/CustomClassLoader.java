package com.zxr.provider.jdk.jvm.stage2;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

/**
 * @Description: 自定义类加载器
 * @Author: Zheng Xinrui
 * @Date: 10:48 2021/5/7
 */
public class CustomClassLoader extends ClassLoader{

    public CustomClassLoader() {
    }

    public CustomClassLoader(ClassLoader parent) {
        super(parent);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String Dir = "D:\\develop\\code\\stadyCode\\My-life\\provider";
        String classPath = Dir + File.separatorChar + name.replace('.', File.separatorChar) + ".class";
        try {
            byte[] result = getClassFromCustomPath(classPath);
            if (result == null) {
                throw new FileNotFoundException();
            } else {
                //defineClass和findClass搭配使用
                return this.defineClass(name, result, 0, result.length);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return super.findClass(name);
    }

    //自定义流的获取方式
    private byte[] getClassFromCustomPath(String name) {
        //从自定义路径中加载指定类:细节略
        //如果指定路径的字节码文件进行了加密，则需要在此方法中进行解密操作。
        File file = new File(name);
        // 这里要读入.class的字节，因此要使用字节流
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            FileChannel fc = fis.getChannel();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            WritableByteChannel wbc = Channels.newChannel(baos);
            ByteBuffer by = ByteBuffer.allocate(1024);

            while (true)
            {
                int i = fc.read(by);
                if (i == 0 || i == -1)
                    break;
                by.flip();
                wbc.write(by);
                by.clear();
            }

            fis.close();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        CustomClassLoader customClassLoader = new CustomClassLoader();
        try {
            //输入包名及类名即可加载
            Class<?> loadClass = customClassLoader.loadClass("com.zxr.provider.jdk.jvm.stage2.ClassLoadTest");
            System.out.println(loadClass.getClassLoader());
            System.out.println(loadClass.getName());
            System.out.println("=================");

            Class<?> c1 = Class.forName("com.zxr.provider.jdk.jvm.stage2.ClassLoadTest", true, customClassLoader);
            Object obj = c1.newInstance();
            System.out.println(obj);
            System.out.println(obj.getClass().getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
