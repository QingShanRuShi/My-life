package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:53 2020/8/25
 */
public interface SendMessageInterface<T> {

    void addCallback(SuccessFunction<? super T> successFunction, FailureFunction failureFunction, Boolean b);
}
