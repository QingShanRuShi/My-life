package com.zxr.provider.jdk.spring.circulardependencies;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description: 模拟三级缓存，简化版bean创建（getBean()），解决循环依赖
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public class MainStart {

    // 一级缓存 单例池   成熟态Bean
    private static final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    // 二级缓存   纯净态Bean (存储不完整的Bean用于解决循环依赖中多线程读取一级缓存的脏数据)
    // 所以当有了三级缓存后，它还一定要存在，因为它要存储的 aop创建的动态代理对象，且不需要重复创建
    private static final Map<String, Object> earlySingletonObjects = new ConcurrentHashMap<>(256);

    // 三级缓存 给每一个Bean设置一个动态代理函数式接口，发生依赖循环时使用该动态代理函数接口提前进行aop封装
    private static final Map<String, MyObjectFactory<Object>> factoryEarlySingletonObjects = new ConcurrentHashMap<>(256);

    // 标识当前bean正在创建，如果正在创建并且从一级缓存中没有拿到是不是说明是依赖
    private static final Set<String> singletonsCurrentlyInCreation = Collections.newSetFromMap(new ConcurrentHashMap<>(16));

    /**
     * 创建Bean
	 *
     * @param beanName Bean名字
     * @return 完全实例化之后的Bean
     */
    private static Object getBean(String beanName) throws Exception {

        // 从缓存中获取是否已经创建了bean
        Object bean = getSingleton(beanName);
        if(bean!=null){
            return bean;
        }

        // 开始创建Bean
		singletonsCurrentlyInCreation.add(beanName);

        // 1.实例化，通过无参构造函数实例化
		Class<?> beanClass = Class.forName(beanName);
        Object beanInstant = beanClass.newInstance();

        // 到这里，该bean只是被初始化，并没有完成属性赋值
        // 当前bean在三级缓存中初始有动态代理函数式接口的PostProcessor，作用是当有循环依赖时保证产生依赖的Bean实例化完整
		Object finalBeanInstant = beanInstant;
		factoryEarlySingletonObjects.put(beanName, () -> getEarlyBeanReference(beanName, finalBeanInstant));

        // 2.属性赋值 解析Autowired
        // 拿到所有的属性名
        Field[] declaredFields = beanClass.getDeclaredFields();
        // 循环所有属性
        for (Field declaredField : declaredFields) {
            // 从属性上拿到@Autowired，如果有，则该属性需要实例化（getBean()）并且注入（属性bean实例化后赋值）
            Autowired annotation = declaredField.getAnnotation(Autowired.class);
            if(annotation != null){
            	declaredField.setAccessible(true);
                Class<?> type = declaredField.getType();

                //递归拿到@Autowired修饰的属性实例bean，并赋值到当前实例的该属性上
				Object fileBean = getBean(type.getName());

				declaredField.set(beanInstant, fileBean);
			}

        }
        // 3.初始化（省略）
        // 4.后置处理器将没有循环依赖的有aop的bean创建动态代理

        // 从二级缓存中获取最完整的bean（即发生循环依赖，二级缓存必有当前bean的实例，且该实例是经过了动态代理（如果需要的话））
		if(earlySingletonObjects.containsKey(beanName)){
			beanInstant = earlySingletonObjects.get(beanName);
		}
		// 存入到一级缓存
        singletonObjects.put(beanName, beanInstant);
        return beanInstant;
    }

	// 从三个缓存池中获取bean
    private  static Object getSingleton(String beanName) {
        // 优先从一级缓存中取
        Object bean = singletonObjects.get(beanName);
        // 如果一级缓存没有拿到且正在创建，就说明当前是循环依赖创建
        // 只是循环依赖才创建动态代理, 解决 aop下面循环依赖
        if (bean == null && singletonsCurrentlyInCreation.contains(beanName)) {
            // 从二级缓存中取一次，防止BeanA与BeanB、BeanC...等等同时有多个循环依赖导致重复创建的问题
            bean = earlySingletonObjects.get(beanName);
            if (bean == null) {
                // 调用bean的后置处理器创建动态代理，并放入二级缓存中，然后清理三级缓存
                MyObjectFactory<Object> factory = factoryEarlySingletonObjects.get(beanName);
                bean = factory.getObject();
                earlySingletonObjects.put(beanName, bean);
                factoryEarlySingletonObjects.remove(beanName);
            }
        }
        return bean;
    }

    // 自定义动态代理后置处理器
    private  static  Object getEarlyBeanReference(String beanName, Object bean){
		JdkProxyBeanPostProcessor beanPostProcessor = new JdkProxyBeanPostProcessor();
		//这里是模仿spring后置处理器调用方式，在List<xxxBeanPostProcessor>中，
		//找到实现了指定SmartInstantiationAwareBeanPostProcessor的xxxxBeanPostProcessor进行执行
		if(beanPostProcessor instanceof SmartInstantiationAwareBeanPostProcessor){
			bean = beanPostProcessor.getEarlyBeanReference(bean, beanName);
		}
    	return bean;
    }

    // 运行测试，如果没有解决循环依赖，会一直递归死循环最终异常停止
    // 这里权作为三级缓存解决循环依赖的demo示例，代码中动态代理的逻辑未完善跑不通，去掉动态代理部分正常
    // 需要jdk动态代理和cglib动态代理互相结合使用，才能覆盖所有属性依赖注入有动态代理的问题
	public static void main(String[] args) throws Exception {

		// 已经加载循环依赖
		String beanName="com.zxr.provider.jdk.spring.circulardependencies.InstanceA";

		InstanceA a= (InstanceA) getBean(beanName);

		a.say();

		singletonObjects.forEach((k, v) -> System.out.println("k="+k + "  v="+v));
	}

}
