package com.zxr.provider.jdk.dynamicProxy;


import java.lang.reflect.Proxy;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 22:42 2022/7/19
 */
public class JdkDynamicFactory<T> {

    private final Class<T> target;

    public JdkDynamicFactory(Class<T> target) {
        this.target = target;
    }

    public Class<T> getTarget() {
        return target;
    }

    @SuppressWarnings("unchecked")
    private T newInstance(JdkDynamicProxy<T> jdkDynamicProxy) {
        if(target.isInterface()){
            return (T) Proxy.newProxyInstance(target.getClassLoader(), new Class[] { target }, jdkDynamicProxy);
        }else {
            return (T) Proxy.newProxyInstance(target.getClassLoader(), target.getInterfaces(), jdkDynamicProxy);
        }
    }

    public T newInstance() {
        final JdkDynamicProxy<T> tJdkDynamicProxy = new JdkDynamicProxy<>(target);
        return newInstance(tJdkDynamicProxy);
    }
}
