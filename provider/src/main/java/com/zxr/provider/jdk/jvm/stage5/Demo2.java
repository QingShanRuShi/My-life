package com.zxr.provider.jdk.jvm.stage5;

import java.util.concurrent.TimeUnit;

/**
 * @Description: 设置堆空间大小，然后执行，使用jdk/bin中的jvisualvm.exe工具查看jvm进程堆空间信息
 *               -Xms20m -Xmx20m
 * @Author: Zheng Xinrui
 * @Date: 16:32 2022/2/21
 */
public class Demo2 {

    public static void main(String[] args) {
        System.out.println("start...");

        try {
            Thread.sleep(600000);
            TimeUnit.MINUTES.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("end....");
    }
}
