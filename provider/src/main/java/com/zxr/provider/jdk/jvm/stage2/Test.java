package com.zxr.provider.jdk.jvm.stage2;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 11:45 2021/5/7
 */
public class Test {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        CustomClassLoader customClassLoader = new CustomClassLoader();
        Class<?> c1 = Class.forName("com.zxr.provider.domain.Log", true, customClassLoader);
        Object obj = c1.newInstance();
        System.out.println(obj);
        System.out.println(obj.getClass().getClassLoader());
    }
}
