package com.zxr.provider.jdk.lambda.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @description: jdk1.8新特性之Stream高级应用
 * 
 * @author: ZhengXinrui
 * @date: 2019年1月9日 上午10:49:08
 */
public class StreamTest02 {

	private enum Status {
		OPEN, CLOSED
	};

	private static final class Task {
		private final Status status;
		private final Integer points;

		Task(final Status status, final Integer points) {
			this.status = status;
			this.points = points;
		}

		public Integer getPoints() {
			return points;
		}

		public Status getStatus() {
			return status;
		}

		@Override
		public String toString() {
			return String.format("[%s, %d]", status, points);
		}
	}

	public static void main(String[] args) {
		final Collection<Task> tasksList = Arrays.asList(new Task(Status.OPEN, 5), new Task(Status.OPEN, 13),
				new Task(Status.CLOSED, 8));

		// 过滤并求和
		final long totalPointsOfOpenTasks = tasksList
				.stream()
				.filter(task -> task.getStatus() == Status.OPEN) // 过滤掉status不等于open的
				.mapToInt(Task::getPoints) // 遍历取出point生成Integer集合stream流
				.sum(); // 求和
		System.out.println("Total points: " + totalPointsOfOpenTasks);

		// 是用并行处理并使用reduce
		/**
		 * Reduce中文含义为：减少、缩小；而Stream中的Reduce方法干的正是这样的活：
		 * 根据一定的规则将Stream中的元素进行计算后返回一个唯一的值。
		 */
		final Integer totalPoints = tasksList
				.stream()
				.parallel() // 开启并行处理
				.map(task -> task.getPoints()) // or map( Task::getPoints ) 取出所有points
				.reduce(0, Integer::sum); // 将上面的取出的所有points，以零为基数求和
		System.out.println("Total points (all tasks): " + totalPoints);
		
		//对于一个集合，经常需要根据某些条件对其中的元素分组
		final Map< Status, List< Task > > map = tasksList
			    .stream()
			    .collect( Collectors.groupingBy( Task::getStatus ) );
		System.out.println( map );
		
	}

}
