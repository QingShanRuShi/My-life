package com.zxr.provider.jdk.lambda;

/**
 * @Description: lambda函数式编程测试
 * @Author: Zheng Xinrui
 * @Date: 18:10 2020/8/20
 */
public class TestFunction {

    public static void main(String[] args) {
        function(() -> {
            System.out.println("这里也是无参数无返回的lambda函数式编程");
        });

        function((num, num2) -> {
            System.out.println("这里也是有指定类型的参数无返回的lambda函数式编程：" + num + ", " + num2);
        });

        function((num) -> {
            System.out.println("这里也是有指定类型的参数有返回的lambda函数式编程：" + num);
            return num;
        });

        function((Boolean str) -> {
            System.out.println("这里也是有指定类型的参数无返回的lambda函数式编程：" + str);
        });
    }

    private static void function(FunctionInterface1 functionInterface1){
        System.out.println("这里是无参数无返回的lambda函数式编程");
        functionInterface1.function();
        System.out.println("==============================");
    }

    private static void function(FunctionInterface2 functionInterface2){
        String num = "hhhhhhhhh";
        int num2 = 222222;
        System.out.println("这里是有指定类型的参数无返回的lambda函数式编程");
        functionInterface2.function(num, num2);
        System.out.println("==============================");
    }

    private static void function(FunctionInterface3 functionInterface3){
        int num = 3333;
        int function = functionInterface3.function(num);
        System.out.println("这里是有指定类型的参数有返回的lambda函数式编程: " + function);
        System.out.println("==============================");
    }

    private static void function(FunctionInterface4<Boolean> functionInterface4){
        boolean str = false;
        functionInterface4.function(str);
        System.out.println("这里是无类型的参数无返回的lambda函数式编程");
        System.out.println("===============================");
    }

}
