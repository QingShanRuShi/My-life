package com.zxr.provider.jdk.lambda.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description: jdk1.8新特性之Stream
 * 
 * @author: ZhengXinrui
 * @date: 2018年12月29日 下午1:38:54
 */
public class StreamTest01 {

	public static void main(String args[]) {
		List<Student> list = studentsList();
		System.out.println("-------------过滤--------------");
		// 过滤筛选出满足条件的，然后遍历输出
		list.stream() // 初始化steam
			    .filter(studnet -> studnet.getSex().equals("M")) // intermediate 中间过程，是一个懒加载
				.forEach(System.out::println); // terminal 终端,结束化，当结束的时候，intermediate才真正运行
		// 另一种实现方式
		List<Student> collectStudentList = list.stream().filter(studnet -> studnet.getSex().equals("M"))
				.collect(Collectors.toList());
		System.out.println("另一种实现方式");
		collectStudentList.forEach(collect -> System.out.println(collect.toString()));

		System.out.println("----------------去重(垃圾，还不如直接放set)------------------");

		// 去重(垃圾，还不如直接放set)
		Stream.of(1, 2, 3, 4, 4, 2, 0).distinct().forEach(System.out::println);

		System.out.println("----------------使用双冒号结合stream------------------");

		// 使用双冒号结合stream
		List<Integer> collect = list.stream().map(Student::getNo).collect(Collectors.toList());
		System.out.println(collect.toString());

		System.out.println("----------------使用双冒号结合forEach------------------");
		List<Student> list2 = new ArrayList<>();
		list.forEach(list2 :: add);
		System.out.println(list2);

		System.out.println("-------------list转map，key必须唯一---------------------");

		// list转map，key必须唯一
		Map<Integer, Student> collectMap3 = list.stream().collect(Collectors.toMap(Student::getNo, student -> student));
		collectMap3.forEach((k, v) -> System.out.println(k + "--" + v));
		System.out.println("key值相同");
		list.add(new Student(5, "F", "M", 190));//增加一个要取的key值相同的对象,并提供解决方案
		Map<Integer, Student> collectMap = list.stream().collect(Collectors.toMap(Student::getNo, student -> student, (key1,key2)->key1));
		collectMap.forEach((k, v) -> System.out.println(k + "--" + v));
		Map<Integer, String> collectMap2 = list.stream().collect(Collectors.toMap(Student::getNo, Student::getName,(key1,key2) -> key2));
		collectMap2.forEach((k, v) -> System.out.println(k + "--" + v));

		System.out.println("--------------list取固定数量值--------------------");

		// list取固定数量值,不会有IndexOutOfBoundsException,超出即取所有,但是要大于等于零IllegalArgumentException
		List<Student> collect2 = list.stream().limit(1000000).collect(Collectors.toList());
		System.out.println(collect2);

	}

	public static List<Student> studentsList() {
		Student stuA = new Student(1, "A", "M", 184);
		Student stuB = new Student(2, "B", "G", 163);
		Student stuC = new Student(3, "C", "M", 175);
		Student stuD = new Student(4, "D", "G", 158);
		Student stuE = new Student(5, "E", "M", 170);
		List<Student> list = new ArrayList<Student>();
		list.add(stuA);
		list.add(stuB);
		list.add(stuC);
		list.add(stuD);
		list.add(stuE);
		return list;
	}
}

class Student {

	private int no;

	private String name;

	private String sex;

	private float height;

	public Student(int no, String name, String sex, float height) {
		super();
		this.no = no;
		this.name = name;
		this.sex = sex;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Student [no=" + no + ", name=" + name + ", sex=" + sex + ", height=" + height + "]";
	}

}
