package com.zxr.provider.jdk.jvm.stage4;

import java.util.Date;

/**
 * @Description: 栈帧结构，局部变量表
 * 每个栈帧中存储着：
 * - 局部变量表（Local Variables）
 * - 操作数栈（Operand Stack）（或表达式栈）
 * - 动态链接（Dynamic Linking）（或指向运行时常量池的方法引用）
 * - 方法返回地址（Return Address）（或方法正常退出或者异常退出的定义）
 * - 一些附加信息
 * @Author: Zheng Xinrui
 * @Date: 16:42 2021/5/17
 */
public class C_LocalVariablesTest {

    private int count = 0;

    /**
     * javap -v C_LocalVariablesTest.class，找到main方法区域可以看到如下
     * stack=2, locals=3, args_size=1（stack操作数栈深度，locals局部变量表深度，arg_size形参个数）
     *
     *LocalVariableTable:
     *         Start  Length  Slot  Name   Signature
     *             0      16     0  args   [Ljava/lang/String;
     *             8       8     1  test   Lcom/zxr/provider/jdk/jvm/stage4/C_LocalVariablesTest;
     *            11       5     2   num   I
     */
    public static void main(String[] args) {
        C_LocalVariablesTest test = new C_LocalVariablesTest();
        int num = 10;
        test.test1();
    }

    //练习：
    public static void testStatic(){
        C_LocalVariablesTest test = new C_LocalVariablesTest();
        Date date = new Date();
        int count = 10;
        System.out.println(count);
        //因为this变量不存在于当前方法的局部变量表中！！
//        System.out.println(this.count);
    }

    //关于Slot的使用的理解
    public C_LocalVariablesTest(){
        this.count = 1;
    }

    public void test1() {
        Date date = new Date();
        String name1 = "atguigu.com";
        test2(date, name1);
        System.out.println(date + name1);
    }

    public String test2(Date dateP, String name2) {
        dateP = null;
        name2 = "songhongkang";
        double weight = 130.5;//占据两个slot
        char gender = '男';
        return dateP + name2;
    }

    public void test3() {
        this.count++;
    }

    public void test4() {
        int a = 0;
        {
            int b = 0;
            b = a + 1;
        }
        //变量c使用之前已经销毁的变量b占据的slot的位置
        int c = a + 1;
    }
}
