package com.zxr.provider.jdk.jvm.stage4;

/**
 * @Description: 栈异常演示
 * @Author: Zheng Xinrui
 * @Date: 18:01 2021/5/7
 */
public class A_StackErrorTest {

    public static int count = 1;

    public static void main(String[] args) {


        //设置jvm栈大小 -Xss256k -Xss1m
        //使用jvm默认栈大小(默认1m)时count最高打印值为：11410
        //设置为256k时count最高打印值为：2463
        System.out.println(count);
        count++;

        //栈异常分为StackOverflowError和OutOfMemoryError
        //StackOverFlowError如下很好演示，即固定栈大小的前提下超过了栈指定大小抛出
        //OutOfMemoryError需要在动态调整栈大小的前提下耗尽系统内存抛出
        main(args);
    }
}
