package com.zxr.provider.jdk.jvm.stage5;

/**
 * @Description: 设置堆空间大小，然后执行，使用jdk/bin中的jvisualvm.exe工具查看jvm进程堆空间信息
 *               -Xms10m -Xmx10m，-X是jvm参数，ms是memory start，mx是memory max
 *
 *               1.设置堆空间大小
 *                  -Xms是设置堆空间（新生代+老年代）的初始内存大小
 *                  -Xmx是设置堆空间（新生代+老年代）的最大内存大小
 *               2.默认堆空间大小
 *                  初始内存大小=机器物理内存大小/64
 *                  最大内存大小=机器武器内存大小/4
 *               3.开发建议将初始和最大设置成一样，避免堆内存大小调整增加压力
 *
 *               4.查看当前jvm进程使用的堆内存参数及使用情况
 *                  1.命令窗口，jps + jstat -gc 进程号
 *                  2.添加jvm启动参数：-XX:+PrintGCDetails，控制台打印，使用的时候把下面休眠代码注释，该命令是基于执行完程序后的数据打印，并且出现gc时会打印gc日志
 *
 *               5.新生代、老年代参数设置
 *                  -XX:NewRatio ： 设置新生代与老年代的比例。默认-XX:NewRatio=2，表示新生代占1，老年代占2，新生代占整个堆的1/3
 *                  -XX:SurvivorRatio ：设置新生代中Eden区与Survivor区的比例。默认值是8:1:1。-XX:SurvivorRatio=10，表示比例是10:1:1
 *                  -XX:-UseAdaptiveSizePolicy ：关闭自适应的内存分配策略  （一般用不到）
 *                  -Xmn:设置新生代的空间的大小。 （一般不设置）
 *
 * @Author: Zheng Xinrui
 * @Date: 16:32 2022/2/21
 */
public class Demo1 {

    public static void main(String[] args) {
        System.out.println("start...");

        //获取虚拟机堆内存信息
        long ms = Runtime.getRuntime().totalMemory();
        long mx = Runtime.getRuntime().maxMemory();
        System.out.println("-Xms: "+ms/1024/1024+"M");
        System.out.println("-Xmx: "+mx/1024/1024+"M");

        try {
            Thread.sleep(600000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("end....");
    }
}
