package com.zxr.provider.jdk.dynamicProxy.impl;


/**
 * @Description: 顶层接口
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public interface InterfaceApi {

	void say();
}
