package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description: 证明变量的位置，不影响和其他方法执行的顺序，因为变量总是会先与任何方法之前得到初始化，就算是散布于方法的各个位置
 * @Author: Zheng Xinrui
 * @Date: 20:03 2021/2/23
 */
public class Window {

    Window(int marker){
        System.out.println("Window(" + marker + ")");
    }
}
