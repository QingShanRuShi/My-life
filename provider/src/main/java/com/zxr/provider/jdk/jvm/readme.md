#java虚拟机学习demo  
##目录  

视频地址：https://www.bilibili.com/video/BV1PJ411n7xZ   学习位置：P109
stage1  字节码初体验, jvm生命周期   p1-p25 
stage2  类加载子系统  p26-p40
stage3  pc寄存器  p41-p43
stage4  栈  p44-64
stage5  堆  p65-p86
stage6 方法区 p87-101
stage7 对象实例化 p102-p106

