package com.zxr.provider.jdk.jvm.stage6;

/**
 * @Description: 方法区，全局常量：none-final
 * @Author: Zheng Xinrui
 * @Date: 22:16 2022/2/23
 */
public class MethodAreaTest1 {

    /*
    * 这里并不会出现空指针异常，会执行到对应的静态方法和打印静态数据
    * 并且声明为final的静态常量，在编译时期就会被写入到字节码文件，而没有final修饰的static常量则是在类加载时确定内容然后加载到方法区
    * */
    public static void main(String[] args) {
        Order order = null;
        order.say();
        System.out.println(order.count);
        System.out.println(order.number);
    }

}


class Order{
    public static int count = 1;
    public static final int number = 2;

    public static void say(){
        System.out.println("hello world method area");
    }
}
