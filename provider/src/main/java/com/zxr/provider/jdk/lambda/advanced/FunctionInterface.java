package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 18:40 2020/8/20
 */
@FunctionalInterface
public interface FunctionInterface {

    void function(Dog dog);
}
