package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 19:50 2021/2/23
 */
public class Main {

    public static void main(String[] args) {
        new InitialValues().printInitialValues();
        System.out.println("===============================");
        new House().f();
        System.out.println("===============================");
    }
}
