package com.zxr.provider.jdk.spring.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description: spring ioc配置类
 * @Author: Zheng Xinrui
 * @Date: 23:46 2022/6/24
 */
@Configuration
@EnableAspectJAutoProxy(exposeProxy = true) //(proxyTargetClass = true)  /*<aop:aspectj-autoproxy/>*/
@ComponentScan("com.zxr.provider.jdk.spring.aop")
public class MainConfig {

   /* @Bean
    public Calculate calculate() {
        return new TulingCalculate();
    }

    @Bean
    public TulingLogAspect tulingLogAspect() {
        return new TulingLogAspect();
    }


    @Bean
    public Calculate calculate2() {
        return new TulingCalculate();
    }*/
}
