package com.zxr.provider.jdk.spring.circulardependencies;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;

/**
 * @Description: 实现动态代理后置处理器
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public class JdkProxyBeanPostProcessor implements SmartInstantiationAwareBeanPostProcessor {

	public Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {
		// 假设B 被切点命中 需要创建代理
		/*判断是不是被增强的类，是不是需要创建动态代理*/
		if(bean instanceof InstanceA) {
			JdkDynamicProxy jdkDynamicProxy = new JdkDynamicProxy(bean);
			return  jdkDynamicProxy.getProxy();
		}
		return bean;
	}
}
