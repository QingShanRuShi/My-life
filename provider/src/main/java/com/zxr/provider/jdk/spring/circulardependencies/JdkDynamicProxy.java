package com.zxr.provider.jdk.spring.circulardependencies;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


/**
 * @Description: jdk动态代理
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public class JdkDynamicProxy implements InvocationHandler {

	private final Object target;

	public Object getTarget() {
		return target;
	}

	public JdkDynamicProxy(Object target) {
		this.target = target;
	}

	public Object getProxy() {
		return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("测试");
		return method.invoke(target, args);
	}

	public static void main(String[] args) {
		InstanceA instanceA = new InstanceA();
		JdkDynamicProxy jdkDynamicProxy = new JdkDynamicProxy(instanceA);
		Object proxy = jdkDynamicProxy.getProxy();
		IApi iApi = (IApi) proxy;
		iApi.say();
	}
}
