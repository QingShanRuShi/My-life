package com.zxr.provider.jdk.spring.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description: 测试spring ioc aop 启动main
 * @Author: Zheng Xinrui
 * @Date: 23:46 2022/6/24
 */
public class TulingMainClass {

    public static void main(String[] args) {

    	AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(MainConfig.class);

        Calculate calculate = (Calculate) ctx.getBean("tulingCalculate");
		System.out.println(calculate.add(4, 2));

        System.out.println("=================");

        Calculate calculate2 = (Calculate) ctx.getBean("tulingCalculate2");
        System.out.println(calculate2.add(8, 2));

    }
}
