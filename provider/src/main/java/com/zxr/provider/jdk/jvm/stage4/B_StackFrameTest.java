package com.zxr.provider.jdk.jvm.stage4;

/**
 * @Description: 栈帧示例
 * @Author: Zheng Xinrui
 * @Date: 15:53 2021/5/17
 */
public class B_StackFrameTest {

    /**
     * 在“stackFrameTest.method1();”断点，debug执行，一步步走当前代码行，即可通过idea看到栈帧
     * 正在执行的栈帧叫做当前栈帧，对应的方法叫做当前方法，还有当前类
     * 在一条线程上，每个方法对应一个栈帧，执行到哪个方法，该方法栈帧会被置于栈顶（压栈），执行引擎运行的字节码指令只会针对当前栈帧进行操作
     * 当前栈帧（方法）执行完毕（return或者抛出没有被trycathy的异常），会执行出栈
     */
    public static void main(String[] args) {
        System.out.println("main开始执行");
        B_StackFrameTest stackFrameTest = new B_StackFrameTest();
        stackFrameTest.method1();
        System.out.println("main执行结束");
    }

    public void method1(){
        System.out.println("method1开始执行");
        method2();
        System.out.println("method1执行结束");
    }

    public int method2(){
        System.out.println("method2开始执行");
        int i = 1;
        double v = method3();
        System.out.println("method2即将结束");
        return i + (int)v;
    }

    public double method3(){
        System.out.println("method3开始执行");
        double i = 1.0;
        System.out.println("method3即将结束");
        return i;
    }
}
