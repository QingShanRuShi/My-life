package com.zxr.provider.jdk.dynamicProxy.impl;


/**
 * @Description: BeanA
 * @Author: Zheng Xinrui
 * @Date: 23:58 2022/6/22
 */
public class DInstanceA implements InterfaceApi {

    public DInstanceA() {
        System.out.println("InstanceA实例化");
    }

	@Override
	public void say() {
		System.out.println("I'm A");
	}
}
