package com.zxr.provider.jdk.dynamicProxy;


import com.zxr.provider.jdk.dynamicProxy.impl.DInstanceA;
import com.zxr.provider.jdk.dynamicProxy.impl.DInstanceB;
import com.zxr.provider.jdk.dynamicProxy.impl.InterfaceApi;

/**
 * @Description: 动态代理测试
 * @Author: Zheng Xinrui
 * @Date: 22:56 2022/7/19
 */
public class Test {

    public static void main(String[] args) throws ClassNotFoundException {

        String className = "com.zxr.provider.jdk.dynamicProxy.impl.DInstanceA";
        Class<?> aClass = Class.forName(className);
        JdkDynamicFactory<?> jdkDynamicFactory = new JdkDynamicFactory<>(aClass);
        InterfaceApi interfaceApi = (InterfaceApi)jdkDynamicFactory.newInstance();
        interfaceApi.say();

        System.out.println("============================================================");

        String classNameInterface = "com.zxr.provider.jdk.dynamicProxy.impl.InterfaceApi";
        Class<?> aClassInterface = Class.forName(classNameInterface);
        JdkDynamicFactory<?> interfaceJdkDynamicFactory = new JdkDynamicFactory<>(aClassInterface);
        InterfaceApi interfaceApi2 = (InterfaceApi)interfaceJdkDynamicFactory.newInstance();
        interfaceApi2.say();

        System.out.println("==========================================================\n");
        String className2 = "com.zxr.provider.jdk.dynamicProxy.impl.DInstanceA";
        Class<?> aClass2 = Class.forName(className2);
        CglibDynamicFactory<?> cglibDynamicFactory = new CglibDynamicFactory<>(aClass2);
        DInstanceA dInstanceA = (DInstanceA)cglibDynamicFactory.newInstance();
        dInstanceA.say();

        System.out.println("============================================================");
        String className3 = "com.zxr.provider.jdk.dynamicProxy.impl.DInstanceB";
        Class<?> aClass3 = Class.forName(className3);
        CglibDynamicFactory<?> cglibDynamicFactory2 = new CglibDynamicFactory<>(aClass3);
        DInstanceB dInstanceB = (DInstanceB)cglibDynamicFactory2.newInstance();
        System.out.println(dInstanceB.sayB());
    }
}
