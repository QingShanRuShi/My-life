package com.zxr.provider.jdk.jvm.stage6;

/**
 * @Description: 运行之后，使用工具jvisualvm，可以在“监视”中看到一个普通的打印休眠main也加载了一千六百多个类
 * @Author: Zheng Xinrui
 * @Date: 21:13 2022/2/23
 */
public class MethodAreaDemo {

    public static void main(String[] args) {
        System.out.println("start...");
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("end...");
    }

}
