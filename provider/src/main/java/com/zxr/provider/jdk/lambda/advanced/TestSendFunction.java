package com.zxr.provider.jdk.lambda.advanced;

/**
 * @Description: 借鉴kafkaTemplate源码用法
 * @Author: Zheng Xinrui
 * @Date: 17:05 2020/8/25
 */
public class TestSendFunction {

    public static void main(String[] args) {

        SendMessageInterface<String> sendMessage = new SendMessage<>();
        sendMessage.addCallback(
                success->{ System.out.println("11111111111"); System.out.println(success);},
                failure->{ System.out.println("33333333333"); System.out.println(failure.getMessage());},
                false);
    }
}
