package com.zxr.provider.jdk.lambda;

/**
 * @Description: 函数式接口定义，函数式接口只能有一个方法体，不用注解@FunctionalInterface也可以的，只需要自己规范代码保证只有一个方法即可
 * @Author: Zheng Xinrui
 * @Date: 18:13 2020/8/20
 */
public interface FunctionInterface2 {

    //有指定类型的参数无返回
    void function(String function, int function2);
}
