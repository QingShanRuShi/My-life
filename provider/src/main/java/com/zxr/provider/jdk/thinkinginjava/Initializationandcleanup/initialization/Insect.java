package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description: 展示有继承关系的类的加载顺序
 * @Author: Zheng Xinrui
 * @Date: 22:51 2021/2/24
 */
public class Insect {

    private int i = 9;
    protected int j;
    private int x = printInit("Insect.x initialized");
    Insect(){
        System.out.println("i=" + i + ", j=" + j);
        j = 39;
    }

    private static int x1 = printInit("static Insect.x1 initialized");

    static int printInit(String s){
        System.out.println(s);
        return 47;
    }
}
