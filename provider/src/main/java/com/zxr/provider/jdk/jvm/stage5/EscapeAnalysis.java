package com.zxr.provider.jdk.jvm.stage5;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 17:59 2022/2/23
 */

/**
 *  逃逸分析、栈上分配
 *  未发生逃逸，会被分配到栈上而不是堆，跟随栈的消亡而消亡
 *  但是oracle的Hotspot jvm默认没有使用逃逸分析栈上分配对象，它也有严重的不足（耗性能等），但是Hotspot使用了标量替换(见demo4)，
 *  将可以拆解的对象（聚合量）拆成一个个的局部标量（矢量），进而让栈管理它，达到和逃逸分析一样的目的
 *
 *  如何快速的判断是否发生了逃逸分析，大家就看new的对象实体是否有可能在方法外被调用。
 */
public class EscapeAnalysis {

    public EscapeAnalysis obj;

    /*
    方法返回EscapeAnalysis对象，发生逃逸
     */
    public EscapeAnalysis getInstance(){
        return obj == null? new EscapeAnalysis() : obj;
    }
    /*
    为成员属性赋值，发生逃逸
     */
    public void setObj(){
        this.obj = new EscapeAnalysis();
    }
    //思考：如果当前的obj引用声明为static的？仍然会发生逃逸。

    /*
    对象的作用域仅在当前方法中有效，没有发生逃逸
     */
    public void useEscapeAnalysis(){
        EscapeAnalysis e = new EscapeAnalysis();
    }
    /*
    引用成员变量的值，发生逃逸
     */
    public void useEscapeAnalysis1(){
        EscapeAnalysis e = getInstance();
        //getInstance().xxx()同样会发生逃逸
    }
}
