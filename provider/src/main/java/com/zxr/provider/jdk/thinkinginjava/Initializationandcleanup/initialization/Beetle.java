package com.zxr.provider.jdk.thinkinginjava.Initializationandcleanup.initialization;

/**
 * @Description: 展示有继承关系的类的加载顺序
 * @Author: Zheng Xinrui
 * @Date: 22:55 2021/2/24
 */
public class Beetle extends Insect {

    private int k = printInit("Beetle.k initialized");

    public Beetle(){
        System.out.println("k = " + k + ", j = " + j);
    }

    private static int x2 = printInit("static Beetle.x2 initialized");

    /*
    启动顺序总结：
    1.从根父类开始到子类，挨个初始化所有static修饰的变量和静态代码块
    2.从根父类开始到子类，每个类挨个执行两个初始化，只有当一个类执行完了下面的两个初始化才轮到下一个子类：
        1.初始化所有成员变量
        2.初始化指定构造函数，父类初始化构造函数由指定子类的构造函数中的super指定，没有则默认是super();
     */
    public static void main(String[] args) {
        System.out.println("Beetle constructor");
        Beetle beetle = new Beetle();
    }
}
