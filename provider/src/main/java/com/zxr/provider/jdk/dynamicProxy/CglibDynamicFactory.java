package com.zxr.provider.jdk.dynamicProxy;

import net.sf.cglib.proxy.Enhancer;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 23:28 2022/7/19
 */
public class CglibDynamicFactory<T> {

    private final Class<T> target;

    public CglibDynamicFactory( Class<T> target) throws ClassNotFoundException {
        if(target.isInterface()){
            throw new ClassNotFoundException("cglib dynamic proxy not allow interface: " + target.getName());
        }
        this.target = target;
    }

    public Object getTarget() {
        return target;
    }

    @SuppressWarnings("unchecked")
    public T newInstance(){
        Enhancer enhancer = new Enhancer();
        // 设置父类
        enhancer.setSuperclass(target);
        // 设置回调(在调用父类方法时，转为调用new CglibDynamicProxy().intercept())
        enhancer.setCallback(new CglibDynamicProxy());
        // 创建代理对象
        return (T)enhancer.create();
    }
}
