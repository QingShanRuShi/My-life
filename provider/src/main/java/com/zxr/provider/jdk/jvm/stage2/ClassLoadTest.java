package com.zxr.provider.jdk.jvm.stage2;

/**
 * @Description: 三种核心类加载器
 * @Author: Zheng Xinrui
 * @Date: 21:51 2021/5/6
 */
public class ClassLoadTest {


    public static void main(String[] args) {

        //获取系统类加载器 AppClassLoad
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);

        //获取系统类加载器上层：扩展类加载器ExtClassLoad(二者包含关系，并非继承，是系统类加载器的父类加载器)
        ClassLoader extClassLoader = systemClassLoader.getParent();
        System.out.println(extClassLoader);

        //获取扩展类加载器上层：启动类(引导类)加载器BootstrapClassLoad(包含关系，并非继承，是扩展类加载器的父类加载器)
        //系统类加载器和扩展类加载器都是使用启动类加载器加载
        ClassLoader bootstrapClassLoad = extClassLoader.getParent();
        System.out.println(bootstrapClassLoad);

        //对于用户自定义类的加载器，默认使用系统类加载器加载
        ClassLoader classLoader = ClassLoadTest.class.getClassLoader();
        System.out.println(classLoader);

        //java的核心类库都是使用引导类加载器加载，如String等
        ClassLoader stringClassLoad = String.class.getClassLoader();
        System.out.println(stringClassLoad);
    }

}
