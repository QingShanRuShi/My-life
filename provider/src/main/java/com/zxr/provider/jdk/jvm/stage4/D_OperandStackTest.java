package com.zxr.provider.jdk.jvm.stage4;

/**
 * @Description: 操作数栈
 *  每一个独立的栈帧除了包含局部变量表以外，还包含一个后进先出操作数栈，也称表达式栈
 *  主要用于保存计算过程的中间结果，同时作为计算过程中变量临时的存储空间。
 *  操作数栈大小在编译期间就定好了,它是由数组实现，但是只能出栈入栈，不能通过数组索引访问
 *  某些字节码指令将值压入操作数栈，其余的字节码指令将操作数取出栈。使用它们后再把结果压入栈，例如执行复制、交换、求和等操作会用到操作数栈
 * @Author: Zheng Xinrui
 * @Date: 10:14 2021/5/18
 */
public class D_OperandStackTest {

    public void testAddOperand(){
        byte i = 15;
        int j = 8;
        int k = i + j;
    }

    public int getSum(){
        int m = 10;
        int n = 20;
        int k = m + n;
        return k;
    }

    public void testGetSum(){
        //获取上一个栈桢返回的结果，并保存在操作数栈中
        int i = getSum();
        int j = 10;
    }
}
