package com.zxr.provider.jdk;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * @Description: 文件流高级操作之Zero-Copy
 * @Author: Zheng Xinrui
 * @Date: 14:59 2019/11/26
 */
public class FileStream {

    /*  正常拷贝：  磁盘  -> 内核空间  - > user空间 -> 内核空间 -> 目的缓冲区
        零拷贝方式  ：  磁盘  -> 内核空间  - >目的缓冲区*/
    public static void main(String[] args) throws IOException {
        commonFileStream();
        zeroCopyMin();
        zeroCopyMax();
    }

    //零拷贝 channelIn.transferTo(),根据源码，只支持最大2*1024*1024k - 1k的容量(2g)
    private static void zeroCopyMin() throws IOException {
        //源文件
        File file = new File("C:\\data\\mavenRepository2.zip");
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel channelIn = fileInputStream.getChannel();
        //目标文件夹（文件夹必须存在，不会自动建立）
        FileOutputStream fileOutputStream = new FileOutputStream("C:\\data\\copy\\zeroCopyMin.zip");
        FileChannel channelOut = fileOutputStream.getChannel();
        //省去对用户空间和内核空间的多一步操作，原理是使用了linux内核的sendfile()零拷贝技术
        System.out.println("小于2G文件零拷贝开始...");
        long startTimeMillis = System.currentTimeMillis();
        long l = channelIn.transferTo(0, file.length(), channelOut);
        System.out.println("小于2G文件零拷贝用时：" + (System.currentTimeMillis() - startTimeMillis) + "  文件大小为：" + l);
    }

    //零拷贝，大文件（超过2G）实现
    private static void zeroCopyMax(){
        File inputFile = new File("C:\\data\\mavenRepository.zip");
        File outputFile = new File("C:\\data\\copy\\zeroCopyMax.zip");
        try(FileInputStream fileInputStream = new FileInputStream(inputFile);
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            FileChannel inputFileChannel = fileInputStream.getChannel();
            FileChannel outputFileChannel = fileOutputStream.getChannel();){
            //实现很简单，把2g当做一个分割点就好
            int position = 0;
            long size = inputFileChannel.size();
            long size2 = inputFileChannel.size();
            System.out.println("大于2G文件零拷贝开始...");
            long startTimeMillis = System.currentTimeMillis();
            while (0 < size){
                long l = inputFileChannel.transferTo(position, size, outputFileChannel);
                if(l > 0){
                    position += l;
                    size -= l;
                }
            }
            System.out.println("大于2G文件零拷贝用时：" + (System.currentTimeMillis() - startTimeMillis) + "  文件大小为：" + size2);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //常用缓冲流比较
    private static void commonFileStream() {
        File outputfile = new File("C:\\data\\copy\\commonCopyMin.zip");
        File inputFile = new File("C:\\data\\mavenRepository2.zip");
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            if (!outputfile.exists()) {
                outputfile.createNewFile();
            }
            bufferedInputStream = new BufferedInputStream(new FileInputStream(inputFile));
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(outputfile, true));
            byte[] bytes = new byte[1024];
            int i;
            System.out.println("小于2G常用缓冲流拷贝开始...");
            long startTimeMill = System.currentTimeMillis();
            while ((i = bufferedInputStream.read(bytes)) != -1) {
                bufferedOutputStream.write(bytes, 0, i);
                bufferedOutputStream.flush();
            }
            System.out.println("小于2G常用缓冲流拷贝结束，耗时：" + (System.currentTimeMillis() - startTimeMill) + "  文件大小为：" + inputFile.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
                if (bufferedOutputStream != null) {
                    bufferedOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
