package com.zxr.provider.mapper.primary;


import com.zxr.provider.domain.User;
import com.zxr.providerapi.dto.UserDTO;

import java.util.List;
import java.util.Optional;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    Optional<User> selectOptionalByUserId(Integer id);

    List<UserDTO> findUserAll();
}