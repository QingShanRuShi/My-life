package com.zxr.provider.mapper.secondary;


import com.zxr.provider.domain.UserSecond;

public interface UserSecondMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserSecond record);

    int insertSelective(UserSecond record);

    UserSecond selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserSecond record);

    int updateByPrimaryKey(UserSecond record);
}