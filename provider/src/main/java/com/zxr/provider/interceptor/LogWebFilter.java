package com.zxr.provider.interceptor;

import com.zxr.provider.domain.Log;
import com.zxr.provider.mapper.primary.LogMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @Description: 使用webFilter实现操作日志记录
 * @Author: Zheng Xinrui
 * @Date: 16:30 2019/4/29
 */
@Slf4j
@WebFilter(urlPatterns = {"/log/*"}, filterName="logWebFilter")
public class LogWebFilter implements Filter{

    private static final String TOKEN = "token";

    @Autowired
    private LogMapper logMapper;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        ServletRequestAttributes requestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getParameter(TOKEN);
        String stringParame = null;

        Map<String, String[]> map = request.getParameterMap();
        if (!map.isEmpty()) {
            //使用treeMap自动排序
            Map<String, String> treeMap = map.keySet().stream().collect(Collectors.toMap(strKey -> strKey, strKey -> Arrays.toString(map.get(strKey)), (a, b) -> b, TreeMap::new));
            //拼接字符串
            String stringBuilder = treeMap.keySet().stream().map(key -> key + "=" + treeMap.get(key) + "&").collect(Collectors.joining());
            stringParame = stringBuilder.substring(0, stringBuilder.length() - 1).replaceAll("\\]|\\[","");
        }

        String method = request.getRequestURI() + "?" + stringParame;
        token = StringUtils.isEmpty(token) ? Long.toString(System.currentTimeMillis()) : token;

        Log logEntity = Log.builder().token(token).method(method).build();
        logMapper.insertSelective(logEntity);

        filterChain.doFilter(servletRequest, servletResponse);
        log.info("日志记录完毕，id-{}, token-{}", logEntity.getId(), token);
    }
}

