package com.zxr.provider.interceptor;

import com.zxr.common.vm.Result;
import com.zxr.provider.annotation.CheckToken;
import com.zxr.provider.common.util.RedisUtil;
import com.zxr.providerapi.dto.UserDTO;
import com.zxr.providerapi.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

import static com.zxr.provider.common.constant.RedisConstants.DEFAULT_LIVE_TIME;

/**
 * @Description: 校验token过滤器实现
 * @Author: Zheng Xinrui
 * @Date: 14:44 2019/4/9
 */
@Slf4j
public class CheckTokenInterceptro extends HandlerInterceptorAdapter{

    @Autowired
    private UserService userService;

    String TOKEN = "token";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        log.info("======拦截器过滤器=====");
        //类型转换校验
        if(!(handler instanceof HandlerMethod)){
            return true;
        }
        CheckToken checkToken = getCheckToken((HandlerMethod) handler);
        if(checkToken != null && !checkToken.ignore()){
           return checkToken(request);
        }
        return true;
    }

    private CheckToken getCheckToken(HandlerMethod handlerMethod){
        Method method = handlerMethod.getMethod();
        CheckToken annotation = method.getAnnotation(CheckToken.class);
        if(annotation == null){
            annotation = method.getDeclaringClass().getAnnotation(CheckToken.class);
        }
        return annotation;
    }

    private boolean checkToken(HttpServletRequest request){
        String token = request.getParameter(TOKEN);
        if(StringUtils.isEmpty(token) || !RedisUtil.hasKey(token)){
            int id = Integer.parseInt(request.getParameter("id"));
            Result<UserDTO> userDTOResult = userService.getUser(id);
            if(userDTOResult.getCode() < 0){
                log.warn("user is null and id is {}", id);
                return false;
            }
            UserDTO data = userDTOResult.getData();
            if(data != null){
                token = data.getUsername() + id + data.getPassword();
                log.info("token is create:{}, user:{}", token, data);
                RedisUtil.set(token, data, DEFAULT_LIVE_TIME);
            }else {
                log.warn("token or id is bad,token:{},id:{}", token, id);
                return false;
            }
        }
        return true;
    }
}
