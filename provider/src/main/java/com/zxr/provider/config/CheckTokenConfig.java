package com.zxr.provider.config;

import com.zxr.provider.interceptor.CheckTokenInterceptro;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description: token校验过滤器config
 * @Author: Zheng Xinrui
 * @Date: 14:43 2019/4/9
 */
@Configuration
public class CheckTokenConfig implements WebMvcConfigurer{

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(checkTokenInterceptro()) //添加过滤器实现
                .addPathPatterns("/check/**") //需要拦截过滤的路径
                .excludePathPatterns("/user/**", "/rocket/**", "/static/**"); //不需要拦截过滤的路径

//        registry.addInterceptor()可以一次配置多个不同的拦截器
    }

    @Bean
    public CheckTokenInterceptro checkTokenInterceptro(){
        return new CheckTokenInterceptro();
    }

}
