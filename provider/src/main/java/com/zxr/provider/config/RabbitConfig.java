package com.zxr.provider.config;

import com.zxr.provider.common.constant.QueueNames;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: rabbitmq配置文件
 * @Author: Zheng Xinrui
 * @Date: 10:43 2019/4/4
 */
@Configuration
public class RabbitConfig {

    @Bean
    public Queue defaultQueue(){
        return new Queue(QueueNames.DEFAULT);
    }
}
