package com.zxr.provider.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Description: 异步方法线程池配置类
 * @Author: Zheng Xinrui
 * @Date: 13:26 2019/4/30
 */
@Slf4j
@Configuration
public class AsyncConfig implements AsyncConfigurer{

    /**
     * 处理异步方法线程池配置
     *
     * 拒绝策略包括：
     ThreadPoolExecutor.AbortPolicy 丢弃任务并抛出RejectedExecutionException异常(默认)。
     ThreadPoolExecutor.DiscardPolic 丢弃任务，但是不抛出异常。
     ThreadPoolExecutor.DiscardOldestPolicy 丢弃队列最前面的任务，然后重新尝试执行任务
     ThreadPoolExecutor.CallerRunsPolic 由调用线程(主线程)处理该任务
     *
     * @return
     */
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);//核心线程数
        taskExecutor.setMaxPoolSize(20);//最大核心线程数
        taskExecutor.setKeepAliveSeconds(300);//线程最大空闲时间
        taskExecutor.setQueueCapacity(1000);//队列阻塞长度
        taskExecutor.setThreadNamePrefix("AsyncExecutorThread-");//新创建线程前缀
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());//拒绝策略
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);//保证子线程不会因为主线程的销毁而提前销毁
        taskExecutor.initialize();//初始化
        return taskExecutor;
    }


    /**
     * 自定义异常输出
     *
     * @return
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new MyAsyncExceptionHandler();
    }

    class MyAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

        @Override
        public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
            log.info("Exception message：{}", throwable.getMessage());
            log.info("Method name：{}", method.getName());
            for (Object param : obj) {
                log.info("Parameter value - " + param);
            }
        }

    }
}
