package com.zxr.provider.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

/**
 * @Description: 异步service
 * @Author: Zheng Xinrui
 * @Date: 13:44 2019/4/30
 */
@Slf4j
@Service
public class AsyncService {

    @Async
    public void asyncRetrunVoid() throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        log.info("异步方法调用开始=========, 线程名：{}", threadName);
        long startTime = System.currentTimeMillis();
        Thread.sleep(10000L);
        log.info("异步方法调用结束，耗时time:{}, 线程名：{}",System.currentTimeMillis() - startTime, threadName);
    }

    @Async
    public Future<String> asyncRetrunString(int i) throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        log.info("异步方法调用开始=========, 线程名：{}", threadName);
        long startTime = System.currentTimeMillis();
        Thread.sleep(5000L);
        log.info("异步方法调用结束，耗时time:{}, 线程名：{}",System.currentTimeMillis() - startTime, threadName+"=="+i);
        return new AsyncResult<>("异步方法返回值嘤嘤嘤");
    }
}
