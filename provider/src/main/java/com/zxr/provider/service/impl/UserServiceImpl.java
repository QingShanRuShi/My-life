package com.zxr.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zxr.common.enums.ErrorCodes;
import com.zxr.common.vm.Result;
import com.zxr.provider.common.exception.BaseException;
import com.zxr.provider.domain.User;
import com.zxr.provider.domain.UserSecond;
import com.zxr.provider.mapper.primary.UserMapper;
import com.zxr.provider.mapper.secondary.UserSecondMapper;
import com.zxr.providerapi.dto.UserDTO;
import com.zxr.providerapi.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:15 2019/3/14
 */
@Slf4j
@Service(interfaceClass = UserService.class)
@org.springframework.stereotype.Service
public class UserServiceImpl implements UserService {

    @Value("${aaaa:}")
    @Autowired
    private UserMapper userMapper;

    @NotNull
    @Autowired
    private UserSecondMapper userSecondMapper;

    @Override
    public Result<UserDTO> getUser(int id) {
        Optional<User> userOptional = userMapper.selectOptionalByUserId(1);
        //如果User对象为空则执行lambda表达式
        log.info("mybatis+optional-{}", userOptional.orElseThrow(()-> new BaseException(ErrorCodes.ERROR_20003)));
        User user = userMapper.selectByPrimaryKey(id);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return Result.result(userDTO);
    }

    @Transactional
    @Override
    public Result<UserDTO> updUser(UserDTO userDTO){
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        userMapper.updateByPrimaryKeySelective(user);
/*        //测试事务是否能正常使用，答案是可以的
        System.out.println("开始搞事情--------》");
        System.out.println(5/0);*/
        User userResult = userMapper.selectByPrimaryKey(user.getId());
        BeanUtils.copyProperties(userResult, userDTO);
        return Result.result(userDTO);
    }

    /**
     * 测试第二数据源获取用户信息
     *
     * @param id 用户id
     * @return
     */
    public Result<UserSecond> getSecondUser(int id){
        return Result.result(userSecondMapper.selectByPrimaryKey(id));
    }

    public List<UserDTO> getUserAll() {
        return userMapper.findUserAll();
    }
}
