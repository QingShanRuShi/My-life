package com.zxr.provider.aop;

import com.zxr.common.enums.ErrorCodes;
import com.zxr.provider.annotation.RedisLock;
import com.zxr.provider.common.exception.BaseException;
import com.zxr.provider.common.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @Description: redis分布式锁, 切面实现
 * @Author: Zheng Xinrui
 * @Date: 16:24 2019/4/1
 */
@Component
@Slf4j
@Aspect
public class RedisLockAspect {

    private String TOKEN = "token";
    private long lockTime = 3L;


    public RedisLockAspect() {
        log.info("init RedisLockAspect");
    }

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.zxr.provider.annotation.RedisLock)")
    public void pointCutMethod() {
        //nothing to do
    }

    /**
     * 实现切面
     * redis
     *
     * @return
     */
    @Around("pointCutMethod()&&@annotation(redisLock)")
    public void doAround(ProceedingJoinPoint proceedingJoinPoint, RedisLock redisLock){
        log.info("redis分布式锁开始执行");

        //获取切入方法所有参数
        final Object[] parameterValues = proceedingJoinPoint.getArgs();
        //获取所有参数名集合
        CodeSignature codeSignature = (CodeSignature) proceedingJoinPoint.getStaticPart().getSignature();
        String[] parameterNames = codeSignature.getParameterNames();

        //根据token生成该用户对应的方法唯一redis-key，防重复调用
        //获取方法名
        String mathodName = proceedingJoinPoint.getSignature().getName();
        String parameterToken = "";
        for(int i=0; i<parameterNames.length; i++){
            if(TOKEN.equals(parameterNames[i])){
                parameterToken = (String) parameterValues[i];
            }
        }
        //从参数中获取不到token，就从request中获取
        if(StringUtils.isEmpty(parameterToken)){
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            parameterToken = request.getParameter(TOKEN);
            if(StringUtils.isEmpty(parameterToken)){
                throw new BaseException(ErrorCodes.ERROR_20001);
            }
        }
        //token获取正常，获取redis锁
        String timeOut = redisLock.timeOut();
        String lockKey =  mathodName + parameterToken;
        lockTime = StringUtils.isEmpty(timeOut) ? lockTime : Integer.parseInt(timeOut);
        log.info("lockkey={},locktime={}", lockKey, lockTime);
        if(RedisUtil.hasKey(lockKey)){
            log.info("get redis lock is failed! parameterToken={},mathodName={}",parameterToken,mathodName);
            throw new BaseException(ErrorCodes.ERROR_20002);
        }else {
            log.info("get redis lock is success");
            try {
                RedisUtil.set(lockKey, UUID.randomUUID().toString(), lockTime);
                proceedingJoinPoint.proceed();
            }catch (Throwable throwable){
                log.error("获得锁但是出异常(error日志不需要{}，最后加上就会默认输出)", throwable);
            }finally {
                //执行完毕释放锁
                RedisUtil.delete(lockKey);
                log.info("redis分布式锁正常执行结束");
            }
        }
    }
}
