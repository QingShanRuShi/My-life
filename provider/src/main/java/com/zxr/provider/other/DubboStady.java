package com.zxr.provider.other;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.rpc.service.GenericService;

/**
 * @Description: dubbo学习
 * @Author: Zheng Xinrui
 * @Date: 15:35 2021/1/26
 */
public class DubboStady {

    /**
     * dubbo泛化调用
     * 泛化接口调用方式主要用于客户端没有 API 接口及模型类元的情况，参数及返回值中的所有 POJO 均用 Map 表示，
     * 通常用于框架集成，比如：实现一个通用的服务测试框架，可通过 GenericService 调用所有服务实现。
     *
     * @param args
     */
    public static void main(String[] args) {
        ReferenceConfig<GenericService> reference = new ReferenceConfig<>();
        // 当前dubbo consumer的application配置，不设置会直接抛异常
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("test");
        // 注册中心配置
        RegistryConfig registryConfig = new RegistryConfig();
        // 注册中心这里需要配置上注册中心协议，例如下面的zookeeper或者nacos,还可以设置group等信息
        registryConfig.setAddress("nacos://49.235.87.157:8848");
        reference.setApplication(applicationConfig);
        reference.setRegistry(registryConfig);
        // 设置调用的reference属性，可以设置协议、接口名、版本、超时时间等信息
        reference.setProtocol("dubbo");
        reference.setInterface("com.wondersgroup.common.api.SecurityService");
        reference.setTimeout(3000);
        // 声明为泛化接口
        reference.setGeneric("true");
        // GenericService可以接住所有的实现
        GenericService genericService = reference.get();
        //参数1方法名，参数2方法的参数顺序及类型，参数3对应参数类型的请求参数值
        Object result = genericService.$invoke("createTokenWithJwt", new String[]{"java.lang.String", "java.lang.Integer"}, new Object[]{"zxr123456ZXR", 1440});
//        Object result = genericService.$invoke("validTokenWithJwt", new String[]{"java.lang.String"}, new Object[]{"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2MTE3MzIxMDksInN1YiI6Inp4cjEyMzQ1NlpYUiIsImlhdCI6MTYxMTY0NTcwOX0.1VsCiSRAB7SYPk6paKhvtXCWLT5EZuSCtk7tdbHkzJd1CpgZg5oF10TkJZhXUnSDPZHMsiM3sbLcJo_IQ6gPow"});
        System.out.println(result.toString());
    }
}
