package com.zxr.provider.common.exception;

import com.zxr.common.enums.CodeMessage;
import lombok.Getter;

/**
 * @Description: 自定义基础异常
 * @Author: Zheng Xinrui
 * @Date: 10:57 2019/4/2
 */
public class BaseException extends RuntimeException implements CodeMessage {

    @Getter
    private final int code;

    public BaseException(int code, String codeMessage){
        super(codeMessage);
        this.code = code;
    }

    public BaseException(CodeMessage codeMessage){
        super(codeMessage.getMessage());
        this.code = codeMessage.getCode();
    }
}
