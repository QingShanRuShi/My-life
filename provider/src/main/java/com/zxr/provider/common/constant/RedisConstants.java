package com.zxr.provider.common.constant;

/**
 * @Description: redis常量
 * @Author: Zheng Xinrui
 * @Date: 14:36 2019/4/2
 */
public class RedisConstants {

    private RedisConstants(){
        //nothing to do
    }

    /**
     * 默认过期时间(s)
     */
    public static final long DEFAULT_LIVE_TIME = 24 * 60 * 60L;
}
