package com.zxr.provider.common.util;

import com.alibaba.fastjson.JSONObject;
import okhttp3.*;

import javax.net.ssl.X509TrustManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: song
 * @Data： 2018/7/3010
 * @Description
 *
 *      http请求工具类
 */
public class HttpUtils {


    public static JSONObject syncGet(String url) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = (new Request.Builder()).url(url).build();
        Response response = okHttpClient.newCall(request).execute();
        ResponseBody body = response.body();
        if(body != null){
            return JSONObject.parseObject(body.string());
        }
        return new JSONObject();
    }

    public static JSONObject syncHeaderGet(String url, Map<String, String> map) throws IOException {

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = (new Request.Builder()).url(url).headers(Headers.of(map)).build();
        Response response = okHttpClient.newCall(request).execute();
        ResponseBody body = response.body();
        if(body != null){
            return JSONObject.parseObject(body.string());
        }
        return new JSONObject();
    }

    public static JSONObject syncPost(String url, Map<String, Object> params) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        FormBody.Builder builder = new FormBody.Builder();

        for(Map.Entry<String, Object> entry : params.entrySet()){
            builder.add(entry.getKey(), entry.getValue()+"");
        }
        RequestBody requestBody = builder.build();
        Request request = (new Request.Builder()).url(url).post(requestBody).build();
        Response response = okHttpClient.newCall(request).execute();
        ResponseBody body = response.body();
        if(body != null){
            return JSONObject.parseObject(body.string());
        }
        return new JSONObject();
    }

    public static JSONObject syncHeaderPost(String url, Map<String, Object> params, Map<String, String> headerMap) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        FormBody.Builder builder = new FormBody.Builder();

        for(Map.Entry<String, Object> entry : params.entrySet()){
            builder.add(entry.getKey(), entry.getValue()+"");
        }
        RequestBody requestBody = builder.build();
        Request request = (new Request.Builder()).url(url).headers(Headers.of(headerMap)).post(requestBody).build();
        Response response = okHttpClient.newCall(request).execute();
        ResponseBody body = response.body();
        if(body != null){
            return JSONObject.parseObject(body.string());
        }
        return new JSONObject();
    }

    /**
     * 同步post请求
     * 返回json对象
     *
     * @param url 请求地址
     * @return
     * @throws Exception
     */
    public static Object syncPostWithType(String url, String json) throws IOException {
        OkHttpClient     okHttpClient = new OkHttpClient();

        RequestBody      requestBody  = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        Request          request      = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response         response     = okHttpClient.newCall(request).execute();

        return JSONObject.parse(response.body().string());
    }

    /**
     * 文件上传
     *
     * @param url 请求地址
     * @param fileByte 文件字节流
     * @param fileNamePara 文件参数名
     * @param fileName 文件名
     * @param paraMap 其他参数键值对，该参数设置的是只能放在form-data里面
     */
    public static JSONObject fileUpload(String url, byte[] fileByte, String fileNamePara, String fileName, HashMap<String, Object> paraMap) throws IOException {
        X509TrustManager manager = SSLSocketClientUtil.getX509TrustManager();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .sslSocketFactory(SSLSocketClientUtil.getSocketFactory(manager), manager)
                .hostnameVerifier(SSLSocketClientUtil.getHostnameVerifier())
                .build();
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), fileByte);
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(fileNamePara, fileName, fileBody);
        if(paraMap != null && paraMap.size() > 0){
            for (Map.Entry<String, Object> entry : paraMap.entrySet()) {
                multipartBuilder.addFormDataPart(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }
        RequestBody requestBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = okHttpClient.newCall(request).execute();
        ResponseBody body = response.body();
        if(body != null){
            return JSONObject.parseObject(body.string());
        }
        return new JSONObject();
    }

    public static void main(String[] args) throws IOException {
        String filePath = "/Users/zhengxiaoxin/Desktop/工作文件/工作/ocr/用例图片/医疗发票/ylfp.jpg";
        File file = new File(filePath);
        String url = "https://ai.wonderscloud.com/ocr_bill";
        HashMap<String, Object> paraMap = new HashMap<>();
        String fileNamePara = "file";
        String fileName = file.getName();
        byte[] fileByte = file2byte(file);
        JSONObject jsonObject = HttpUtils.fileUpload(url, fileByte, fileNamePara, fileName, paraMap);
        System.out.println(jsonObject.toJSONString());
    }

    /**
     * 将文件转换成byte数组
     */
    public static byte[] file2byte(File tradeFile){
        byte[] buffer = null;
        try
        {
            FileInputStream fis = new FileInputStream(tradeFile);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1)
            {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (IOException e){
            e.printStackTrace();
        }
        return buffer;
    }

}
