package com.zxr.provider.common.exception;

import com.zxr.common.enums.ErrorCodes;
import com.zxr.common.vm.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Description: 全局异常捕获
 * @Author: Zheng Xinrui
 * @Date: 13:31 2019/4/18
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BaseException.class)
    @Primary
    @ResponseBody
    public Result baseExceptionHandler(BaseException baseException){
        log.error("捕获到自定义异常-{}", baseException.getMessage());
        baseException.printStackTrace();
        return Result.result(baseException);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result excetionHandler(Exception tr){
        log.error("其他异常:{}",tr.getMessage());
        tr.printStackTrace();
        return Result.result(ErrorCodes.ERROR_10002);
    }
}
