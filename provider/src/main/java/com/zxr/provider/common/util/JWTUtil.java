package com.zxr.provider.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Date;

@Component
public class JWTUtil {

    //密钥
    private static final String SECRET = "zhengxinrui";
    //token过期时间(ms)
    private static final long EXPIRETIME = 24 * 60 * 60 * 1000;


    /**
     * 校验token是否正确
     * @param token 密钥
     * @param claim 查询条件key
     * @param username 查询条件value
     * @return 是否正确
     */
    public  boolean verify(String token, String claim , String username) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim(claim, username)
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     *
     * @param token 密钥
     * @param claim 要获取的条件的key
     * @return token中包含的用户名
     */
    public  String getUser(String token, String claim) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim(claim).asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 生成签名,5min后过期
     * @param username 用户唯一标识
     * @return 加密的token
     */
    public String getToken(String username) {
        try {
            Date date = new Date(System.currentTimeMillis()+ EXPIRETIME);
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            // 附带username信息
            return JWT.create()
                    .withClaim("username", username)
                    .withClaim("name", username)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
