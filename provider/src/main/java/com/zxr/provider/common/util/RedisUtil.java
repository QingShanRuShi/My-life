package com.zxr.provider.common.util;

import com.zxr.provider.common.constant.RedisConstants;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @Description: redis 工具类
 * @Author: Zheng Xinrui
 * @Date: 14:03 2019/4/2
 */
@Slf4j
public class RedisUtil {

    private static RedisTemplate redisTemplate;

    private RedisUtil(){
        //nothing to do
    }

    public static void setRedisTemplate(RedisTemplate redisTemplate){
        RedisUtil.redisTemplate = redisTemplate;
    }

    public static <T> RedisTemplate<String, T> getTemplate(){
        return redisTemplate;
    }

    /**
     * 将一个对象存入redis, 不设置过期时间，默认一整天
     *
     * @param key
     * @param value
     */
    public static void set(@NonNull String key, @NonNull Object value){
        set(key, value, RedisConstants.DEFAULT_LIVE_TIME);
    }

    /**
     * 将一个对象存入redis, 手动设置过期时间
     *
     * @param key
     * @param value
     * @param seconds 过期时间（s）,-1为永久
     */
    public static void set(@NonNull String key, @NonNull Object value, long seconds){
        set(key, value, seconds, getTemplate());
    }

    /**
     * 将一个对象存入redis，设置过期时间
     *
     * @param key
     * @param value
     * @param seconds，秒数，-1时为无超时
     * @param redisTemplate      指定template（指定序列化方式）
     */
    public static void set(@NonNull String key, @NonNull Object value, long seconds, @NonNull RedisTemplate redisTemplate){
        if(seconds < 0){
            redisTemplate.opsForValue().set(key, value);
        }else {
            redisTemplate.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
        }
    }

    /**
     * 获取一个字符串
     *
     * @param key
     * @return redis-key
     */
    public static String getString(@NonNull String key){
        if(hasKey(key)){
            return redisTemplate.opsForValue().get(key).toString();
        }
        return null;
    }

    /**
     * 获取一个对象
     *
     * @param key
     * @param objectClass class
     * @param <T> 对象类型
     * @return
     */
    public static <T> T getObject(@NonNull String key, @NonNull Class<T> objectClass){
        if(!hasKey(key)){
            return null;
        }else if(objectClass.equals(String.class)){
            return (T) getString(key);
        }else {
            Object value = getObject(key);
            if(Number.class.isAssignableFrom(objectClass)){
                try {
                    return objectClass.getConstructor(String.class).newInstance(value.toString());
                }catch (Exception e){
                    log.error("redis数值类型转换异常，key-{},objectClass-{}", key, objectClass, e);
                }
            }else {
                return (T) value;
            }
            return null;
        }
    }

    /**
     * 获取一个对象
     *
     * @param key
     * @return
     */
    public static Object getObject(@NonNull String key){
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 判断是否有key
     *
     * @param key redis-key
     * @return
     */
    public static boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    /**
     * 根据key删除一个实例
     *
     * @param key redis-key
     * @return
     */
    public static void delete(String key){
        redisTemplate.delete(key);
    }
}
