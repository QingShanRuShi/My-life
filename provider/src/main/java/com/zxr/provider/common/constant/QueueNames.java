package com.zxr.provider.common.constant;

/**
 * @Description: rabbitmq队列名
 * @Author: Zheng Xinrui
 * @Date: 10:39 2019/4/4
 */
public class QueueNames {

    public static final String DEFAULT = "default_queue";

    public static final String KAFKADEFAULT = "default_queue_kafka";
    public static final String KAFKADEFAULT2 = "default_queue_kafka2";
}
