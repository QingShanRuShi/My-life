package com.zxr.provider.common.util;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @Description: 包括加验签，RSA加解密，AES加解密，生成公私钥
 * @Author: Zheng Xinrui
 * @Date: 11:00 2019/4/18
 */

/*
* 签名：客户端私钥加签，服务端公钥验签，参数排列规则需要一致
* RSA: 非对称加密，客户端公钥加密，服务端私钥解密
* AES: 对称加密对称加密，客户端服务端加解密都是同一个密钥、一个偏移量组成
* */
@Slf4j
public class SignUtils {

    private static final String ALGORITHM = "RSA";

    private static final String SIGN_ALGORITHMS = "SHA1WithRSA";

    private static final String DEFAULT_CHARSET = "UTF-8";

    private static final String AES = "AES";
    private static final String AES_CBC = "AES/CBC/PKCS7Padding";
    private static final int DEFAULT_AES_KEYSIZE = 128;
    private static final int DEFAULT_IVSIZE = 16;
    private static final SecureRandom RANDOM = new SecureRandom();
    private static final String DEFAULT_URL_ENCODING = "UTF-8";
    private static final byte[] DEFAULT_KEY = new byte[]{-97, 88, -94, 9, 70, -76, 126, 25, 0, 3, -20, 113, 108, 28, 69, 125};



    /**
     * 签名
     *
     * @param content 需要签名的内容
     * @param privateKey 私钥
     * @return
     */
    public static String sign(String content, String privateKey) {
        try {
            PrivateKey priKey = getPrivateKey(privateKey);

            java.security.Signature signature = java.security.Signature
                    .getInstance(SIGN_ALGORITHMS);

            signature.initSign(priKey);
            signature.update(content.getBytes(DEFAULT_CHARSET));

            byte[] signed = signature.sign();

            return Base64.encode(signed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 验签
     *
     * @param content 需要验签的内容
     * @param sign 验签标识
     * @param publicKey 验签公钥
     * @return
     */
    public static boolean verify(String content, String sign, String publicKey) {
        try {

            PublicKey pubKey = getPublicKey(publicKey);

            java.security.Signature signature = java.security.Signature
                    .getInstance(SIGN_ALGORITHMS);
            signature.initVerify(pubKey);
            signature.update(content.getBytes(DEFAULT_CHARSET));
            return signature.verify(Base64.decode(sign));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * RSA加密（Base64）
     *
     * @param text 需要加密内容
     * @param publicKey 公钥
     * @return
     */
    public static String encrypt(String text, String publicKey) {
        //RSA加密
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
            byte[] bytes = text.getBytes();
            int inputLen = bytes.length;
            int offLen = 0;//偏移量
            int i = 0;
            ByteArrayOutputStream bops = new ByteArrayOutputStream();
            while (inputLen - offLen > 0) {
                byte[] cache;
                if (inputLen - offLen > 117) {
                    cache = cipher.doFinal(bytes, offLen, 117);
                } else {
                    cache = cipher.doFinal(bytes, offLen, inputLen - offLen);
                }
                bops.write(cache);
                i++;
                offLen = 117 * i;
            }
            bops.close();
            byte[] encryptedData = bops.toByteArray();
            String encodeToString = Base64.encode(encryptedData);
            return encodeToString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * RSA解密（Base64）
     *
     * @param text 需要解密的内容
     * @param privateKey 私钥
     * @return
     */
    public static String decrypt(String text, String privateKey) {
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));
            byte[] bytes = Base64.decode(text);
            int inputLen = bytes.length;
            int offLen = 0;
            int i = 0;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (inputLen - offLen > 0) {
                byte[] cache;
                if (inputLen - offLen > 128) {
                    cache = cipher.doFinal(bytes, offLen, 128);
                } else {
                    cache = cipher.doFinal(bytes, offLen, inputLen - offLen);
                }
                byteArrayOutputStream.write(cache);
                i++;
                offLen = 128 * i;

            }
            byteArrayOutputStream.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return new String(byteArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //公钥转换为PublicKey对象
    private static PublicKey getPublicKey(String publicKey) {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decode(publicKey));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pk = keyFactory.generatePublic(keySpec);
            return pk;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //私钥转换为PrivateKey对象
    private static PrivateKey getPrivateKey(String privateKey) {
        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
                Base64.decode(privateKey));
        KeyFactory keyf = null;
        try {
            keyf = KeyFactory.getInstance(ALGORITHM);
            PrivateKey priKey = keyf.generatePrivate(priPKCS8);
            return priKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /*------------------分割线，AES加密---------------------*/

    /**
     * des加密
     *
     * @param content 需要加密内容
     * @param key 唯一密钥
     * @param iv 偏移量
     * @return
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws UnsupportedEncodingException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    public static String encryptAES(String content, String key, String iv) throws InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException {
        byte[] byteContent = content.getBytes("UTF-8");
        byte[] enCodeFormat = key.getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");
        byte[] initParam = iv.getBytes();
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(1, secretKeySpec, ivParameterSpec);
        byte[] encryptedBytes = cipher.doFinal(byteContent);
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        return encoder.encodeToString(encryptedBytes);
    }

    /**
     * des解密
     *
     * @param content 需要解密的内容
     * @param key 唯一密钥
     * @param iv 偏移量
     * @return
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     */
    public static String decryptAES(String content, String key, String iv) throws NoSuchPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
        byte[] encryptedBytes = decoder.decode(content);
        byte[] enCodeFormat = key.getBytes();
        byte[] decryptedBytes = aesDecryptBytes(encryptedBytes, enCodeFormat, iv);
        return new String(decryptedBytes, "UTF-8");
    }

    //获取key
    public static String genKeyString() {
        return EncodeUtils.encodeHex(genKey(128));
    }

    public static String encode(String input) {
        try {
            return EncodeUtils.encodeHex(encode(input.getBytes("UTF-8"), DEFAULT_KEY));
        } catch (UnsupportedEncodingException var2) {
            return "";
        }
    }

    public static String encode(String input, String key) {
        try {
            return EncodeUtils.encodeHex(encode(input.getBytes("UTF-8"), EncodeUtils.decodeHex(key)));
        } catch (UnsupportedEncodingException var3) {
            return "";
        }
    }

    public static String decode(String input) {
        try {
            return new String(decode(EncodeUtils.decodeHex(input), DEFAULT_KEY), "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            return "";
        }
    }

    public static String decode(String input, String key) {
        try {
            return new String(decode(EncodeUtils.decodeHex(input), EncodeUtils.decodeHex(key)), "UTF-8");
        } catch (UnsupportedEncodingException var3) {
            return "";
        }
    }

    public static byte[] genKey() {
        return genKey(128);
    }

    public static byte[] genKey(int keysize) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(keysize);
            SecretKey secretKey = keyGenerator.generateKey();
            return secretKey.getEncoded();
        } catch (GeneralSecurityException var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static byte[] genIV() {
        byte[] bytes = new byte[16];
        RANDOM.nextBytes(bytes);
        return bytes;
    }

    public static byte[] encode(byte[] input, byte[] key) {
        return aes(input, key, 1);
    }

    public static byte[] encode(byte[] input, byte[] key, byte[] iv) {
        return aes(input, key, iv, 1);
    }

    public static byte[] decode(byte[] input, byte[] key) {
        return aes(input, key, 2);
    }

    public static byte[] decode(byte[] input, byte[] key, byte[] iv) {
        return aes(input, key, iv, 2);
    }

    private static byte[] aes(byte[] input, byte[] key, int mode) {
        try {
            SecretKey secretKey = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(mode, secretKey);
            return cipher.doFinal(input);
        } catch (GeneralSecurityException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    private static byte[] aes(byte[] input, byte[] key, byte[] iv, int mode) {
        try {
            SecretKey secretKey = new SecretKeySpec(key, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(mode, secretKey, ivSpec);
            return cipher.doFinal(input);
        } catch (GeneralSecurityException var7) {
            var7.printStackTrace();
            return null;
        }
    }


    public static byte[] aesDecryptBytes(byte[] contentBytes, byte[] keyBytes, String iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        return cipherOperation(contentBytes, keyBytes, iv, 2);
    }

    private static byte[] cipherOperation(byte[] contentBytes, byte[] keyBytes, String iv, int mode) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec secretKey = new SecretKeySpec(keyBytes, "AES");
        byte[] initParam = iv.getBytes("UTF-8");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, secretKey, ivParameterSpec);
        return cipher.doFinal(contentBytes);
    }


}
