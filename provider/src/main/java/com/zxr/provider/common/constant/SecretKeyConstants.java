package com.zxr.provider.common.constant;

/**
 * @Description: 公钥私钥,测试使用，可以动态生成
 * @Author: Zheng Xinrui
 * @Date: 10:16 2019/4/18
 */
public class SecretKeyConstants {

    public static String clientPublicKEY="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDTnC2pT2b2uKOcfs9SCRgBZMCg\n" +
            "cVCqAZSHkBAJcwqjQleugAEbfOr9XIxPIAXORCagqulKiR3bVMt2Hl+0IdUoBpVs\n" +
            "xAqyWEu2py8c5+47fRQU4+OjUNKr/rtWFZsZPNVjS0RPlOH5Of4T/MUnARn1aceJ\n" +
            "VPMyM3Fv1fcVALXd7wIDAQAB";
    public static String clientPrivateKEY="MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBANOcLalPZva4o5x+\n" +
            "z1IJGAFkwKBxUKoBlIeQEAlzCqNCV66AARt86v1cjE8gBc5EJqCq6UqJHdtUy3Ye\n" +
            "X7Qh1SgGlWzECrJYS7anLxzn7jt9FBTj46NQ0qv+u1YVmxk81WNLRE+U4fk5/hP8\n" +
            "xScBGfVpx4lU8zIzcW/V9xUAtd3vAgMBAAECgYEAsV/aTm9GKUy/p/ALrAH/Yzum\n" +
            "01XofXhxneg96Q3LPunfQV/+jaH5/4HSAokfFLS/hDF+94FTy4OaQFpZck5hsnGr\n" +
            "YVBRDEtKqT3rBkLfKYS6LnrK/IxjlUt4mXneYf9Rf8QiXXvJNr1WQQ/SX1gnUf/P\n" +
            "gafKkkUnDzS1Sb0A9CECQQD9Su8hQSpt5qds4aBUHyqNOLFaDFF1ZUEIW9u51kh4\n" +
            "mUC3QbMhnPULq9+Jq9w3Cj6+RoXWViA8AkbSsvONDEL3AkEA1d8w8NViKrppfe3q\n" +
            "atSN0eAD75MK4HgU6aa6ICgwW09BVjdWdE0HgXZwtG/Cnhz5wIOgNX60Vhp3FvxM\n" +
            "FziGyQJBALssHYt4aU8vS/eEv4KKSVp0U3JUefmDakwv/S3VsFYWAaGqFiI3p0kJ\n" +
            "qDVG34N4uabH3Gzn1ggiMo3OmXVYYz8CQQCpM+fbUWZV2Z8mMvkXxcvhtDJI8vNS\n" +
            "/Y3GANMWJ9WCF+TgGe27G0oJ4bNu/VBimRf6d+hjCjgQOuXm+HoWkPEJAkEAvdAV\n" +
            "d/SqeLUQtT1YzqH1tGcazxSxVVb51zz1HWdzqS9U83NjGkI3NgFyfJ+qfunHLAmg\n" +
            "9HxkntdwymEcxyNbnw==";
    public static String systemPublicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxGvOgmfgFCYyF8aiN/jbR9V5Y\n" +
            "KbRYj+/k3kfk/ajxGDshSFuG9i2/5hsoG3uHS3DRdJYF8i9BMA+cZJerd1d2z1hF\n" +
            "tB5LOVCgUFUg0FREgktTR9Z8Hk5VM2QWhm8nvl6gRYIpixkaK98HtqTPgw3UqeLh\n" +
            "s65OqMIMX6HYyj4GzQIDAQAB";
    public static String systemPrivateKey="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALEa86CZ+AUJjIXx\n" +
            "qI3+NtH1XlgptFiP7+TeR+T9qPEYOyFIW4b2Lb/mGygbe4dLcNF0lgXyL0EwD5xk\n" +
            "l6t3V3bPWEW0Hks5UKBQVSDQVESCS1NH1nweTlUzZBaGbye+XqBFgimLGRor3we2\n" +
            "pM+DDdSp4uGzrk6owgxfodjKPgbNAgMBAAECgYAcL4viXV6pfpGMp58JqhGTbJdt\n" +
            "NIXtKkslg3KxsOT4KcEPVPKsEfXJ+pcL1QPdzlXQH9vYami22S1C2IeC1GVo0SeA\n" +
            "6cv6LD05W8UTrcV9brTUJgANKGu/DSbTUQYl+jf2ouF2k4V9a6uHiLdVL1SW0EBY\n" +
            "m5gwvt9+7iHHzW54yQJBAOeJ79xe7EgJ5uDAf6lihyVEqt2Nc83CgzkTrlZNnNnJ\n" +
            "/gt4SoIqvb8Mx0dCBb7jSBTaCKkDgr8W6tfwXy5xvF8CQQDD0NZTh0/RVYKUML8f\n" +
            "8egQ7m+ZQjrQ19K9vbeRop9apDkbKLZp+o+lTLMZ8xBKh1+L+UP1lpzPZlykfbyV\n" +
            "5oxTAkArCAPAHRQkv1JdaulO/bTIRF7NFkDzeLcSoInCGGKSR2wjjZOrZW4mV6Wh\n" +
            "pK+UroDy/yKk5rYh9Yxn3ZCS/ShZAkEAt28eJ9ddJTuWfF4B3NoPTDUgpBBP6q4T\n" +
            "RB3CJhKSdyXaPR/xaewHvpDQQqHZBAg2sHhMenah5QP+CzWigUYc8wJAAmgbYOHg\n" +
            "E8zJgVyjQlXIN3+Tb11Cdenbv70pLEBUg57X8YGfiztPcLZ3viFK44By/iGhZkRm\n" +
            "zcyR2I8IEyGmyg==";
}
