package com.zxr.provider.common.util;

import com.github.pagehelper.PageHelper;

import java.util.Properties;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 15:49 2019/5/5
 */
public class PageHelperUtils {

    public static PageHelper getPageHelper() {
        PageHelper pageHelper = new PageHelper();

        Properties properties = new Properties();
        properties.setProperty("offsetAsPageNum", "true");
        properties.setProperty("rowBoundsWithCount", "true");
        properties.setProperty("reasonable", "true");
        properties.setProperty("returnPageInfo", "check");
        properties.setProperty("params", "count=countSql");

        pageHelper.setProperties(properties);
        return pageHelper;
    }
}
