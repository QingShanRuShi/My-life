package com.zxr.provider.leetcode.tree;

/**
 * @Description: 完全二叉树
 *  二叉树的深度为k，除第 k 层外，其它各层 (1～k-1) 的结点数都达到最大个数，第k 层所有的结点都连续集中在最左边
 * @Author: Zheng Xinrui
 * @Date: 18:15 2021/4/12
 */
public class CompleteBinaryTree {


    //计算完全二叉树的节点数
    //利用完全二叉树必有一个子树是满二叉树的特性，降低时间复杂度
    public int countComplete(Node root){
        int leftHeight = 0;
        int rightHeight = 0;
        Node leftHeightNode = root;
        Node rightHeightNode = root;
        while (leftHeightNode != null){
            leftHeight++;
            leftHeightNode = leftHeightNode.getLeftNode();
        }
        while (rightHeightNode != null){
            rightHeight++;
            rightHeightNode = rightHeightNode.getRightNode();
        }
        //如果左右字数深度一致，说明是满二叉树
        if(leftHeight == rightHeight){
            return (int) Math.pow(2, leftHeight) - 1;
        }
        //否则就按正常二叉树节点数计算
        return 1 + countComplete(root.getLeftNode()) + countComplete(root.getRightNode());
    }
}
