package com.zxr.provider.leetcode.lru;

import java.util.HashMap;

/**
 * @Description: lru实现 hashMap+linkedList；linkedHashMap
 * ⼀种缓存淘汰策略
 * LRU 缓存淘汰算法就是⼀种常⽤策略。LRU 的全称是 Least Recently
 * Used，也就是说我们认为最近使⽤过的数据应该是是「有⽤的」，很久都
 * 没⽤过的数据应该是⽆⽤的，内存满了就优先删那些很久没⽤过的数据。
 * @Author: Zheng Xinrui
 * @Date: 21:56 2021/3/16
 */
public class LRUCache {

    // key 映射到 Node(key, val)，map用来搜索查询
    HashMap<Integer, Node> map;
    // Node(k1, v1) <-> Node(k2, v2)...， list用来顺序的插入删除
    DoubleList cache;
    // 最⼤容量
    private final int cap;

    public LRUCache(int capacity) {
        this.cap = capacity;
        map = new HashMap<>();
        cache = new DoubleList();
    }

    /**
     * 获取指定key的缓存,并且刷新key对应的链表数据的优先级
     */
    public int get(int key) {
        if (!map.containsKey(key)) {
            //如果不存在key
            return -1;
        } else {
            //如果存在则返回，并且将对应链表数据的优先级提到开头
            Node node = map.get(key);
            int val = node.val;
            put(node.key, node.val);
            return val;
        }
    }

    /**
     * 存入lru缓存，存放位置位于链表开头
     */
    public void put(int key, int val) {
        Node x = new Node(key, val);
        if (map.containsKey(key)) {
            //把旧的数据删除,这里指的是删除在cache链表中的旧数据
            cache.remove(key);
            //将新节点 x 插⼊到开头；
            cache.addFirst(x);
            //更新map中的数据
            map.put(key, x);
        } else {
            if (cap == cache.size()) {
                //删除链表的最后⼀个数据腾位置；
                Node node = cache.removeLast();
                //删除 map 中映射到该数据的键；
                map.remove(node.key);
            }
            //将新节点 x 插⼊到开头；
            //map 中新建 key 对新节点 x 的映射；
            cache.addFirst(x);
            map.put(key, x);
        }
    }


}
