package com.zxr.provider.leetcode.tree;

/**
 * @Description: 满二叉树
 *  每层都是是满的，节点个数为2^n-1，n为树的层数
 * @Author: Zheng Xinrui
 * @Date: 18:14 2021/4/12
 */
public class PerfectBinaryTree {


    //判断二叉树是否为满二叉树
    public boolean isPerfect(Node root){
        //通过计算左右所有节点的左右字数的高度判断
        int leftHeight = 0;
        int rightHeight = 0;
        Node leftHeightNode = root;
        Node rightHeightNode = root;
        while (leftHeightNode != null){
            leftHeight++;
            leftHeightNode = leftHeightNode.getLeftNode();
        }
        while (rightHeightNode != null){
            rightHeight++;
            rightHeightNode = rightHeightNode.getRightNode();
        }
        return leftHeight == rightHeight;
    }

    //计算满二叉树的节点数
    //每层都是2的n次方，n从0开始
    public int countPerfect(Node root){
        int height = 0;
        while (root != null){
            height++;
            root = root.getLeftNode();
        }
        return (int)Math.pow(2, height) - 1;
    }
}
