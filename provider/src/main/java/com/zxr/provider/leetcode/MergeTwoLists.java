package com.zxr.provider.leetcode;

//将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
//
//
//
// 示例 1：
//
//
//输入：l1 = [1,2,4], l2 = [1,3,4]
//输出：[1,1,2,3,4,4]
//
//
// 示例 2：
//
//
//输入：l1 = [], l2 = []
//输出：[]
//
//
// 示例 3：
//
//
//输入：l1 = [], l2 = [0]
//输出：[0]
//
//
//
//
// 提示：
//
//
// 两个链表的节点数目范围是 [0, 50]
// -100 <= Node.val <= 100
// l1 和 l2 均按 非递减顺序 排列
//
// Related Topics 递归 链表
// 👍 1585 👎 0

import java.util.ArrayList;

/**
 * @Description: 合并链表
 * @Author: Zheng Xinrui
 * @Date: 13:29 2021/3/9
 */
public class MergeTwoLists {

    public static void main(String[] args) {
        ListNode l13 = new ListNode(5, null);
        ListNode l12 = new ListNode(4, l13);
        ListNode l11 = new ListNode(2, l12);
        ListNode l1 = new ListNode(1, l11);

        ListNode l23 = new ListNode(4, null);
        ListNode l22 = new ListNode(3, l23);
        ListNode l21 = new ListNode(1, l22);
        ListNode l2 = new ListNode(0, l21);
        new ArrayList<>();
        ListNode listNode = mergeTwoLists(l1, l2);
        System.out.println(listNode.toString());
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        System.out.println(l1);
        System.out.println(l2);
        System.out.println("=================");
        if(null == l1){
            return l2;
        }else if(null == l2){
            return l1;
        }else if(l1.val <= l2.val){
            l1.next = mergeTwoLists(l1.next, l2);
            System.out.println("l1: " + l1);
            return l1;
        }else {
            l2.next = mergeTwoLists(l1, l2.next);
            System.out.println("l2: " + l2);
            return l2;
        }
    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }
}
