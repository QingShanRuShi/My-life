package com.zxr.provider.leetcode.lru;

/**
 * @Description: 测试lru缓存
 * @Author: Zheng Xinrui
 * @Date: 9:55 2021/3/17
 */
public class Test {

    public static void main(String[] args) {
        LRUCache lruCache = new LRUCache(5);
        lruCache.put(1, 100);
        lruCache.put(2, 102);
        lruCache.put(3, 104);
        lruCache.put(4, 106);

        System.out.println(lruCache.cache.toString());
        System.out.println(lruCache.map.toString());
        System.out.println();

        int i = lruCache.get(2);
        System.out.println(i);
        System.out.println(lruCache.cache.toString());

        lruCache.put(5, 30);
        System.out.println();
        System.out.println(lruCache.cache.toString());
        System.out.println(lruCache.map.toString());
        lruCache.put(6, 35);
        System.out.println();
        System.out.println(lruCache.cache.toString());
        System.out.println(lruCache.map.toString());
    }
}
