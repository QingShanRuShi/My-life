package com.zxr.provider.leetcode;

//给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
//
// 示例 1:
//
// 输入: 123
//输出: 321
//
//
// 示例 2:
//
// 输入: -123
//输出: -321
//
//
// 示例 3:
//
// 输入: 120
//输出: 21
//
//
// 注意:
//
// 假设我们的环境只能存储得下 32 位的有符号整数，则其数值范围为 [−2^31, 2^31 − 1]。请根据这个假设，如果反转后整数溢出那么就返回 0。
// Related Topics 数学
// 👍 2318 👎 0

/**
 * @Description: 整数反转
 * @Author: Zheng Xinrui
 * @Date: 10:11 2021/3/3
 */
public class Reverse {

    public static void main(String[] args) {
        long reversed = 0;
        int reverse = -1230;
        int reverse2 = reverse;
        while (reverse != 0){
            int imo = reverse % 10;
            reverse = reverse / 10;
            reversed = reversed * 10 + Math.abs(imo);
        }
        if(reversed > 0x7fffffff || reversed < 0x80000000){
            reversed = 0;
        }
        if(reverse2 < 0){
            reversed = -reversed;
        }
        System.out.println(reversed);
    }
}
