package com.zxr.provider.leetcode.tree;

/**
 * @Description: 二叉树
 * @Author: Zheng Xinrui
 * @Date: 15:29 2021/3/18
 */
public class BinaryTree {

    //节点
    public Node root;

    public void setHeadNode(Node node){
        this.root = node;
    }

    //前序遍历
    public void preOrder(){
        if(root != null){
            System.out.println("前序遍历");
            root.preOrder();
        }else {
            System.out.println("树为空，无法进行前序遍历");
        }
    }

    //中序遍历
    public void infixOrder(){
        if(root != null){
            System.out.println("中序遍历");
            root.infixOrder();
        }else {
            System.out.println("树为空，无法进行中序遍历");
        }
    }

    //后序遍历
    public void postOrder(){
        if(root != null){
            System.out.println("后续遍历");
            root.postOrder();
        }else {
            System.out.println("树为空，无法进行后序遍历");
        }
    }

    //计算二叉树节点数
    public long countNodes(Node root){
        if(root == null){
            return 0;
        }
        return 1 + countNodes(root.getLeftNode()) + countNodes(root.getRightNode());
    }

    //二叉树操作框架
    public void traverse(Node root) {
        // root 需要做什么, 在这做。
        // 其他的不⽤ root 操⼼，抛给框架
        traverse(root.getLeftNode());
        traverse(root.getRightNode());
    }

    //操作框架使用示例
    //讲所有节点的值都加1
    public void plusOne(Node root){
        if(root == null){
            return;
        }
        // root 需要做什么, 在这做。
        root.setContent(root.getContent()+1);
        // 其他的不⽤ root 操⼼，抛给框架
        plusOne(root.getLeftNode());
        plusOne(root.getRightNode());
    }

    //操作框架使用示例
    //判断两个二叉树是否相同
    public boolean isSameTree(Node root1, Node root2){
        // root 需要做什么, 在这做。
        if(root1 == null && root2 == null){
            return true;
        }else if(root1 == null || root2 == null){
            return false;
        }else if(!root1.getContent().equals(root2.getContent())){
            return false;
        }else {
            // 其他的不⽤ root 操⼼，抛给框架
            return isSameTree(root1.getLeftNode(), root2.getLeftNode()) &&
                    isSameTree(root1.getRightNode(), root2.getRightNode());
        }
    }


    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        Node root = Node.initExample();
        binaryTree.setHeadNode(root);
        binaryTree.preOrder();
//        binaryTree.infixOrder();
//        binaryTree.postOrder();
//
//        binaryTree.plusOne(binaryTree.root);
//        binaryTree.preOrder();
//
//        boolean sameTree = binaryTree.isSameTree(root, root);
//        Node node = Node.initExample();
//        //这里的root，因为上面binaryTree.plusOne(binaryTree.root);，给每个节点加过1了有变化
//        boolean sameTree2 = binaryTree.isSameTree(root, node);
//        System.out.println(sameTree);
//        System.out.println(sameTree2);
    }
}
