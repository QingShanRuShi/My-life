package com.zxr.provider.leetcode.lru;


import java.util.LinkedList;

/**
 * @Description: 链表存储
 * @Author: Zheng Xinrui
 * @Date: 20:33 2021/3/16
 */
public class DoubleList {

    /**
     * 第一个节点
     */
    private Node firstNode;

    /**
     * 最后一个节点
     */
    private Node lastNode;

    /**
     * 链表大小
     */
    private int size;

    /**
     * 永远记得firstNode和lastNode都是在操作一个引用地址的对象
     * 特殊的前三次插入操作：
     * 1.第一次插入，初始化first和last，此时first和last的next和prev都是空都一样
     * 2.第二次插入，first变成了被插入的node，last此时的prev变成了被插入的node, first的next变成了last
     * 3.第三次插入，first变成了被插入的node, last此时没有任何改变它的prev依旧是第二步被插入的node，但是现在的first的next却是指向第二步完成之后的first
     * 4.第四次插入，first变成了被插入的node, last此时没有任何改变它的prev依旧是第二步被插入的node，但是现在的first的next却是指向第三步完成之后的first
     * 总结，所以从头部插入节点，除了前两次last有特殊变化，后面的插入和last无关，都是在first上面操作，老first的prev指向新first，新first的next指向老first
     */
    // 在链表头部添加节点 x，时间 O(1)
    public void addFirst(Node x){
        Node nodeTmp = firstNode;
        x.next = firstNode;
        firstNode = x;
        if(nodeTmp == null){
            //末节点初始化
            lastNode = x;
        }else {
            //上一个首节点增加前节点
            nodeTmp.prev = x;
        }
        size++;
    }

    //在链表的尾部添加节点
    //就是在链表头部添加节点的不同位置的反操作，除了前两次后面的插入都是last变动
    public void addLast(Node x){
        Node nodeTmp = lastNode;
        x.prev = lastNode;
        lastNode = x;
        if(nodeTmp == null){
            firstNode = x;
        }else {
            nodeTmp.next = x;
        }
        size++;
    }

    /**
     * 指定位置前插入节点
     *
     * @param index 位置编号
     * @param x 插入的节点
     */
    public void addNode(int index, Node x){
        Node node = node(index);
        Node prev = node.prev;
        x.prev = prev;
        x.next = node;
        node.prev = x;
        if(prev == null){
            firstNode = x;
        }else {
            prev.next = x;
        }
        size++;
    }

    // 删除链表中的 x 节点（x ⼀定存在）
    // 由于是双链表且给的是⽬标 Node 节点，时间 O(1)
    //这里node直接通过外部传进来是有问题的，必须是使用当前DoubleList对象的链表引用地址才行，不然就是对传进来的对象进行操作,
    // 涉及不到first或者last指向的链表地址也就是涉及不到当前DoubleList对象的链表数据更改
    public void remove(Node x){

        Node next = x.next;
        Node prev = x.prev;

        //如果上一个节点是null，说明这个节点是首节点
        if(prev == null){
            firstNode = next;
        }else {
            prev.next = next;
            x.prev = null;
        }
        //如果下一个节点是null，说明这个节点是未节点
        if(next == null){
            lastNode = prev;
        }else {
            next.prev = prev;
            x.next = null;
        }
        x.key = null;
        x.val = null;
        size--;
    }


    // 删除链表中的 x 节点（x ⼀定存在）
    // 由于是双链表且给的是⽬标 Node 节点，时间 O(n)
    //根据key的唯一性去遍历查询node节点，进行删除

    /**
     * 删除node里指定key的节点
     *
     * @param key node的成员变量，具有唯一性
     */
    public void remove(int key){
        Node node = firstNode;
        while (node != null){
            if(key == node.key){
                remove(node);
                return;
            }
            node = node.next;
        }
    }

    // 删除链表中最后⼀个节点，并返回该节点，时间 O(1)
    public Node removeLast() {

        if(lastNode == null){
            return null;
        }
        Node node = new Node(lastNode.key, lastNode.val);
        Node prev = lastNode.prev;
        //将末位节点删除
        lastNode.prev = null;
        lastNode.key = null;
        lastNode.val = null;
        //末位节点等于被删的末位节点的上一个节点
        lastNode = prev;
        //如果末节点的上一个节点是空，说明当前的链表就一个存储
        if(prev == null){
            firstNode = null;
        }else {
            prev.next = null;
        }
        size--;
        return node;
    }

    /**
     * 获取指定节点位置的node
     *
     * @param index 指定位置, 0为第一个位置
     * @return 返回指定位置的Node
     */
    public Node node(int index){
        if(index < (size>>1)){
            Node node = firstNode;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
            return node;
        }else {
            Node node = lastNode;
            for (int i = size-1; i > index; i--) {
                node = node.prev;
            }
            return node;
        }
    }

    // 返回链表⻓度，时间 O(1)
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder s1 = new StringBuilder();
        Node node1 = firstNode;
        while (node1 != null){
            s1.append("(").append(node1.key).append(",").append(node1.val).append("),");
            node1 = node1.next;
        }
        StringBuilder s2 = new StringBuilder();
        Node node2 = lastNode;
        while (node2 != null){
            s2.append("(").append(node2.key).append(",").append(node2.val).append("),");
            node2 = node2.prev;
        }
        String s = s1.toString();
        String s3 = s2.toString();
        return "first: " + s.substring(0, s.length()-1) + "\n" + "last: "+ s3.substring(0, s3.length()-1) + "\n";
    }

    public static void main(String[] args) {
        DoubleList doubleList = new DoubleList();
        Node node = new Node(1, 2);
        Node node2 = new Node(3, 4);
        Node node3 = new Node(5, 6);
        Node node4 = new Node(7, 8);
        Node node5 = new Node(-1, 0);

        doubleList.addFirst(node);
        System.out.println(doubleList);
        doubleList.addFirst(node2);
        System.out.println(doubleList);
        doubleList.addFirst(node3);
        System.out.println(doubleList);
        doubleList.addFirst(node4);
        System.out.println(doubleList);
        doubleList.addLast(node5);
        System.out.println(doubleList);

        doubleList.addNode(4, new Node(100, 88));
        System.out.println(doubleList);

        LinkedList<Integer> integers = new LinkedList<>();
        integers.addFirst(1);
        System.out.println(integers.toString());
        integers.addFirst(2);
        System.out.println(integers.toString());
        integers.addFirst(3);
        System.out.println(integers.toString());
        integers.addFirst(4);
        System.out.println(integers.getLast());
        integers.addFirst(5);
        System.out.println(integers.toString());
        integers.addFirst(6);
        System.out.println(integers.toString());
        integers.addFirst(7);
        System.out.println(integers.toString());
        integers.addFirst(8);
        System.out.println(integers.toString());
    }
}
