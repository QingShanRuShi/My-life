package com.zxr.provider.leetcode;

//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
//
// 有效字符串需满足：
//
//
// 左括号必须用相同类型的右括号闭合。
// 左括号必须以正确的顺序闭合。
//
//
//
//
// 示例 1：
//
//
//输入：s = "()"
//输出：true
//
//
// 示例 2：
//
//
//输入：s = "()[]{}"
//输出：true
//
//
// 示例 3：
//
//
//输入：s = "(]"
//输出：false
//
//
// 示例 4：
//
//
//输入：s = "([)]"
//输出：false
//
//
// 示例 5：
//
//
//输入：s = "{[]}"
//输出：true
//
//
//
// 提示：
//
//
// 1 <= s.length <= 104
// s 仅由括号 '()[]{}' 组成
//
// Related Topics 栈 字符串
// 👍 2216 👎 0

import java.util.HashMap;
import java.util.Stack;

/**
 * @Description: 有效的括号
 * @Author: Zheng Xinrui
 * @Date: 10:54 2021/3/9
 */
public class IsValid {

    //使用栈的知识
    public static void main(String[] args) {

        String s = "{([]())}";

        HashMap<Character, Character> map = new HashMap<>();
        map.put('{','}');
        map.put('(',')');
        map.put('[',']');

        Stack<Character> stack = new Stack<>();
        char[] chars = s.toCharArray();
        for (char aChar : chars) {
            if (map.containsKey(aChar)) {
                stack.push(map.get(aChar));
            } else if (stack.isEmpty() || stack.pop() != aChar) {
                System.out.println(false);
            }
        }
        System.out.println(stack.isEmpty());
    }
}
