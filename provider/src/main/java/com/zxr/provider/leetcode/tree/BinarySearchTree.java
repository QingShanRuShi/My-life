package com.zxr.provider.leetcode.tree;

/**
 * @Description: 二叉搜索树BST
 * ⼆叉搜索树（Binary Search Tree，简称 BST）是⼀种很常⽤的的⼆叉树，又叫二叉查找树。它
 * 的定义是：⼀个⼆叉树中，任意节点的值要⼤于等于左⼦树所有节点的值，
 * 且要⼩于等于右边⼦树的所有节点的值。
 * 二叉搜索树的中序遍历是一个递增序列
 * @Author: Zheng Xinrui
 * @Date: 17:03 2021/3/18
 */
public class BinarySearchTree {

    //节点
    public Node root;

    public void setHeadNode(Node node){
        this.root = node;
    }

    //判断二叉树是否是二叉搜索树
    public boolean isValidBST(Node root){
        return isValidBST(root, null, null);
    }

    //BST遍历框架
    public void BST(Node root, int target){
        if (root.getContent() == target){
            // 找到⽬标，做点什么
        } else if (root.getContent() < target){
            BST(root.getRightNode(), target);
        }else if (root.getContent() > target){
            BST(root.getLeftNode(), target);
        }
    }


    /**
     * 判断二叉树是否是二叉搜索树
     *
     * @param root 需要判断的子节点
     * @param min 判断左子树的时候，该参数表示当前树中的右子树的根节点，判断右子树时候，该参数表示当前树的根节点
     * @param max 判断左子树的时候，该参数表示当前树的根节点，判断右子树时候，该参数表示当前树中的左子树的根节点
     * @return
     */
    boolean isValidBST(Node root, Node min, Node max) {
        //如果没有节点，那也是BST
        if (root == null) {
            return true;
        }
        //节点是否满足任意节点的值大于等于左子树所有节点值，就是说所有左子树都要小于等于所有上层节点
        //节点是否满足任意节点的值小于等于右子树所有节点的值，就是说所有右子树都要大于等于所有上层节点

        //所有右子树都要大于等于根节点
        if (min != null && root.getContent() < min.getContent()) {
            return false;
        }
        //所有左子树都要小于等于根节点
        if (max != null && root.getContent() > max.getContent()) {
            return false;
        }
        return isValidBST(root.getLeftNode(), min, root) &&
                isValidBST(root.getRightNode(), root, max);
    }

    //判断一个数在BST中是否存在。套用上面的BST遍历框架
    boolean isInBST(Node root, int target){
        if(root == null){
            return false;
        }
        if(target == root.getContent()){
            return true;
        }
        //这种方式可以用来判断树中是否有节点等于target
        /*return isInBST(root.getLeftNode(), target) ||
                isInBST(root.getRightNode(), target);*/
        //BST树的性质可以利用起来，类似于二分
        if(target > root.getContent()){
            return isInBST(root.getRightNode(), target);
        }else {
            return isInBST(root.getLeftNode(), target);
        }
    }

    //插入一个数
    //二叉搜索树其实已经排好序的，插入一个数最终是插在某个叶子结点的末位，利用根节点永远大于左半部分节点和永远小于右半部分节点特性
    //套用上面的BST框架
    Node insertNode(Node root, int target){
        if(root == null){
            return new Node(target);
        }
        if(target == root.getContent()){
            root.setContent(target);
        } else if (root.getContent() < target){
            root.setRightNode(insertNode(root.getRightNode(), target));
        } else if (root.getContent() > target){
            root.setLeftNode(insertNode(root.getLeftNode(), target));
        }
        return root;
    }

    //获取最小值的节点，BST最左边的节点就是最小的
    Node getMiniNode(Node root){
        while (root.getLeftNode() != null){
            root = root.getLeftNode();
        }
        return root;
    }

    //获取最大值的节点，BST最右边的节点就是最大的
    Node getMaxNode(Node root){
        while (root.getRightNode() != null){
            root = root.getRightNode();
        }
        return root;
    }

    //删除一个数（该数存在于当前树中）
    //套用上面的BST递归框架
    //删除的复杂点在于找到需要删除的数的位置，删了之后怎么维持BST的性质
    Node deleteNode(Node root, int target){
        if (root.getContent() == target){
            //删除操作
            //1.删除的节点是叶子结点,那么就直接删除
            if(root.getRightNode() == null && root.getLeftNode() == null){
                return null;
            }
            //2.删除的节点只有一个子节点，那么唯一的子节点上位即可
            if(root.getLeftNode() == null){
                return root.getRightNode();
            }else if(root.getRightNode() == null){
                return root.getLeftNode();
            }else {
                //3.删除的节点有两个子节点，判断左子节点最大值或者右子节点最小值替换到需要删除的节点位置，然后删除左子节点最大值或者右子节点最小值即可
                //找到右子树的最小值（右子树为例）
                Node miniNode = getMiniNode(root.getRightNode());
                //替换节点，如果是双向链表结构，只需要变动指针指向即可
                root.setContent(miniNode.getContent());
                //删除叶子结点
                root.setRightNode(deleteNode(root.getRightNode(), miniNode.getContent()));
            }
        } else if (root.getContent() < target){
            root.setRightNode(deleteNode(root.getRightNode(), target));
        } else if (root.getContent() > target){
            root.setLeftNode(deleteNode(root.getLeftNode(), target));
        }
        return root;
        //写在删除的最后，会发现删除挺复杂，大部分情况下真的需要去进行这个物理删除的步骤吗，可以通过给Node增加一个“有效”的标识，当删除BST树节点时，
        //找到该节点后，改变其状态为“失效（已经被删除）”，这样设计要树查找时要增加额外的判断是否有效逻辑，相关取舍根据实际场景自行选择
    }

    public static void main(String[] args) {
        Node node1 = new Node(5);
        Node node2 = new Node(2);
        Node node3 = new Node(6);
        Node node4 = new Node(1);
        Node node5 = new Node(4);
        Node node6 = new Node(8);
        Node node7 = new Node(3);
        node1.setLeftNode(node2);
        node1.setRightNode(node3);
        node2.setLeftNode(node4);
        node2.setRightNode(node5);
        node5.setLeftNode(node7);
        node3.setRightNode(node6);
//        node1.infixOrder();
//        node1.preOrder();

        BinarySearchTree binarySearchTree = new BinarySearchTree();
        boolean validBST = binarySearchTree.isValidBST(node1);
        System.out.println(validBST);

        boolean inBST = binarySearchTree.isInBST(node1, 3);
        System.out.println(inBST);

        Node node = binarySearchTree.insertNode(node1, 7);
        System.out.println(binarySearchTree.isValidBST(node));

        Node node8 = binarySearchTree.deleteNode(node, 5);
        System.out.println(binarySearchTree.isValidBST(node8));
    }

}
