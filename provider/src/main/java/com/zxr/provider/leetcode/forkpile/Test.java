package com.zxr.provider.leetcode.forkpile;

import java.util.Arrays;

/**
 * @Description: 测试二叉堆
 * ⼆叉堆其实就是⼀种特殊的⼆叉树（完全⼆叉树），只不过存储在数
 * 组⾥。⼀般的链表⼆叉树，我们操作节点的指针，⽽在数组⾥，我们把数组
 * 索引作为指针
 * ⼆叉堆还分为最⼤堆和最⼩堆。最⼤堆的性质是：每个节点都⼤于等于它的
 * 两个⼦节点。类似的，最⼩堆的性质是：每个节点都⼩于等于它的⼦节点。
 * @Author: Zheng Xinrui
 * @Date: 13:19 2021/3/16
 */
public class Test {

    public static void main(String[] args) {
//        testMaxPQ();
        testMinPQ();
    }

    private static void testMinPQ(){
        MinPQ<Integer> integerMinPQ = new MinPQ<>(20);
        integerMinPQ.insert(1);
        integerMinPQ.insert(5);
        integerMinPQ.insert(2);
        integerMinPQ.insert(13);
        integerMinPQ.insert(55);
        integerMinPQ.insert(11);
        integerMinPQ.insert(12);
        integerMinPQ.insert(10);
        integerMinPQ.insert(3);
        integerMinPQ.insert(3);
        integerMinPQ.insert(6);
        integerMinPQ.insert(56);
        integerMinPQ.insert(100);
        integerMinPQ.insert(22);
        integerMinPQ.insert(21);
        integerMinPQ.insert(23);
        integerMinPQ.insert(44);
        integerMinPQ.insert(88);
        integerMinPQ.insert(880);
        integerMinPQ.insert(77);
        System.out.println(integerMinPQ.getN());
        System.out.println(Arrays.toString(integerMinPQ.getPq()));

        Integer integer = integerMinPQ.delMax();
        System.out.println("min " + integer);
        System.out.println(integerMinPQ.getN());
        System.out.println(Arrays.toString(integerMinPQ.getPq()));
    }

    private static void testMaxPQ() {
        MaxPQ<Integer> integerMaxPQ = new MaxPQ<>(20);
        integerMaxPQ.insert(1);
        integerMaxPQ.insert(5);
        integerMaxPQ.insert(2);
        integerMaxPQ.insert(13);
        integerMaxPQ.insert(55);
        integerMaxPQ.insert(11);
        integerMaxPQ.insert(12);
        integerMaxPQ.insert(10);
        integerMaxPQ.insert(3);
        integerMaxPQ.insert(3);
        integerMaxPQ.insert(6);
        integerMaxPQ.insert(56);
        integerMaxPQ.insert(100);
        integerMaxPQ.insert(22);
        integerMaxPQ.insert(21);
        integerMaxPQ.insert(23);
        integerMaxPQ.insert(44);
        integerMaxPQ.insert(88);
        integerMaxPQ.insert(880);
        integerMaxPQ.insert(77);
        System.out.println(integerMaxPQ.getN());
        System.out.println(Arrays.toString(integerMaxPQ.getPq()));

        Integer integer = integerMaxPQ.delMax();
        System.out.println("max " + integer);
        System.out.println(integerMaxPQ.getN());
        System.out.println(Arrays.toString(integerMaxPQ.getPq()));
    }
}
