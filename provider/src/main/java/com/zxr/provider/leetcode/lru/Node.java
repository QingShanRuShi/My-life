package com.zxr.provider.leetcode.lru;

/**
 * @Description: 链表node，这里直接指定类型为int了
 * @Author: Zheng Xinrui
 * @Date: 20:24 2021/3/16
 */
public class Node {

    /**
     * 在hash表中存储key值
     */
    public Integer key;
    /**
     * 实际存储的值
     */
    public Integer val;

    /**
     * 前节点
     */
    public Node prev;

    /**
     * 后节点
     */
    public Node next;

    public Node(Integer k, Integer v) {
        this.key = k;
        this.val = v;
    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", val=" + val +
                '}';
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
