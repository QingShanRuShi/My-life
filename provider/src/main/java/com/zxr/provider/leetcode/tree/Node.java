package com.zxr.provider.leetcode.tree;

/**
 * @Description: 节点
 * @Author: Zheng Xinrui
 * @Date: 14:47 2021/3/18
 */
public class Node {

    /**
     * 节点内容
     */
    private Integer content;

    /**
     * 左节点
     */
    private Node leftNode;

    /**
     * 右节点
     */
    private Node rightNode;

    public Node(Integer content) {
        this.content = content;
    }

    public Integer getContent() {
        return content;
    }

    public void setContent(Integer content) {
        this.content = content;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node leftNode) {
        this.leftNode = leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node rightNode) {
        this.rightNode = rightNode;
    }

    //前序遍历
    //先输出父节点，再以此遍历左子树和右子树
    public void preOrder(){
        System.out.println(this);
        if(this.leftNode != null){
            this.leftNode.preOrder();
        }
        if(this.rightNode != null){
            this.rightNode.preOrder();
        }
    }

    //中序遍历
    //先遍历左子树，再输出父节点，再遍历右子树
    public void infixOrder(){
        if(this.leftNode != null){
            this.leftNode.infixOrder();
        }
        System.out.println(this);
        if(this.rightNode != null){
            this.rightNode.infixOrder();
        }
    }

    //后序遍历
    //先遍历左子树，再遍历右子树，最后输出父节点
    public void postOrder(){
        if(this.leftNode != null){
            this.leftNode.postOrder();
        }
        if(this.rightNode != null){
            this.rightNode.postOrder();
        }
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Node{" +
                "content=" + content +
                '}';
    }

    /**
     * 生成一个示例
     */
    public static Node initExample(){
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        Node node6 = new Node(6);
        Node node7 = new Node(7);
        Node node8 = new Node(8);
        Node node9 = new Node(9);
        Node node10 = new Node(10);
        Node node11 = new Node(11);
        node1.setLeftNode(node2);
        node1.setRightNode(node3);
        node2.setLeftNode(node4);
        node2.setRightNode(node5);
        node3.setLeftNode(node6);
        node3.setRightNode(node7);
        node4.setLeftNode(node8);
        node4.setRightNode(node9);
        node5.setLeftNode(node10);
        node5.setRightNode(node11);
        return node1;
    }

    public static void main(String[] args) {

        Node node = initExample();

        System.out.println("前序遍历");
        node.preOrder();
        System.out.println("中序遍历");
        node.infixOrder();
        System.out.println("后序遍历");
        node.postOrder();

    }
}
