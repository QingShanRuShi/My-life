package com.zxr.provider.leetcode.forkpile;

/**
 * @Description: 二叉堆，优先队列，最大堆
 * @Author: Zheng Xinrui
 * @Date: 13:05 2021/3/16
 */
public class MaxPQ<Key extends Comparable<Key>> {

    // 存储元素的数组
    private final Key[] pq;

    // 当前 Priority Queue 中的元素个数
    private int N = 0;

    /**
     * 初始化堆空间
     *
     * @param cap 堆大小
     */
    public MaxPQ(int cap) {
        //索引 0 不⽤，所以多分配⼀个空间
        pq = (Key[]) new Comparable[cap + 1];
    }

    /**
     * 返回当前队列中最⼤元素
     *
     */
    public Key max() {
        return pq[1];
    }

    /**
     * ⽗节点的索引
     */
    int parent(int root) {
        return root / 2;
    }

    /**
     * 左孩⼦的索引
     */
    int left(int root) {
        return root * 2;
    }

    /**
     * 右孩⼦的索引
     */
    int right(int root) {
        return root * 2 + 1;
    }

    /**
     * 交换数组的两个位置元素
     *
     * @param i 节点1
     * @param j 节点2
     */
    private void exch(int i, int j) {
        Key temp = pq[i];
        pq[i] = pq[j];
        pq[j] = temp;
    }

    /**
     * pq[i] 是否⽐ pq[j] ⼩
     */
    private boolean less(int i, int j) {
        return pq[i].compareTo(pq[j]) < 0;
    }

    /**
     * 插⼊元素 e
     * 把要插⼊的元素添加到堆底的最后，然后让其上浮到正确位置
     */
    public void insert(Key e) {
        N++;
        // 先把新元素加到最后
        pq[N] = e;
        // 然后让它上浮到正确的位置
        swim(N);
    }

    /**
     * 删除当前队列中最⼤元素并返回
     * 先把堆顶元素 A 和堆底最后的元素 B 对调，然后删除 A，最后让 B 下沉到正确位置
     */
    public Key delMax() {
        // 最⼤堆的堆顶就是最⼤元素
        Key max = pq[1];
        // 把这个最⼤元素换到最后，删除之
        exch(1, N);
        pq[N] = null;
        N--;
        // 让 pq[1] 下沉到正确位置
        sink(1);
        return max;
    }

    /**
     * 上浮第 k 个元素，以维护最⼤堆性质
     */
    private void swim(int k) {
        // 如果浮到堆顶，就不能再上浮了
        while (k > 1 && less(parent(k), k)) {
            // 如果第 k 个元素⽐上层⼤
            // 将 k 换上去
            exch(parent(k), k);
            k = parent(k);
        }

    }

    /**
     * 下沉第 k 个元素，以维护最⼤堆性质
     */
    private void sink(int k) {
        // 如果沉到堆底，就沉不下去了
        while (left(k) <= N) {
            // 先假设左边节点较⼤
            int older = left(k);
            // 如果右边节点存在，⽐⼀下⼤⼩
            if (right(k) <= N && less(older, right(k)))
                older = right(k);
            // 结点 k ⽐俩孩⼦都⼤，就不必下沉了
            if (less(older, k)) break;
            // 否则，不符合最⼤堆的结构，下沉 k 结点
            exch(k, older);
            k = older;
        }
    }

    public Key[] getPq() {
        return pq;
    }

    public int getN() {
        return N;
    }
}
