package com.zxr.provider.leetcode;

//实现 strStr() 函数。
//
// 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如
//果不存在，则返回 -1。
//
// 示例 1:
//
// 输入: haystack = "hello", needle = "ll"
//输出: 2
//
//
// 示例 2:
//
// 输入: haystack = "aaaaa", needle = "bba"
//输出: -1
//
//
// 说明:
//
// 当 needle 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
//
// 对于本题而言，当 needle 是空字符串时我们应当返回 0 。这与C语言的 strstr() 以及 Java的 indexOf() 定义相符。
// Related Topics 双指针 字符串
// 👍 725 👎 0


/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 22:46 2022/3/14
 */
public class StrStr {

    public static void main(String[] args) {
        String  haystack = "aaa1aa", needle = "1aaa";
        System.out.println(strStr(haystack, needle));
    }

    // 还可以优化成不使用substring，将两个参数都变成字符数组，
    //源数组找到目标数组的起始位置，挨个位置取值比较，速度更快，substring比较耗费时间
    private static int strStr(String  haystack, String needle){
        if (needle == null){
            return 0;
        }
        int hayLen = haystack.length();
        int needLen  = needle.length();
        if(hayLen < needLen){
            return -1;
        }
        if(hayLen == needLen && haystack.equals(needle)){
            return 0;
        }
        for (int i = 0; i < hayLen; i++) {
            int j = i + needLen;
            if(hayLen < j){
                return -1;
            }
            if (haystack.substring(i, j).equals(needle)) {
                return i;
            }
        }
        return -1;
    }
}
