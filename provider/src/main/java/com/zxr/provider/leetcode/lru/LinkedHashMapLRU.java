package com.zxr.provider.leetcode.lru;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Description: 使用LinkedHashMap实现lru
 * @Author: Zheng Xinrui
 * @Date: 22:08 2022/6/30
 */
public class LinkedHashMapLRU<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = -3419984235688685837L;
    private final int CACHE_SIZE;

    /**
     * 传递进来最多能缓存多少数据
     *
     * @param cacheSize 缓存大小
     */
    public LinkedHashMapLRU(int cacheSize) {
        // true 表示让 linkedHashMap 按照访问顺序来进行排序，最近访问的放在头部，最老访问的放在尾部。
        super((int) Math.ceil(cacheSize / 0.75) + 1, 0.75f, true);
        CACHE_SIZE = cacheSize;
    }

    // 当 map中的数据量大于指定的缓存个数的时候，就自动删除最老的数据。
    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return super.size() > CACHE_SIZE;
    }
}
