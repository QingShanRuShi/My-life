package com.zxr.provider.leetcode;

//编写一个函数来查找字符串数组中的最长公共前缀。
//
// 如果不存在公共前缀，返回空字符串 ""。
//
//
//
// 示例 1：
//
//
//输入：strs = ["flower","flow","flight"]
//输出："fl"
//
//
// 示例 2：
//
//
//输入：strs = ["dog","racecar","car"]
//输出：""
//解释：输入不存在公共前缀。
//
//
//
// 提示：
//
//
// 0 <= strs.length <= 200
// 0 <= strs[i].length <= 200
// strs[i] 仅由小写英文字母组成
//
// Related Topics 字符串
// 👍 1483 👎 0

/**
 * @Description: 最长公共前缀
 * @Author: Zheng Xinrui
 * @Date: 14:45 2021/3/8
 */
public class LongestCommonPrefix {

    public static void main(String[] args) {
        String[] strs = {"flower","flow","floight"};
        /*String resultStr = "";
        if(strs.length < 1){
            return resultStr;
        }
        char[] chars1 = strs[0].toCharArray();
        if(chars1.length < 1){
            return resultStr;
        }
        String c = "";
        for (int i = 0; i < chars1.length; i++) {
            String resultStr1 = String.valueOf(chars1[i]);
            c += resultStr1;
            int i1 = 1;
            boolean flag = true;
            while (i1 < strs.length){
                if(!strs[i1].startsWith(c)) {
                    flag = false;
                    break;
                }
                i1++;
            }
            if(!flag){
                break;
            }else {
                resultStr += resultStr1;
            }
        }
        System.out.println(resultStr);*/

        if(strs.length==0){
            System.out.println("");
        }
        int index=0;
        while(true){
            if(index==strs[0].length()){
                break;
            }
            char c=strs[0].charAt(index);
            for(int i=1;i<strs.length;i++){
                if(strs[i].length()<=index||strs[i].charAt(index)!=c){
                    System.out.println(strs[0].substring(0,index));
                    break;
                }
            }
            index++;
        }
        System.out.println(strs[0].substring(0,index));
    }
}
