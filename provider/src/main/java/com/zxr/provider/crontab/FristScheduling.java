package com.zxr.provider.crontab;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Description: 定时任务
 * @Author: Zheng Xinrui
 * @Date: 16:48 2019/4/30
 */
@Component
@Slf4j
public class FristScheduling {


    @Scheduled(cron = "${scheduled.test}")
//    @Scheduled(cron = "0/60 * * * * ?")
    public void fristScheduling(){
        log.info("60秒一次的定时任务======路过");
    }

}
