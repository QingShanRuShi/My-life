package com.zxr.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zxr.common.vm.Result;
import com.zxr.providerapi.dto.UserDTO;
import com.zxr.providerapi.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 16:38 2019/4/28
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Reference(check = false)
    private UserService userService;

    @GetMapping("test")
    public Result<UserDTO> test(){
        return userService.getUser(1);
    }
}
