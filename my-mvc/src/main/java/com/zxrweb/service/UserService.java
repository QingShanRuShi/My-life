package com.zxrweb.service;

import com.zxrweb.bean.User;


public interface UserService {

     public  void  findUser();

     public User getUser();
}
