package com.zxrweb.service.impl;

import com.zxrweb.bean.User;
import com.zxrweb.service.UserService;
import com.springmvc.annotation.Service;


@Service(value="userService")
public class UserServiceImpl implements UserService {


    public  void  findUser(){
        System.out.println("====调用UserServiceImpl==findUser===");
    }

    public User getUser(){

       return new User(1,"老王","admin");
    }

}
