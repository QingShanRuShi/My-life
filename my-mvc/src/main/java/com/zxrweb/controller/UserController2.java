package com.zxrweb.controller;

import com.springmvc.annotation.*;
import com.zxrweb.bean.User;
import com.zxrweb.service.UserService;


@Controller
public class UserController2 {

    @AutoWired(value="userService")
    private UserService userService;


    //定义方法
    @RequestMapping("/findUser2")
    public  String  findUser2(String name, Integer age){
        System.out.println(name + ": " + age);
        //调用服务层
        userService.findUser();
        return "forward:/success.jsp";
    }

    @RequestMapping("/getData2")
    @ResponseBody  //返回json格式的数据
    public User getData2(@RequestParam(value = "myName") String name, Integer age){
        System.out.println(name + ": " + age);
        //调用服务层
        return userService.getUser();
    }
}
