package com.zxr.common.vm;

import com.zxr.common.enums.CodeMessage;
import lombok.Data;

import java.io.Serializable;

import static com.zxr.common.enums.ResultCodes.SUCCESS;

/**
 * @Description: 返回规范
 * @Author: Zheng Xinrui
 * @Date: 15:25 2019/4/3
 */
@Data
public class Result<T> implements Serializable{

    /**
     * 状态码
     */
    private int code;

    /**
     * 返回说明
     */
    private String message;

    /**
     * 返回内容
     */
    private T data;

    public Result(){
        //nothing to do
    }

    public Result(int code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Result<T> result(){
        return result(SUCCESS);
    }

    public static <T> Result<T> result(T data){
        return result(data, SUCCESS);
    }

    public static <T> Result<T> result(T data, CodeMessage codeMessage){
        return new Result(codeMessage.getCode(), codeMessage.getMessage(), data);
    }

    public static <T> Result<T> result(CodeMessage codeMessage){
        return new Result(codeMessage.getCode(), codeMessage.getMessage(), null);
    }

    public static <T> Result<T> result(T data, int code, String message){
        return new Result(code, message, data);
    }

    public static <T> Result<T> result(int code, String message){
        return new Result(code, message, null);
    }
}
