package com.zxr.common.vm;

import com.github.pagehelper.Page;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @Description: 简单分页对象，用于系统内外部数据传输交互
 * @Author: Zheng Xinrui
 * @Date: 10:29 2019/5/6
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PageTO<T> {

    private static final long serialVersionUID = 7630016407919147864L;
    /**
     * 当前页
     */
    private int pageNum;
    /**
     * 每页的数量
     */
    private int pageSize;
    /**
     * 总记录数
     */
    private long total;
    /**
     * 总页数
     */
    private int pages;
    /**
     * 排序字段
     */
    private String orderBy;
    /**
     * 数据结果
     */
    private List<T> list;

    public PageTO(List list) {
        if (list instanceof Page) {
            Page page = (Page) list;
            this.pageNum = page.getPageNum();
            this.pageSize = page.getPageSize();
            this.orderBy = page.getOrderBy();

            this.pages = page.getPages();
            this.list = page;
            this.total = page.getTotal();
        }
    }

}

