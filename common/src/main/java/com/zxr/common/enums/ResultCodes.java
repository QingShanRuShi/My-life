package com.zxr.common.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 14:49 2019/4/3
 */
@AllArgsConstructor
public enum ResultCodes implements CodeMessage{

    SUCCESS(1, "成功"),
    FAIL(-1, "失败");


    @Getter
    private int code;

    @Getter
    private String message;
}
