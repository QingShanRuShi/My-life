package com.zxr.common.enums;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 13:40 2019/4/2
 */
public interface CodeMessage {

    int getCode();

    String getMessage();
}
