package com.zxr.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 通用状态码集合
 * @Author: Zheng Xinrui
 * @Date: 13:41 2019/4/2
 */
@AllArgsConstructor
public enum  ErrorCodes implements CodeMessage{

    //10000打头表示系统通用异常
    ERROR_10001(10001, "失败"),
    ERROR_10002(10002, "异常"),

    //20000打头表示自定义error
    ERROR_20001(20001, "token不存在"),
    ERROR_20002(20002, "重复刷接口停止访问"),
    ERROR_20003(20003, "用户不存在");

    @Getter
    private int code;
    @Getter
    private String message;
}
